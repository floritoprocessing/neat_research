class FoodManager {

  

  ArrayList<Food> foods;

  FoodManager() {
    foods = new ArrayList<Food>();
  }


  void integrate(float dt, PlayField field) {
    float foodAmount = dt * Z.FOOD_PER_SECOND;
    while (foodAmount>1 && foods.size()<Z.FOOD_MAX) {
      foods.add(createFood(field));
      foodAmount--;
    }
    // now foodAmount is between 0 and 1;
    if (random(1.0)<foodAmount && foods.size()<Z.FOOD_MAX) {
      foods.add(createFood(field));
    }
  }


  Food createFood(PlayField field) {
    PVector pos;
    Food food = null;
    boolean foodOverlapsLines;
    do {
      pos = field.getRandomPosition();
      food = new Food(pos, Z.FOOD_SIZE);
      foodOverlapsLines = false;
      ArrayList<Line> fieldLines = field.lines;
    outer:
      for (Line foodLine : food.lines) {
        for (Line fieldLine : fieldLines) {
          if (lineSegmentsIntersect(foodLine, fieldLine)) {
            foodOverlapsLines = true;
            break outer;
          }
        }
      }
      
    } while (foodOverlapsLines);
    
    return food;
  }
}



class Food {

  PVector center;
  float size;

  PVector p0, p1, p2, p3;
  Line[] lines;
  float energy;

  Food(PVector position, float size) {
    center = position.copy();
    this.size = size;
    lines = new Line[4];  
    p0 = PVector.sub( position, new PVector(-size/2, -size/2) );
    p1 = PVector.add( p0, new PVector(size, 0) );
    p2 = PVector.add( p1, new PVector(0, size) );
    p3 = PVector.add( p2, new PVector(-size, 0) );
    lines[0] = new Line(p0, p1);
    lines[1] = new Line(p1, p2);
    lines[2] = new Line(p2, p3);
    lines[3] = new Line(p3, p0);
    energy = size*size * Z.FOOD_AREA_TO_ENERGY_FACTOR;
  }

  void draw() {
    noFill();
    stroke(100, 100, 255);
    strokeWeight(1);
    beginShape(QUAD);
    vertex(p0.x, p0.y);
    vertex(p1.x, p1.y);
    vertex(p2.x, p2.y);
    vertex(p3.x, p3.y);
    endShape();
  }
}
