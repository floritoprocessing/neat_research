


/*

 
 FIELD
 
 
 */

import java.util.Arrays;


class PlayField {

  float[] FIELD0_BASIC = new float[] {
    34, 33, 749, 47, 743, 37, 766, 622, 
    777, 617, 26, 620, 35, 630, 45, 25};

  float[] FIELD1_SOME_LINES = new float[] {
    34, 33, 749, 47, 743, 37, 766, 622, 
    777, 617, 26, 620, 35, 630, 45, 25, 
    191, 340, 221, 267, 391, 369, 465, 438, 
    551, 207, 609, 246, 567, 546, 629, 507, 
    117, 546, 205, 574, 167, 165, 349, 189, 
    555, 79, 487, 153};

  float[] FIELD2_MORE_LINES = new float[] {
    34, 33, 749, 47, 743, 37, 766, 622, 
    777, 617, 26, 620, 35, 630, 45, 25, 
    191, 340, 221, 267, 391, 369, 465, 438, 
    551, 207, 609, 246, 567, 546, 629, 507, 
    117, 546, 205, 574, 167, 165, 349, 189, 
    555, 79, 487, 153, 321, 290, 292, 554, 
    298, 555, 326, 294, 332, 293, 305, 555, 
    312, 555, 337, 297, 315, 295, 285, 540, 
    283, 523, 307, 310, 292, 369, 283, 458};

  float[][] FIELDS = new float[][] {
    FIELD0_BASIC, 
    FIELD1_SOME_LINES, 
    FIELD2_MORE_LINES
  };

  PVector minPos, maxPos;

  int fieldIndex = 1;
  ArrayList<Line> lines;
  Line currentLineInMaking = null;
  boolean makePlayfield = false;

  PlayField() {
    loadNextPlayField();
  }

  PVector getRandomPosition() {
    return new PVector( 
      minPos.x + random(1) * (maxPos.x-minPos.x), 
      minPos.y + random(1) * (maxPos.y-minPos.y));
  }

  PVector getStartPosition() {
    float p0 = 0.1;
    float p1 = 0.9;
    return new PVector( 
      minPos.x + random(p0, p1) * (maxPos.x-minPos.x), 
      minPos.y + random(p0, p1) * (maxPos.y-minPos.y));
  }

  float getStartDirection() {
    return random(TWO_PI);
  }

  void loadNextPlayField() {
    fieldIndex++;
    fieldIndex %= FIELDS.length;
    println("loadNextPlayField(): "+fieldIndex);
    createLines( FIELDS[fieldIndex] );
    calcMinMax();
  }


  void calcMinMax() {
    float minX = Float.MAX_VALUE;
    float minY = minX;
    float maxX = Float.MIN_VALUE;
    float maxY = maxX;
    ArrayList<PVector> points = new ArrayList<PVector>();
    for (Line line : lines) {
      points.add( line.getP0() );
      points.add( line.getP1() );
    }
    for (PVector p : points) {
      minX = min(minX, p.x);
      maxX = max(maxX, p.x);
      minY = min(minY, p.y);
      maxY = max(maxY, p.y);
    }
    minPos = new PVector(minX, minY);
    maxPos = new PVector(maxX, maxY);
  }


  boolean isInTheMaking() {
    return makePlayfield;
  }

  void initMakePlayfield() {
    makePlayfield = true;
    lines = new ArrayList<Line>();
  }




  void addPoint(float x, float y) {
    if (currentLineInMaking==null) {
      currentLineInMaking = new Line( new PVector(x, y), new PVector(x, y) );
      lines.add(currentLineInMaking);
    } else {
      currentLineInMaking.setP1( new PVector(x, y) );
      currentLineInMaking = null;
    }
  }



  void createLines(float... xyPairs) {
    lines = new ArrayList<Line>();
    for (int i=0; i<xyPairs.length; i+=4) {
      lines.add( new Line( new PVector(xyPairs[i], xyPairs[i+1]), new PVector(xyPairs[i+2], xyPairs[i+3]) ) );
    }
  }



  void finalize() {
    // remove last if it's an incomplete line
    if (currentLineInMaking!=null) {
      currentLineInMaking = null;
      lines.remove(lines.size()-1);
    }
    makePlayfield = false;
    calcMinMax();

    ArrayList<PVector> points = new ArrayList<PVector>();
    for (Line line : lines) {
      points.add(line.getP0());
      points.add(line.getP1());
    }
    printPointsArray(points);
  }

  void printPointsArray(float... xyPairs) {
    ArrayList<PVector> points = new ArrayList<PVector>();
    for (int i=0; i<xyPairs.length; i+=2) {
      points.add( new PVector(xyPairs[i], xyPairs[i+1]) );
    }
    printPointsArray(points);
  }

  void printPointsArray(ArrayList<PVector> points) {
    print("float[] xyPoints = new float[] {");
    for (int i=0; i<points.size(); i++) {
      if (i%4==0) {
        println();
      }
      print("   "+int(points.get(i).x)+", "+int(points.get(i).y));
      if (i<points.size()-1) print(",");
    }
    print("};");
  }







  void draw() {

    strokeWeight(1);
    if (makePlayfield) {

      // draw cursor
      stroke(0);
      line(mouseX, mouseY-20, mouseX, mouseY-5);
      line(mouseX, mouseY+20, mouseX, mouseY+5);
      line(mouseX-20, mouseY, mouseX-5, mouseY);
      line(mouseX+20, mouseY, mouseX+5, mouseY);

      //move last point
      if (currentLineInMaking!=null) {
        currentLineInMaking.setP1( new PVector(mouseX, mouseY) );
      }
    }

    
    for (int i=0; i<lines.size(); i++) {
      stroke(64);
      lines.get(i).draw();
    }
  }
}




int uniqueLineId = 0;

class Line {

  private final PVector _p0, _p1;
  private PVector _normal;
  private float _heading;
  private float _magnitude;

  public boolean drawFlag = false;
  int id = uniqueLineId++;

  Line(float x0, float y0, float x1, float y1) {
    this( new PVector(x0, y0), new PVector(x1, y1) );
  }

  Line(PVector p0, PVector p1) {
    this._p0=p0;
    this._p1=p1;
    calcHeadingAndMagnitude();
  }

  private void calcHeadingAndMagnitude() {
    PVector dir = PVector.sub(_p1, _p0);
    _heading = dir.heading();
    _magnitude = dir.mag();
    _normal = dir.copy();
    _normal.rotate(HALF_PI);
    _normal.normalize();
  }

  void setP0(PVector p) {
    _p0.set(p);
    calcHeadingAndMagnitude();
  }

  void setP1(PVector p) {
    _p1.set(p);
    calcHeadingAndMagnitude();
  }

  PVector getP0() {
    return _p0;
  }
  PVector getP1() {
    return _p1;
  }
  PVector getNormal() {
    return _normal;
  }

  float getHeading() {
    return _heading;
  }

  float getMagnitude() {
    return _magnitude;
  }

  float calcPercentageFromX(float x) {
    return (x-_p0.x)/(_p1.x-_p0.x);
  }


  void draw() {
    if (drawFlag) {
      stroke(255, 0, 0);
      drawFlag = false;
      //println("Line "+id+"flagged");
    }
    line(_p0.x, _p0.y, _p1.x, _p1.y);
  }


  String toString() {
    return "Line ("+_p0.toString()+", "+_p1.toString()+")";
  }
}
