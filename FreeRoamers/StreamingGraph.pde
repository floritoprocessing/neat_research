class StreamingGraph {

  boolean drawAverage = true;
  boolean drawMean = true;

  int MAX_HORIZONTAL_VALUES = 20;//Integer.MAX_VALUE;
  int VALUE_MARKER_DIVISIONS = 5;
  int MAX_AMOUNT_HORIZONTAL_TEXT = 10;
  float VERTICAL_OVERHEAD = 0.1;

  int horizontalIndexOffset;
  float allValuesMin;
  float allValuesMax;
  Values[] values;
  Values averages;
  Values means;

  StreamingGraph(int nrOfValueLines) {
    reset(nrOfValueLines);
  }

  void reset() {
    if (values==null) throw new RuntimeException("reset() can only be called after reset(int nrOfValues)");
    else {
      reset(values.length);
    }
  }

  void reset(int nrOfValueLines) {
    horizontalIndexOffset = 0;
    allValuesMin = Float.MAX_VALUE;
    allValuesMax = Float.MIN_VALUE;

    values = new Values[nrOfValueLines];
    averages = new Values();
    means = new Values();
    for (int i=0; i<nrOfValueLines; i++) {
      values[i] = new Values();
      values[i].add(0);
    }
    averages.add(0);
    means.add(0);
    updateAllValuesMinMax();
  }



  void addValues(float[] timeValues) {


    if (timeValues.length!=values.length) {
      //throw new RuntimeException("Meep, timeValues("+timeValues.length+") not same length as values("+values.length+")");
      System.err.println("Meep, timeValues("+timeValues.length+") not same length as values("+values.length+"). Will reset graph");
      //values = new Values[];
      reset(timeValues.length);
    } else {


      if (values[0].size()==MAX_HORIZONTAL_VALUES) {
        for (int i=0; i<values.length; i++) {
          values[i].removeFirst();
        }
        averages.removeFirst();
        means.removeFirst();
        horizontalIndexOffset++;
      }
      float av = 0;
      float[] meanValues = new float[values.length];
      for (int i=0; i<values.length; i++) {
        float v = timeValues[i];
        values[i].add(v);
        av += v;
        meanValues[i] = v;
      }
      av /= values.length;
      averages.add(av);
      means.add( meanValues[meanValues.length/2] );
      updateAllValuesMinMax();
    }
  }





  void updateAllValuesMinMax() {
    allValuesMin = Float.MAX_VALUE;
    allValuesMax = Float.MIN_VALUE;
    for (int i=0; i<values.length; i++) {
      allValuesMin = min(allValuesMin, values[i]._min);
      allValuesMax = max(allValuesMax, values[i]._max);
    }
  }

  /*
  *
   *    Drawing
   *
   */


  void draw(float sx, float sy, float w, float h) {

    strokeWeight(1);
    stroke(255);
    fill(0);



    pushMatrix();

    translate(sx, sy);
    rectMode(CORNER);
    rect(0, 0, w, h);

    fill(255);

    int gens = values[0].size();
    if (gens>1) {

      // make time/generation markers     
      int divToDraw = 1;
      while (gens>MAX_AMOUNT_HORIZONTAL_TEXT*divToDraw) {
        divToDraw*=2;
      }      
      textAlign(CENTER, TOP);
      for (int i=0; i<gens; i++) {
        float x = map(i, 0, gens-1, 0, w);
        line(x, h, x, h+5);
        boolean drawText = i%divToDraw==0 || i==gens-1;
        if (drawText) {
          text(i+horizontalIndexOffset, x, h+10);
        }
      }



      // make value markers
      textAlign(RIGHT, BASELINE);
      for (int i=0; i<VALUE_MARKER_DIVISIONS+1; i++) {
        float y = map(i, 0, VALUE_MARKER_DIVISIONS, h, h - (h)/(1.0+VERTICAL_OVERHEAD));
        line(0, y, -5, y);
        String val = nf(map(i, 0, VALUE_MARKER_DIVISIONS, 0, allValuesMax), 1, 3);
        text(val, -10, y);
      }


      // draw lines
      // iterate per line (i)

      int lineCount = values.length + (drawAverage?1:0) + (drawMean?1:0);
      int avIndex = drawAverage ? values.length : -1;
      int meIndex = drawMean ? (drawAverage?(values.length+1):values.length) : -1;

      for (int i=0; i<lineCount; i++) {

        Values vals=null;
        if (i<values.length) {
          vals = values[i];
          stroke(255, 24);
        } else if (i==avIndex) {
          vals = averages;
          stroke(255, 0, 0, 192);
        } else if (i==meIndex) {
          vals = means;
          stroke(0, 255, 0, 192);
        }
        //println(i+": "+vals);

        beginShape(LINE_STRIP);
        // iterate per gen
        for (int t=0; t<gens; t++) {
          float val = vals.get(t);
          float x = map(t, 0, gens-1, 0, w);
          float y = map(val, 0, allValuesMax, h, h - (h)/(1.0+VERTICAL_OVERHEAD));
          vertex(x, y);
        } 
        endShape();
      } // end for linecount
    }


    popMatrix();
  }


  // Values Class

  class Values {
    float _min = Float.MAX_VALUE; 
    float _max = Float.MIN_VALUE; 
    ArrayList<Float> _values = new ArrayList<Float>(); 

    void add(float value) {
      _values.add(value); 
      _min = min(_min, value); 
      _max = max(_max, value);
    }

    float get(int index) {
      return _values.get(index);
    }

    int size() {
      return _values.size();
    }

    void removeFirst() {
      if (_values.size()==0) {
        RuntimeException e = new RuntimeException("Meep!");
        e.printStackTrace();
        throw e;
      }
      _values.remove(0);
    }
  }
} // end of StreamingGraph class
