// VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE



boolean vehicleUnimplementedMethodsWarning = true;

class Vehicle {

  private PlayField _field;
  private FoodManager _foodManager;

  final float turningSpeed; // degrees per second
  final float thrustSpeed; // acceleration in pixels/sˆ2
  final float breakSpeed; // break in pixels/sˆ2
  final float size;
  final PVector[] points; // body points
  final PVector[] localFoodSensorVecs;
  final PVector[] localSensorVecs;

  // updated every frame & used for collision detection
  Line[] globalSensorLines;
  Line[] globalFoodSensorLines;
  Line[] globalBodyLines;

  float energy;

  final RunningAverage averageSpeed;

  String name = "noName";

  boolean singleFrameHighlight;


  PVector lastPosition = new PVector();
  PVector position = new PVector();
  PVector velocity = new PVector();
  float direction;
  float speed;

  boolean[] inSensorVecRange; 
  float[] inSensorVecDistance; // percentages (reverse distance: 1=close, 0=far)

  boolean[] inFoodSensorVecRange; 
  float[] inFoodSensorVecDistance; // percentages (reverse distance: 1=close, 0=far)

  boolean dead;
  VehicleStats stats = new VehicleStats();
  NodeBrain nodeBrain;

  boolean turningLeft=false, turningRight=false, increasingSpeed=false, decreasingSpeed=false;


  Vehicle clone() {
    Vehicle v = new Vehicle();
    v.nodeBrain = nodeBrain.clone();
    v.name = name;
    return v;
  }



  Vehicle() {
    this.averageSpeed = new RunningAverage(Z.VEHICLE_AVERAGE_SPEED_FRAMES);
    this.turningSpeed = Z.VEHICLE_TURNING_SPEED;
    this.thrustSpeed = Z.VEHICLE_THRUST_SPEED;
    this.breakSpeed = Z.VEHICLE_BREAK_SPEED;
    this.size = Z.VEHICLE_SIZE;
    this.points = new PVector[] {  new PVector(-size, size), new PVector(0, -size*1.5), new PVector(size, size) };

    globalBodyLines = new Line[points.length];

    for (int i=0; i<points.length; i++) {
      int i0 = i;
      int i1 = (i+1)%points.length;
      globalBodyLines[i] = new Line(points[i0], points[i1]);
    }
    
    
    localSensorVecs = new PVector[Z.VEHICLE_SENSORS];
    globalSensorLines = new Line[Z.VEHICLE_SENSORS];
    
    for (int i=0; i<Z.VEHICLE_SENSORS; i++) {
      float rd = map(i, 0, Z.VEHICLE_SENSORS-1, -radians(Z.VEHICLE_SENSOR_DEGREES), radians(Z.VEHICLE_SENSOR_DEGREES));
      float len = size*(i==(Z.VEHICLE_SENSORS-1)/2 ? Z.VEHICLE_SIZE_TO_CENTRAL_SENSOR_FAC : Z.VEHICLE_SIZE_TO_SENSOR_FAC);
      PVector sensorVec = new PVector(0, -len);
      sensorVec.rotate(rd);
      localSensorVecs[i] = sensorVec;
      //globalSensorVecs[i] = sensorVec.copy();
      globalSensorLines[i] = new Line(0, 0, sensorVec.x, sensorVec.y);
    }


    localFoodSensorVecs = new PVector[Z.VEHICLE_FOOD_SENSORS];
    globalFoodSensorLines = new Line[Z.VEHICLE_FOOD_SENSORS];

    for (int i=0; i<Z.VEHICLE_FOOD_SENSORS; i++) {
      float rd = map(i, 0, Z.VEHICLE_FOOD_SENSORS-1, -radians(Z.VEHICLE_FOOD_SENSOR_DEGREES), radians(Z.VEHICLE_FOOD_SENSOR_DEGREES));
      float len = size * Z.VEHICLE_FOOD_SIZE_TO_SENSOR_FAC;
      PVector sensorVec = new PVector(0, -len);
      sensorVec.rotate(rd);
      localFoodSensorVecs[i] = sensorVec;
      //globalSensorVecs[i] = sensorVec.copy();
      globalFoodSensorLines[i] = new Line(0, 0, sensorVec.x, sensorVec.y);
    }


    reset();
    //initForStart();
    //initDirectionAndSpeed(radians(45), 0);


    initBrain();
  }





  void initForStart(PlayField field, FoodManager foodManager, ArrayList<Vehicle> population) {
    this._field = field;
    this._foodManager = foodManager;
    reset();

    boolean overlappingWithOtherVehicle = true;
    boolean overlappingWithField = true;
    int maxTries = 1000;
    int tryCount = 0;

    boolean IGNORE_OVERLAP_VEHICLE = true;

    while (overlappingWithOtherVehicle || overlappingWithField) {

      // get new start position
      position = _field.getStartPosition();
      initDirectionAndSpeed( _field.getStartDirection(), 0);

      // check body to body overlap
      overlappingWithOtherVehicle = overlapsWithOtherVehicle(population).size()>0;


      // check body with field line overlap

      overlappingWithField = overlapsWithFieldLines();


      //println(name+": Checking with others: "+populationThatIsOnTheField.size());
      tryCount++;
      if (tryCount>maxTries && (overlappingWithOtherVehicle||overlappingWithField)) {

        if (overlappingWithOtherVehicle) {
          println("initForStart(): Setting on top of other vehicle after "+tryCount+" tries!");
        } else if (overlappingWithField) {
          println("initForStart(): Setting on top of field "+tryCount+" tries!");
        }
        overlappingWithOtherVehicle = false;
        overlappingWithField = false;
      }

      if (IGNORE_OVERLAP_VEHICLE && !overlappingWithField) {
        overlappingWithOtherVehicle = false;
        overlappingWithField = false;
      }
    }
  }


  boolean bodiesOverlap(Vehicle other) {

    for (Line line : globalBodyLines) {
      for (Line otherLine : other.globalBodyLines) {
        if (lineSegmentsIntersect(line, otherLine)) {
          return true;
        }
      }
    }
    return false;
  }



  ArrayList<Vehicle> overlapsWithOtherVehicle(ArrayList<Vehicle> population) {
    //boolean overlappingWithOtherVehicle=false;
    ArrayList<Vehicle> listOfOverlaps = new ArrayList<Vehicle>();

    for (Vehicle v : population) {
      if (v._field!=null && v!=this) {
        if (bodiesOverlap(v)) {
          listOfOverlaps.add(v);
        }
      }
    }

    return listOfOverlaps;
  }


  boolean overlapsWithLine(Line line) {
    for (Line bodyLine : globalBodyLines) {
      if (lineSegmentsIntersect(line, bodyLine)) {
        return true;
      }
    }
    return false;
  }

  boolean overlapsWithFieldLines() {
    boolean overlappingWithField = false;
  outerField: 
    for (Line fieldLine : _field.lines) {
      for (Line bodyLine : globalBodyLines) {
        if (lineSegmentsIntersect(fieldLine, bodyLine)) {
          overlappingWithField = true;
          break outerField;
        }
      }
    }
    return overlappingWithField;
  }




  void reset() {
    singleFrameHighlight = false;
    averageSpeed.reset();

    lastPosition = new PVector();
    position = new PVector();
    velocity = new PVector();
    direction = 0;
    speed = 0;

    inSensorVecRange = new boolean[Z.VEHICLE_SENSORS];
    inSensorVecDistance = new float[Z.VEHICLE_SENSORS];

    inFoodSensorVecRange = new boolean[Z.VEHICLE_FOOD_SENSORS];
    inFoodSensorVecDistance = new float[Z.VEHICLE_FOOD_SENSORS];

    energy = Z.VEHICLE_START_ENERGY;

    dead = false;
    stats.reset();
  }


  //void setField(PlayField field) {
  //  this._field = field;
  //}

  void initBrain() {

    // make input names
    String[] inputNames = new String[Z.VEHICLE_SENSORS+1];
    int half = (Z.VEHICLE_SENSORS-1)/2;
    for (int i=0; i<inputNames.length-1; i++) {
      if (i<half) inputNames[i]="left"+(i);
      else if (i==half) inputNames[i]="center";
      else if (i>half) inputNames[i]="right"+(inputNames.length-half-i+2);
    }
    inputNames[inputNames.length-1] = "speed";

    // make output names
    String[] outputNames = new String[] { "left", "right", "thrustBreak", "break" };

    // make all layer names and counts
    ArrayList<Integer> allLayerNeuronCounts = new ArrayList<Integer>();
    ArrayList<String[]> allLayerNames = new ArrayList<String[]>();

    allLayerNames.add(inputNames);
    allLayerNeuronCounts.add(inputNames.length);

    for (int i=0; i<Z.HIDDEN_NEURONS.length; i++) {
      String[] oneLayerNames = new String[Z.HIDDEN_NEURONS[i]];
      for (int j=0; j<oneLayerNames.length; j++) {
        oneLayerNames[j] = "n"+i+"_"+j;
      }
      allLayerNames.add(oneLayerNames);
      allLayerNeuronCounts.add(oneLayerNames.length);
    }

    allLayerNames.add( outputNames );
    allLayerNeuronCounts.add( outputNames.length );

    int[] nodesPerLayer = new int[allLayerNeuronCounts.size()];
    for (int i=0; i<nodesPerLayer.length; i++) 
      nodesPerLayer[i] = allLayerNeuronCounts.get(i);

    String[][] nodeNames = new String[allLayerNames.size()][];
    for (int i=0; i<nodeNames.length; i++) {
      String[] oneLayerNames = allLayerNames.get(i);
      nodeNames[i] = oneLayerNames;
    }

    nodeBrain = new NodeBrain();
    nodeBrain.createStandard(nodesPerLayer, nodeNames);
    //nodeBrain.createStandard(new int[] {Z.VEHICLE_SENSORS+1, hiddenNeuronAmount0, 4}, names); //hiddenNeuronAmount0
  }



  void turnAndThrust(boolean left, boolean right, boolean thrust, boolean brek) {
    turningLeft=left;
    turningRight=right;
    increasingSpeed=thrust;
    decreasingSpeed=brek;
  }
  void turnLeft(boolean turn) {
    turningLeft = turn;
  }
  void turnRight(boolean turn) {
    turningRight = turn;
  }
  void increaseSpeed(boolean thrust) {
    increasingSpeed = thrust;
  }
  void decreaseSpeed(boolean doBreak) {
    decreasingSpeed = doBreak;
  }


  void initDirectionAndSpeed(float dir, float spd) {
    this.direction = dir;
    this.speed = spd;
    setVelocityFromSpeedAndDirection();
    changeGlobalBodyLinesAndSensors();
  }



  void setVelocityFromSpeedAndDirection() {
    velocity = new PVector(0, -1);
    velocity.rotate(this.direction);
    velocity.setMag(this.speed);
  }



  // CROSSING FIELD LINES

  void slowDownByFieldLines(float dt) {
    if (!dead && overlapsWithFieldLines()) {
      slowDown(dt);
    }
  }


  void decreaseEnergyByOtherVehicles(float dt, ArrayList<Vehicle> population) {
    // CROSSING OTHER BODIES
    ArrayList<Vehicle> listOfOverlaps = overlapsWithOtherVehicle(population);
    if (listOfOverlaps.size()>0) {
      energy -= Z.VEHICLE_ENERGY_DECREASE_ON_BODY_INTERSECTION_PER_SECOND * dt;
      //slowDown(dt);
    }
  }


  void slowDown(float dt) {
    float reverseAcceleration = speed * -10;
    speed = 0;//speed + dt*reverseAcceleration;
    setVelocityFromSpeedAndDirection();
  }





  void checkDeathConditions() {

    //if (vehicleUnimplementedMethodsWarning) System.err.println("TODO: Vehicle.checkDeathConditions()");

    if (!dead) {

      // TOO SLOW:
      //if (averageSpeed.isComplete() && averageSpeed.getAverage()<Z.VEHICLE_AVERAGE_SPEED_DEATH) {
      //  dead = true;
      //  stats.deathReason = VehicleDeathReason.AVERAGE_SPEED_TOO_LOW;
      //} 

      // OUT OF BOUNDS
      if (position.x < _field.minPos.x || position.x >= _field.maxPos.x
        || position.y < _field.minPos.y || position.y >= _field.maxPos.y) {

        killByOutOfBounds();
      }

      // OUT OF ENERGY
      else if (energy<=0) {
        killByOutOfEnergy();
      }
    }

    if (dead) {
      println(name+" dead: "+stats.deathReason);
    }

    vehicleUnimplementedMethodsWarning = false;
  }




  void useBrain() {

    float[] inputValues = new float[inSensorVecDistance.length+1];
    for (int i=0; i<inputValues.length-1; i++) {
      inputValues[i] = inSensorVecDistance[i];
    }
    inputValues[inputValues.length-1] = speed / 200.0;
    nodeBrain.feedForward(inputValues);

    float[] outputs = nodeBrain.getLastLayerFeedForwardResult();
    boolean left = outputs[0]>0;
    boolean right = outputs[1]>0;
    boolean thrust = outputs[2]>0;
    boolean brek = outputs[3]>0;
    turnAndThrust(left, right, thrust, brek);
  }




  void integrate(float seconds) { 
    lastPosition.set(position);
    applyTurningAndSpeed(seconds);
    changeGlobalBodyLinesAndSensors();
    averageSpeed.add(speed);
    energy -= Z.VEHICLE_ENERGY_DECREASE_PER_SECOND * seconds;

    updateStats();
    stats.age(seconds);
    if (stats.age>Z.VEHICLE_MAX_AGE) {
      killByAge();
    }
  }




  void changeGlobalBodyLinesAndSensors() {

    for (int i=0; i<globalSensorLines.length; i++) {
      PVector globalSensorVec = localSensorVecs[i].copy(); 
      globalSensorVec.rotate(direction);
      globalSensorVec.add( position );
      globalSensorLines[i] = new Line( position, globalSensorVec );
    }

    for (int i=0; i<globalFoodSensorLines.length; i++) {
      PVector globalFoodSensorVec = localFoodSensorVecs[i].copy(); 
      globalFoodSensorVec.rotate(direction);
      globalFoodSensorVec.add( position );
      globalFoodSensorLines[i] = new Line( position, globalFoodSensorVec );
    }

    PVector[] globalPoints = new PVector[points.length];
    int pCount = points.length;
    for (int i=0; i<pCount; i++) {
      globalPoints[i] = points[i].copy();
      globalPoints[i].rotate(direction);
      globalPoints[i].add( position );
    }
    for (int i=0; i<pCount; i++) {
      PVector p0 = globalPoints[i];
      PVector p1 = globalPoints[ (i+1)%pCount ];
      globalBodyLines[i] = new Line(p0, p1);
    }
  }


  void updateStats() {
    if (vehicleUnimplementedMethodsWarning) System.err.println("TODO: Vehicle.updateStats()");
  }




  void killByUser() {
    dead = true;
    stats.deathReason = VehicleDeathReason.KILLED_BY_USER;
  }



  void killByFieldLine() {
    dead = true;
    stats.deathReason = VehicleDeathReason.BODY_INTERSECTED_FIELD_LINE;
  }



  void killByOtherVehicle() {
    dead = true;
    stats.deathReason = VehicleDeathReason.BODY_INTERSECTED_OTHER_BODY;
  }



  void killByOutOfEnergy() {
    dead = true;
    stats.deathReason = VehicleDeathReason.OUT_OF_ENERGY;
  }



  void killByOutOfBounds() {
    dead = true;
    stats.deathReason = VehicleDeathReason.OUT_OF_BOUNDS;
  }



  void killByAge() {
    dead = true;
    stats.deathReason = VehicleDeathReason.AGE_TOO_HIGH;
  }


  void applyTurningAndSpeed(float seconds) {
    if (turningLeft||turningRight||increasingSpeed||decreasingSpeed) {
      float deltaTurnRad = 0;
      float deltaSpeed = 0;
      if (turningLeft||turningRight) {
        float deltaTurnDeg = ((turningRight?1:0)+(turningLeft?-1:0)) * turningSpeed * seconds;
        deltaTurnRad = radians(deltaTurnDeg);
      }
      if (increasingSpeed||decreasingSpeed) {
        deltaSpeed = ((increasingSpeed?thrustSpeed:0)-(decreasingSpeed?breakSpeed:0)) * seconds;
      }
      float newSpeed = max(0, speed+deltaSpeed); //
      initDirectionAndSpeed( direction+deltaTurnRad, newSpeed );
    }

    position = PVector.add( position, PVector.mult( velocity, seconds ) );
  }




  void checkSensors() {

    ArrayList<Line> lines = _field.lines;
    Line[] sensorLines = globalSensorLines;
    boolean[] outputInSensorRange = inSensorVecRange;
    float[] outputInSensorVecDistance = inSensorVecDistance;

    checkSensorsAndApply(lines, sensorLines, outputInSensorRange, outputInSensorVecDistance);

    lines.clear();
    for (Food food : _foodManager.foods) {
      for (Line line : food.lines) {
        lines.add(line);
      }
    }
    sensorLines = globalFoodSensorLines;
    outputInSensorRange = inFoodSensorVecRange;
    outputInSensorVecDistance = inFoodSensorVecDistance;

    checkSensorsAndApply(lines, sensorLines, outputInSensorRange, outputInSensorVecDistance);
  }

  void checkSensorsAndApply(ArrayList<Line> lines, Line[] sensorLines, boolean[] outputInSensorRange, float[] outputInSensorVecDistance) {

    for (int sensor=0; sensor<sensorLines.length; sensor++) {

      Line globalSensorLine = sensorLines[sensor];
      boolean intersect = false;
      float closestDistancePerc = Float.MAX_VALUE;
      Line closestLine = null;
      outputInSensorVecDistance[sensor] = 0;

      for (Line line : lines) {
        if (lineSegmentsIntersect(line, globalSensorLine)) {
          PVector intersectionPoint = lineLineIntersection(globalSensorLine, line);
          float distToIntersectionPoint = PVector.sub(intersectionPoint, globalSensorLine.getP0()).mag();
          float distPerc = distToIntersectionPoint/globalSensorLine.getMagnitude();
          if (distPerc<closestDistancePerc) {
            closestDistancePerc=distPerc;
            closestLine = line;
          }
          intersect=true;
        }
      } // end iterating through playfield lines

      outputInSensorRange[sensor] = intersect;
      if (intersect) {
        outputInSensorVecDistance[sensor] = 1-closestDistancePerc;
        //closestLine.drawFlag = true;
      }
    } // end for sensor
  }





  /*
  *
   *    EAT
   *
   */

  void checkFood( FoodManager foodManager ) {



    ArrayList<Food> foods = foodManager.foods;
    for (int i=0; i<foods.size(); i++) {

      Food food = foods.get(i);
      //PVector foodPos = food.center;
      //float dx = foodPos.x-position.x;
      //float dy = foodPos.y-position.y;
      //float inRangeSq = (food.size/2 + size*0.75)*(food.size/2 + size*0.75);
      //float dsq = dx*dx+dy*dy;

      boolean eatFood = false;
      //if (dsq<inRangeSq) {
      for (Line foodLine : food.lines) {
        if (overlapsWithLine(foodLine)) {
          eatFood = true;
          break;
        }
      }
      //}
      if (eatFood) {

        energy += food.energy;
        energy = min(energy, Z.VEHICLE_MAX_ENERGY);
        foods.remove(i);
        i--;
      }
    }
  }




  /*
  *
   *    DRAWING
   *
   */




  void draw() {
    strokeWeight(1);

    if (Z.DRAW_VEHICLE_SENSORS) {

      for (int i=0; i<globalSensorLines.length; i++) {        
        if (!inSensorVecRange[i]) {
          stroke(0, 255, 0);
        } else {
          float green = max(0, min(255, map(inSensorVecDistance[i], 0, 0.2f, 255, 0)));
          float red = map(inSensorVecDistance[i], 0, 1, 128, 255);
          stroke(red, green, 0);
        }
        globalSensorLines[i].draw();
      }  

      for (int i=0; i<globalFoodSensorLines.length; i++) {        
        if (!inFoodSensorVecRange[i]) {
          stroke(0, 0, 255);
        } else {
          float green = max(0, min(255, map(inFoodSensorVecDistance[i], 0, 0.2f, 255, 0)));
          float red = map(inFoodSensorVecDistance[i], 0, 1, 128, 255);
          stroke(0, red, green);
        }
        globalFoodSensorLines[i].draw();
      }
    }

    if (singleFrameHighlight) {
      fill(200, 100, 100);
      stroke(255, 0, 0);
      translate(0, 0, 1);
    } else {
      stroke(255);
      noFill();
    }

    beginShape(TRIANGLES);
    for (int i=0; i<globalBodyLines.length; i++) {
      PVector p = globalBodyLines[i].getP0();
      vertex(p.x, p.y);
    }
    endShape();

    // draw energy
    if (Z.DRAW_VEHICLE_ENERGY_STATS) {
      float barHeight = Z.DRAW_VEHICLE_ENERGY_BAR_HEIGHT;
      float barWidth = Z.DRAW_VEHICLE_ENERGY_BAR_WIDTH;

      rectMode(CORNER);
      noFill();
      stroke(255);
      float x = position.x+size*2;
      float y = position.y-barHeight;
      //rect(x, y, barWidth, barHeight);

      fill(255, 255, 0);
      noStroke();
      float bh = map(energy, 0, Z.VEHICLE_MAX_ENERGY, 0, barHeight-2);
      y = y+1+barHeight-2-bh;
      rect(x+1, y, barWidth-2, bh);
    }

    singleFrameHighlight = false;
  }





  float drawInfoTargetX=0;
  float drawInfoTargetY=0;
  float drawInfoX=0, drawInfoY=0;

  void drawInfo() {

    singleFrameHighlight = true;
    float xOff = 30;
    float x = position.x+xOff;
    float y = position.y;
    float w = 350;
    float h = 250;
    if (x+w>width-10) x=position.x-xOff-w; // left of vehicle
    if (y+h>height-10) y=height-10-h;
    //x=(int)x;
    //y=(int)y;
    drawInfoTargetX=x;
    drawInfoTargetY=y;

    if (drawInfoX==0 && drawInfoY==0) {
      drawInfoX = drawInfoTargetX;
      drawInfoY = drawInfoTargetY;
    } else {
      drawInfoX += (drawInfoTargetX-drawInfoX)*0.1;
      drawInfoY += (drawInfoTargetY-drawInfoY)*0.1;
    }
    x = drawInfoX;
    y = drawInfoY;

    rectMode(CORNER);

    stroke(255);
    pushMatrix(); 

    translate(0, 0, 1);
    fill(Z.VEHICLE_INFO_RECT_FILL);
    rect(x, y, w, h);
    fill(255);
    startText(name, x+10, y+10+textAscent());
    String avspeed = "running average speed=" 
      + (averageSpeed.isComplete()?nf(averageSpeed.getAverage(), 1, 1):"-undef-");
    text(avspeed);

    translate(x+20, screenTextY);
    //brain.draw(w,h-(screenTextY-y));
    drawNodeBrain(nodeBrain, w, h-(screenTextY-y));

    popMatrix();
  }





  /*
  *
   *    SERIALISATION
   *    name & brain
   *
   */

  final String KEY_NAME = "name";
  final String KEY_BRAIN = "brain";

  Vehicle(JSONObject json) {
    this();
    this.name = json.getString( KEY_NAME );
    this.nodeBrain = new NodeBrain( json.getJSONObject( KEY_BRAIN ) );
  }

  JSONObject toJSON() {
    JSONObject out = new JSONObject();
    out.setString( KEY_NAME, name );
    out.setJSONObject( KEY_BRAIN, nodeBrain.toJSON() );
    return out;
  }
}








class VehicleStats {

  VehicleDeathReason deathReason = VehicleDeathReason.NOT_SET;

  float age = 0;
  float fitness = 0;

  void reset() {
    deathReason = VehicleDeathReason.NOT_SET;
    age = 0;
    fitness = 0;
  }

  void age(float dt) {
    age+=dt;
  }
}


enum VehicleDeathReason {
  NOT_SET, 
    KILLED_BY_USER, 
    BODY_INTERSECTED_FIELD_LINE, 
    BODY_INTERSECTED_OTHER_BODY, 
    //AVERAGE_SPEED_TOO_LOW,
    OUT_OF_ENERGY, 
    AGE_TOO_HIGH, 
    OUT_OF_BOUNDS
}
