class DNA {
  
  char[] genes;
  float fitness;
  
  DNA(int numberOfGenes) {
    genes = new char[numberOfGenes];
    for (int i=0;i<numberOfGenes;i++) {
      genes[i] = (char)random(32,126);
    }
  }
  
  void evaluateFitness(String targetSentence) {
    int score = 0;
    for (int i=0;i<genes.length;i++) {
      if (genes[i] == targetSentence.charAt(i)) {
        score++;
      }
    }
    fitness = (float)score / genes.length;
    fitness = fitness*fitness;
  }
  
  
  
  DNA crossOver(DNA other) {
    int midPoint = (int)random(genes.length);
    DNA child = new DNA(genes.length);
    for (int i=0;i<genes.length;i++) {
      if (i==midPoint) child.genes[i] = genes[i];
      else             child.genes[i] = other.genes[i];
    }
    return child;
  }
  
  
  
  void mutate(float mutationRate) {
    for (int i=0;i<genes.length;i++) {
      if (random(1.0)<mutationRate) {
        genes[i] = (char)random(32,126);
      }
    }
  }
  
  
  String toString() {
    String out = new String(genes);
    out += " (fitness="+nf(fitness,1,3)+")";
    return out;
  }
  
}
