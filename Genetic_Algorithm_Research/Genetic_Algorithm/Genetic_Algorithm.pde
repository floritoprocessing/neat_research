import java.util.*;

int POPULATION_SIZE = 5000;
float MUTATION_RATE = 0.01;

String targetSentence = "This sentence has evolved over time!";
DNA[] population = new DNA[POPULATION_SIZE];
int generationCount = 0;
boolean finished = false;

void setup() {
  size(900,400);
  
  restart();
  
  textFont(loadFont("Monospaced-12.vlw"),12);
  //textFont(createFont("Verdana",12));
  
  //nextGeneration();
}

void mousePressed() {
  restart();
}

void restart() {
  for (int i=0;i<population.length;i++) {
    population[i] = new DNA(targetSentence.length());
  }
  finished = false;
  generationCount=0;
}


void draw() {
  if (!finished) {
    nextGeneration();
  }
}



void nextGeneration() {
  
  background(255);
  
  // evaluate fitness
  for (int i=0;i<population.length;i++) {
    population[i].evaluateFitness(targetSentence);
  }
  finished = finished | hasSolution();
  
  drawFittest(population, 20);
  
  // create wheel of fortune
  ArrayList<DNA> wheelOfFortune = new ArrayList<DNA>();
  for (int i=0;i<population.length;i++) {
    int amount = (int)(1000*population[i].fitness);
    for (int j=0;j<amount;j++) {
      wheelOfFortune.add(population[i]);
    }
  }
  
  DNA[] children = new DNA[population.length];
  for (int i=0;i<children.length;i++) {
    
    // choose two parents:
    DNA parent1 = wheelOfFortune.get( (int)random(wheelOfFortune.size()) );
    DNA parent2 = wheelOfFortune.get( (int)random(wheelOfFortune.size()) );
    
    // choose crossOver method
    DNA child = parent1.crossOver(parent2);
    
    // mutate
    child.mutate(MUTATION_RATE);
    
    children[i] = child;
  }
  
  
  population = children;
  
  
  generationCount++;
}





void drawFittest(DNA[] dna, int amount) {
  
  List<DNA> dnaList = Arrays.asList(dna);
  Collections.sort(dnaList, new Comparator<DNA>() {
    public int compare(DNA dna0, DNA dna1) {
      if (dna0.fitness<dna1.fitness) return 1;
      else if (dna0.fitness>dna1.fitness) return -1;
      else return 0;
    }
  });
  
  stroke(0);
  fill(0);
  text("Generation "+generationCount,10,20);
  for (int i=0;i<amount;i++) {
    String txt = nf(i+1,2)+": "+dnaList.get(i).toString();
    text(txt,10,40+16*i);
    //println(txt);
  }
  
}






boolean hasSolution() {
  for (int i=0;i<population.length;i++) {
    if (population[i].fitness==1) {
      return true;
    }
  }
  return false;
}
