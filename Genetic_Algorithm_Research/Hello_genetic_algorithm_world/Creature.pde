class Creature {


  boolean dead = false;
  PVector position = new PVector();
  float direction;
  float speed;
  PVector moveVector;
  float score;

  Creature(float direction, float speed) {
    this.direction = direction;
    this.speed = speed;
    moveVector = new PVector(0, -1);
    moveVector.rotate(radians(direction));
    moveVector.mult(speed);
    score = Float.NaN;
  }



  void setPosition(float x, float y) {
    position.set(x, y);
  }



  void move() {
    if (!dead) {
      position.add(moveVector);
    }
  }






  void draw() {
    

    pushMatrix();

    translate(position.x, position.y,1);
    if (!Float.isNaN(score)) {
      float offX = 30;
      float offY = 30;
      stroke(128);
      strokeWeight(1);
      line(0, 0, offX, offY);
      textAlign(LEFT, CENTER);
      fill(0);
      text("Score = "+nf(score, 1, 3), offX, offY);
    }
    rotate(radians(direction));


    if (!dead) {
      fill(200);
    } else {
      fill(200, 50, 50);
    }
    stroke(0);
    beginShape(TRIANGLE);
    vertex(0, -10);
    vertex(5, 10);
    vertex(-5, 10);  
    endShape();



    popMatrix();
  }
}


Creature createRandomCreature() {
  return new Creature(random(-180, 180), random(0.5, 5));
}
