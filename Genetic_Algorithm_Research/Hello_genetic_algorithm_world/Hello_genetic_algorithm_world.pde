PlayField playField;

final String MODE_GENERATE_ONE_CREATURE = "Generate a creature with random direction and speed";
final String MODE_COMPETING_CREATURES = "Competing creatures";
String mode = MODE_GENERATE_ONE_CREATURE;

int POPULATION_COUNT = 10;



void setup() {
  size(1000, 600, P3D);
  textFont(loadFont("Arial-BoldMT-12.vlw"));
  playField = new PlayField(600, 300, 100, 500);
}

void draw() {
  background(255);
  fill(0);
  textAlign(LEFT, BASELINE);
  text(mode, 10, 20);

  if (mode==MODE_GENERATE_ONE_CREATURE) {
    drawGenerateOne();
  } else if (mode==MODE_COMPETING_CREATURES) {
    drawCompetingCreatures();
  }
}


void keyPressed() {
  if (key=='1') {
    initGenerateOne();
  } else if (key=='2') {
    initCompetingCreatures();
  } else if (mode==MODE_GENERATE_ONE_CREATURE) {
    keyPressedGenerateOne();
  } else if (mode==MODE_COMPETING_CREATURES) {
    keyPressedCompetingCreatures();
  }
}
