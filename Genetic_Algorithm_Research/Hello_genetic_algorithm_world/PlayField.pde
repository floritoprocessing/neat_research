class PlayField {

  float w, h, startX, finishX;
  PlayField(float w, float h, float startX, float finishX) {
    this.w = w;
    this.h=h;
    this.startX = startX;
    this.finishX = finishX;
  }


  boolean contains(Creature creature) {
    float x = creature.position.x;
    float y = creature.position.y;
    return x>=0&&x<w&&y>=0&&y<h;
  }
  
  boolean overFinishLine(Creature creature) {
    return creature.position.x > finishX;
  }
  
  void draw() {
    stroke(0);
    strokeWeight(1);
    noFill();
    rectMode(CORNER);
    rect(0,0,w,h);
    line(startX, 0, startX, h);
    line(finishX, 0, finishX, h);
    line(finishX+4, 0, finishX+4, h);
  }
}
