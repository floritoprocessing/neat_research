void drawGene(float x, float y, Creature c) {
  float W = 100;
  float H = 40;
  rectMode(CORNER);
  stroke(0);
  strokeWeight(1);
  
  
  textAlign(CENTER,CENTER);
  
  fill(200);
  rect(x,y,W,H);
  fill(0);
  text(nf(c.speed,1,1),x+W/2,y+H/2);
  text("speed",x+W/2,y+H+15);
  
  fill(200);
  rect(x+W,y,W,H);
  fill(0);
  text(nf(c.direction,1,1),W+x+W/2,y+H/2);
  text("direction",W+x+W/2,y+H+15);
  
}
