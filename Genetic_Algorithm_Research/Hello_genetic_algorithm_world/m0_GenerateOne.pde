
Creature firstCreature = null;
boolean firstCreatureMoving = false;



/*
*
 *    GENERATE ONE
 *
 */

void initGenerateOne() {
  firstCreature = null;
  firstCreatureMoving = false;
  mode=MODE_GENERATE_ONE_CREATURE;
}

void keyPressedGenerateOne() {
  if (key==' ') {
    firstCreatureMoving = false;
    firstCreature = createRandomCreature();
    firstCreature.setPosition(width/2, height/2);
  } else if (key=='m' && firstCreature!=null) {
    firstCreatureMoving = true;
  }
  if (key=='c') {
    initCompetingCreatures();
  }
}


void drawGenerateOne() {

  fill(0);
  text("press [SPACE] to generate creature", 15, 50);

  if (firstCreature!=null) {
    text("New creature with two genes:", 15, 80);
    text("a direction gene with value "+nf(firstCreature.direction, 1, 1)+" degrees", 15, 110);
    text("a speed gene with value "+nf(firstCreature.speed, 1, 1), 15, 125);
    text("DNA: ", 20, 160);
    if (!firstCreatureMoving) text("Hit [m] to see it move!", 15, 240);    
    drawGene(60, 140, firstCreature);
    firstCreature.draw();
    if (firstCreatureMoving) {
      firstCreature.move();
    }
  }
  fill(0);
  textAlign(LEFT, BASELINE);
  text("Hit [c] to see multiple creatures compete!", 15, height-20);
}
