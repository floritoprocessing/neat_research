
Creature[] generation = null;
boolean creaturesMoving = false;
boolean allCreaturesDead = false;
boolean scoreEvaluation = false;


/*
*
 *    COMPETING CREATURES
 *
 */


void initCompetingCreatures() {
  mode=MODE_COMPETING_CREATURES;
  generation = null;
  creaturesMoving = false;
  allCreaturesDead = false;
  scoreEvaluation = false;
}

void keyPressedCompetingCreatures() {
  if (key=='m') {
    creaturesMoving=true;
  }
  if (key==' ') {
    creaturesMoving = false;
    generation=null;
    allCreaturesDead=false;
    scoreEvaluation = false;
  }


  if (allCreaturesDead && key=='e') {
    scoreEvaluation = true;
  }
}


void drawCompetingCreatures() {
  if (generation==null) {
    generation = new Creature[POPULATION_COUNT];
    for (int i=0; i<POPULATION_COUNT; i++) {
      generation[i] = createRandomCreature();
      float x = playField.startX;
      float y = map(i, -1, POPULATION_COUNT, 0, playField.h);
      generation[i].setPosition(x, y);
    }
  }
  text("A playfield with "+generation.length+" creatures:", 15, 50);
  text("press [SPACE] to reset", 15, 80);

  if (!creaturesMoving) text("Hit [m] to see them move and compete!", 15, 110);

  if (creaturesMoving) {

    // move all creatures;
    for (int i=0; i<generation.length; i++) {
      Creature creature = generation[i];
      creature.move();
      if (!playField.contains(creature)) {
        creature.dead = true;
      }
      if (playField.overFinishLine(creature)) {
        creature.dead = true;
      }
    }

    // check if they are all dead
    if (!allCreaturesDead) {
      boolean allDead = true;
      for (int i=0; i<generation.length; i++) {
        Creature creature = generation[i];
        allDead = allDead && creature.dead;
      }
      if (allDead) allCreaturesDead=true;
    }

    if (allCreaturesDead && !scoreEvaluation) {
      fill(0);
      text("All creatures are dead.\nHit [e] to evaluate their score", 15, 110);
    }

    if (scoreEvaluation) {
      fill(0);
      text("Creature scores are evaluated by\nhow far they are from the finish line.", 15, 110);
      for (int i=0; i<generation.length; i++) {
        Creature creature = generation[i];
        float cx = creature.position.x;
        float flx = playField.finishX;
        float score = map(abs(cx-flx), 0, playField.finishX, 1, 0);
        creature.score = score;
      }
    }
  }

  pushMatrix();
  translate(300, 50);
  playField.draw();
  for (int i=0; i<generation.length; i++) {
    Creature creature = generation[i];
    creature.draw();
  }
  popMatrix();
}
