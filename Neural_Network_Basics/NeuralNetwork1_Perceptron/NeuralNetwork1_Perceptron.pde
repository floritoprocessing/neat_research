/*
@see https://natureofcode.com/book/chapter-10-neural-networks/
 
 Let' try to have a perceptron guess if a point is on the right-top of a line or the left-bottom of a line
 */

int AREA_WIDTH = 320;
int AREA_HEIGHT = 320;
float BIAS = 1;

Perceptron p;
Trainer t;
PVector[] randomPoints;
int trainingRounds = 0;

void setup() {
  size(800, 400);
  resetAll();
}

void resetAll() {
  p = new Perceptron(3);
  t = null;
  trainingRounds = 0;
}

void draw() {
  background(255);
  drawScheme();
  drawPerceptron(width-100, height/2);

  textAlign(LEFT, BASELINE);
  text("Press [c] to reset all", width/2+10, 20);
  text("Press [t] to train one round", width/2+10, 35);
  text("Press [T] to train ten round", width/2+10, 50);
  drawLastTraining();
}

/*
   1. Provide the perceptron with inputs for which there is a known answer.
 2. Ask the perceptron to guess an answer
 3. Compute the error. (Did it get the answer right or wrong?)
 4. Adjust all the weights according to the error.
 5. Return to Step 1 and repeat!
 */
void train() {

  //1 create known answer
  float x = random(AREA_WIDTH);
  float y = random(AREA_HEIGHT);
  t = new Trainer(x, y, x>=y?1:-1);

  //2 guess answer
  //3 computer the error
  //4 adjust weights
  p.train(t.inputs, t.answer);
  
  trainingRounds++;
}




void keyPressed() {
  if (key=='c') {
    resetAll();
  } else if (key=='t') {
    train();
    makeRandomPoints();
    testRandomPoints();
  } else if (key=='T') {
    for (int i=0;i<10;i++)
      train();
    makeRandomPoints();
    testRandomPoints();
  }
}


void makeRandomPoints() {
  randomPoints = new PVector[200];
  float extraX = (width/2-AREA_WIDTH)/2;
  float extraY = (height-AREA_HEIGHT)/2;
  for (int i=0; i<randomPoints.length; i++) {
    randomPoints[i] = new PVector(random(-extraX,AREA_WIDTH+extraX), random(-extraY,AREA_HEIGHT+extraY));
  }
}

void testRandomPoints() {
  for (int i=0; i<randomPoints.length; i++) {
    int result = p.feedforward(new float[] {
      randomPoints[i].x, 
      randomPoints[i].y, 
      BIAS
      });
    randomPoints[i].z = result;
  }
}

void drawLastTraining() {
  if (t!=null) {
    textAlign(LEFT, BASELINE);
    fill(0);
    text("Training rounds: "+trainingRounds,width/2+10,80);
    fill(128, 0, 0);
    text("last trainer: "
      +nf(t.inputs[0], 1, 1)+" / "+nf(t.inputs[1], 1, 1)+" / "+t.inputs[2]+" -> "+t.answer
      +" ("+(t.answer>0?"+":"-")+")"
      , width/2+10, 95);
    

    ellipseMode(RADIUS);
    
    //println(randomPoints.length);
    
    pushMatrix();
    translate((width/2-AREA_WIDTH)/2,(height-AREA_HEIGHT)/2);
    
    strokeWeight(15);
    stroke(128, 0, 0);
    point(t.inputs[0], t.inputs[1]);
    
    strokeWeight(1);
    for (int i=0; i<randomPoints.length; i++) {
      float x = randomPoints[i].x;
      float y = randomPoints[i].y;
      color col = color(0);
      if (x<0||x>=AREA_WIDTH||y<0||y>=AREA_HEIGHT)
        col = color(0,64);
      int result = (int)randomPoints[i].z;
      if (result==1) {
        stroke(col);
        noFill();
      } else {
        noStroke();
        fill(col);
      }
      //println(x+"/"+y);
      ellipse(x, y, 5, 5);
    }
    popMatrix();
  }
}



void drawScheme() {
  pushMatrix();
  translate((width/2-AREA_WIDTH)/2,(height-AREA_HEIGHT)/2);
  strokeWeight(1);
  stroke(0);
  noFill();
  rect(0,0,AREA_WIDTH,AREA_HEIGHT);
  line(0, 0, AREA_WIDTH, AREA_HEIGHT);
  textAlign(CENTER, CENTER);
  fill(0);
  text("(+)", AREA_WIDTH-20, 20);
  text("(-)", 20, AREA_HEIGHT-20);
  popMatrix();
}

void drawPerceptron(float sx, float sy) {
  float radius = 40;
  float lineLength = 150;

  ellipseMode(RADIUS);
  stroke(0);
  noFill();
  

  pushMatrix();
  translate(sx, sy);

  noFill();
  ellipse(0, 0, radius, radius);

  fill(0);
  for (int i=0; i<3; i++) {
    pushMatrix();
    rotate(map(i,0,2,radians(20),radians(-20)));
    line(-radius, 0, -radius-lineLength, 0);
    textAlign(CENTER, TOP);
    text("weight="+nf(p.weights[i],1,4), (-radius-radius-lineLength)/2, 0);
    textAlign(RIGHT,CENTER);
    //
    translate(-radius-lineLength,0);
    String txt = i==0 ? "X" : (i==1 ? "Y" : "BIAS");
    rotate(-map(i,0,2,radians(20),radians(-20)));
    text(txt,-20,0);
    popMatrix();
  }

  popMatrix();
}
