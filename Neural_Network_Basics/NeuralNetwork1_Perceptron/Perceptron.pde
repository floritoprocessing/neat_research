class Perceptron {

  float LEARNING_RATE = 0.01;
  float[] weights;

  Perceptron(int n) {
    weights = new float[n];
    for (int i=0; i<n; i++) {
      weights[i] = random(-1, 1);
    }
  }


  // Step 1: Provide the inputs and known answer.
  // These are passed in as arguments to train().
  // The input is 3 values: x,y and bias.
  void train(float[] inputs, int desired) {

    // Step 2: Guess according to those inputs.
    int guess = feedforward(inputs);

    // Step 3: Compute the error (difference
    // between answer and guess).
    float error = desired - guess;

    //[full] Step 4: Adjust all the weights according
    // to the error and learning constant.
    for (int i = 0; i < weights.length; i++) {
      weights[i] += LEARNING_RATE * error * inputs[i];
    }
    //[end]
  }

  int feedforward(float[] inputs) {
    float sum = 0;
    for (int i = 0; i < weights.length; i++) {
      sum += inputs[i]*weights[i];
    }
    // Result is the sign of the sum, -1 or +1.
    // Here the perceptron is making a guess.
    // Is it on one side of the line or the other?
    return activate(sum);
  }

  // The activation function (sign)
  int activate(float sum) {
    if (sum > 0) return 1;
    else return -1;
  }
}
