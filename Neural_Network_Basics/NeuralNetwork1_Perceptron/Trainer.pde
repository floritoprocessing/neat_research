class Trainer {
  
  float[] inputs;
  int answer;
  
  Trainer(float x, float y, int answer) {
    inputs = new float[3];
    inputs[0] = x;
    inputs[1] = y;
    inputs[2] = BIAS; // bias
    this.answer = answer;
  }
}
