/*
@see https://natureofcode.com/book/chapter-10-neural-networks/
 
 Let' try to have a perceptron guess if a point is on the right-top of a line or the left-bottom of a line
 */

int AREA_WIDTH = 320;
int AREA_HEIGHT = 320;
float BIAS = 1;
int RANDOM_POINT_AMOUNT = 3000;

Perceptron p;
Trainer t;
PVector[] randomPoints;
int trainingRounds = 0;

void setup() {
  size(800, 400);
  resetAll();
}

void resetAll() {
  p = new Perceptron(3);
  t = null;
  trainingRounds = 0;
  createTrainingLineFunction();
}

void draw() {
  background(255);
  
  trainAndShowResults(100);
  
  drawScheme();
  drawPerceptron(width-100, height/2);
  

  textAlign(LEFT, BASELINE);
  text("Press [c] to reset all", width/2+10, 20);
  text("Press [t] to train one round", width/2+10, 35);
  text("Press [T] to train ten round", width/2+10, 50);
  text("Press [p] for new Perceptron",width/2+10,65);
  drawLastTraining();
}




void keyPressed() {
  if (key=='c') {
    resetAll();
  } else if (key=='t') {
    trainAndShowResults(1);
  } else if (key=='T') {
    trainAndShowResults(10);
  } else if (key=='p') {
    p = new Perceptron(3);
  }
}


void trainAndShowResults(int amount) {
  train(amount);
  makeRandomPoints();
  testRandomPoints();
}


/*
   1. Provide the perceptron with inputs for which there is a known answer.
 2. Ask the perceptron to guess an answer
 3. Compute the error. (Did it get the answer right or wrong?)
 4. Adjust all the weights according to the error.
 5. Return to Step 1 and repeat!
 */
void train(int amount) {

  for (int i=0; i<amount; i++) {
    //1 create known answer
    float x = random(AREA_WIDTH);
    float y = random(AREA_HEIGHT);
    int answer = y > lineFunction(x) ? -1 : 1;
    t = new Trainer(x, y, answer);

    //2 guess answer
    //3 computer the error
    //4 adjust weights
    p.train(t.inputs, t.answer);

    trainingRounds++;
  }
}





void makeRandomPoints() {
  randomPoints = new PVector[RANDOM_POINT_AMOUNT];
  float extraX = (width/2-AREA_WIDTH)/2;
  float extraY = (height-AREA_HEIGHT)/2;
  for (int i=0; i<randomPoints.length; i++) {
    randomPoints[i] = new PVector(random(-extraX, AREA_WIDTH+extraX), random(-extraY, AREA_HEIGHT+extraY));
  }
}

void testRandomPoints() {
  for (int i=0; i<randomPoints.length; i++) {
    int result = p.feedforward(new float[] {
      randomPoints[i].x, 
      randomPoints[i].y, 
      BIAS
      });
    randomPoints[i].z = result;
  }
}
