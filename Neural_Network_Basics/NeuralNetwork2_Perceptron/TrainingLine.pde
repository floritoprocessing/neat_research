float A, B;

void createTrainingLineFunction() {
  A = random(-3,3);
  B = random(0.3,0.7)*AREA_HEIGHT;
}

float lineFunction(float x) {
  return A*x+B;
}
