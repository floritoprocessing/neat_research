
void drawLastTraining() {
  if (t!=null) {
    textAlign(LEFT, BASELINE);
    fill(0);
    text("Training rounds: "+trainingRounds,width/2+10,80);
    fill(128, 0, 0);
    text("last trainer: "
      +nf(t.inputs[0], 1, 1)+" / "+nf(t.inputs[1], 1, 1)+" / "+t.inputs[2]+" -> "+t.answer
      +" ("+(t.answer>0?"+":"-")+")"
      , width/2+10, 95);
    

    ellipseMode(RADIUS);
    
    //println(randomPoints.length);
    
    pushMatrix();
    translate((width/2-AREA_WIDTH)/2,(height-AREA_HEIGHT)/2);
    
    strokeWeight(15);
    stroke(128, 0, 0);
    point(t.inputs[0], t.inputs[1]);
    
    strokeWeight(1);
    for (int i=0; i<randomPoints.length; i++) {
      float x = randomPoints[i].x;
      float y = randomPoints[i].y;
      float opacity = (x<0||x>=AREA_WIDTH||y<0||y>=AREA_HEIGHT) ? 128 : 255;
      color col;
      int result = (int)randomPoints[i].z;
      if (result==1) {
        col = color(0,180,0,opacity);
      } else {
        col = color(0,0,255,opacity);
      }
      noStroke();
      fill(col);
      //println(x+"/"+y);
      ellipse(x, y, 5, 5);
    }
    popMatrix();
  }
}



void drawScheme() {
  pushMatrix();
  translate((width/2-AREA_WIDTH)/2,(height-AREA_HEIGHT)/2);
  strokeWeight(1);
  stroke(0);
  noFill();
  rect(0,0,AREA_WIDTH,AREA_HEIGHT);
  float y0 = lineFunction(0);
  float y1 = lineFunction(AREA_WIDTH);
  strokeWeight(5);
  line(0, y0, AREA_WIDTH, y1);
  popMatrix();
}


void drawPerceptron(float sx, float sy) {
  float radius = 40;
  float lineLength = 150;

  ellipseMode(RADIUS);
  stroke(0);
  noFill();
  strokeWeight(1);

  pushMatrix();
  translate(sx, sy);

  noFill();
  ellipse(0, 0, radius, radius);

  fill(0);
  for (int i=0; i<3; i++) {
    pushMatrix();
    rotate(map(i,0,2,radians(20),radians(-20)));
    line(-radius, 0, -radius-lineLength, 0);
    textAlign(CENTER, TOP);
    text("weight="+nf(p.weights[i],1,4), (-radius-radius-lineLength)/2, 0);
    textAlign(RIGHT,CENTER);
    //
    translate(-radius-lineLength,0);
    String txt = i==0 ? "X" : (i==1 ? "Y" : "BIAS");
    rotate(-map(i,0,2,radians(20),radians(-20)));
    text(txt,-20,0);
    popMatrix();
  }

  popMatrix();
}
