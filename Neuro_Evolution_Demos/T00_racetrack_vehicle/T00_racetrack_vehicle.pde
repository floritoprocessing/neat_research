int MODE_CREATE_GAME = 0;
int MODE_PLAY_GAME = 1;
int mode = MODE_PLAY_GAME;
int lastTime;
RaceGame raceGame;

void setup() {
  size(1000, 600);
  lastTime = millis();
  raceGame = new RaceGame();
  if (mode==MODE_CREATE_GAME) raceGame.field.initMakePlayfield();
}

void mousePressed() {
  if (mode==MODE_CREATE_GAME) raceGame.field.addPoint(mouseX, mouseY);
}

void keyPressed() {
  if (mode==MODE_CREATE_GAME) {
    if (key=='f') {
      raceGame.field.finalize();
      mode = MODE_PLAY_GAME;
    }
  } else if (mode==MODE_PLAY_GAME) {
    if (key=='r') {
      raceGame.restart();
    }
    if (key==CODED) {
      if (keyCode==LEFT) raceGame.vehicle.turnLeft(true);
      else if (keyCode==RIGHT) raceGame.vehicle.turnRight(true);
      else if (keyCode==UP) raceGame.vehicle.increaseSpeed(true);
      else if (keyCode==DOWN) raceGame.vehicle.decreaseSpeed(true);
    }
  }
}

void keyReleased() {
  if (mode==MODE_PLAY_GAME) {
    if (keyCode==LEFT) raceGame.vehicle.turnLeft(false);
    else if (keyCode==RIGHT) raceGame.vehicle.turnRight(false);
    else if (keyCode==UP) raceGame.vehicle.increaseSpeed(false);
    else if (keyCode==DOWN) raceGame.vehicle.decreaseSpeed(false);
  }
}


void draw() {
  int ti = millis();
  float frameTime = (ti-lastTime)/1000.0;

  background(255);
  fill(255, 0, 0);
  text(frameTime, 10, 20);
  raceGame.integrate(frameTime);
  raceGame.draw();

  lastTime = ti;
}
