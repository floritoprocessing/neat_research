class Network {

  boolean DEBUG = true;
  float BIAS = 1;
  int layerCount;

  Neuron[][] neurons;

  Network(int... neuronsPerLayer) {

    layerCount = neuronsPerLayer.length;
    neurons = new Neuron[layerCount][];

    for (int layerIndex=0; layerIndex<layerCount; layerIndex++) {

      if (DEBUG) println("layer "+layerIndex+" -------------------");
      int neuronCount = neuronsPerLayer[layerIndex];
      neurons[layerIndex] = new Neuron[neuronCount];

      // layer 0 : just value neurons
      if (layerIndex==0) {
        for (int j=0; j<neuronCount; j++) {
          neurons[layerIndex][j] = new ValueNeuron("n"+layerIndex+""+j, 0);//inputCount);
        }
      } else {
        int prevLayerIndex = layerIndex-1;
        int prevLayerNeuronCount = neuronsPerLayer[prevLayerIndex];
        for (int j=0; j<neuronCount; j++) {
          ActiveNeuron neuron = new ActiveNeuron("n"+layerIndex+""+j, BIAS);//inputCount);
          neurons[layerIndex][j] = neuron; // assign to array
          for (int prevN=0; prevN<prevLayerNeuronCount; prevN++) {
            neuron.addConnection( neurons[prevLayerIndex][prevN] );
          }
          if (DEBUG) println(neuron+" has "+neuron.inputs.length+" inputs");
        }
      }
    }
  }






  void step(float[] inputValues) {


    for (int layer=0; layer<layerCount; layer++) {

      Neuron[] layerNeurons = neurons[layer];

      if (layer==0) {
        for (int i=0; i<layerNeurons.length; i++) {
          ValueNeuron vn = (ValueNeuron)layerNeurons[i];
          vn.value = inputValues[i];
        }
      } else {
        for (int i=0; i<layerNeurons.length; i++) {
          ActiveNeuron an = (ActiveNeuron)layerNeurons[i];
          an.feedforward();
        }
      }
    }
  }





  float[] getLastLayerOutputs() {
    Neuron[] layerNeurons = neurons[layerCount-1];
    float[] output = new float[layerNeurons.length];
    for (int i=0; i<layerNeurons.length; i++) {
      ActiveNeuron an = (ActiveNeuron)layerNeurons[i];
      output[i] = an.getNextOutput();
    }
    return output;
  }






  void draw(float diameter, float spaceX, float spaceY) {

    ellipseMode(DIAMETER);
    stroke(0);
    noFill();


    for (int renderPass=0; renderPass<2; renderPass++) {
      float posX = 0;

      for (int layer=0; layer<layerCount; layer++) {

        Neuron[] layerNeurons = neurons[layer];
        int neuronAmount = layerNeurons.length;
        for (int n=0; n<neuronAmount; n++) {

          float totalY = (neuronAmount-1)*spaceY;
          float posY=0;
          if (neuronAmount>1)
            posY = map(n, 0, neuronAmount-1, -totalY/2, totalY/2);

          if (renderPass==0) 
            layerNeurons[n].drawInputs(posX, posY, diameter);
          else
            layerNeurons[n].drawBody(posX, posY, diameter);
        }

        posX += spaceX;
      }
    }
  }
}
