interface Neuron {
  float getNextOutput();
  String getName();
  void drawBody(float posX, float posY, float diameter);
  void drawInputs(float posX, float posY, float diameter);
}




abstract class DrawableNeuron implements Neuron {

  float drawX = Float.NaN, drawY=Float.NaN;

  void drawBody(float posX, float posY, float diameter) {
    this.drawX = posX;
    this.drawY = posY;
    ellipseMode(DIAMETER);
    strokeWeight(1);
    stroke(0);
    fill(200);
    ellipse(posX, posY, diameter, diameter);

    fill(0);
    textAlign(LEFT, CENTER);
    text(nf(getNextOutput(), 1, 1), posX, posY);
  }

  void drawInputs(float posX, float posY, float diameter) {

    //println(getName()+" drawInputs");
    if (this instanceof ActiveNeuron) {
      ActiveNeuron neuron = (ActiveNeuron)this;
      int inputCount = neuron.inputs.length;
      float maxW = 1.0;
      for (int i=0; i<inputCount; i++) {
        
        float r = min(255,max(0,map(neuron.weights[i],-maxW,maxW,0,255)));
        float g = min(255,max(0,map(neuron.weights[i],-maxW,maxW,255,0)));
        float sw = map(abs(neuron.weights[i]),0,maxW,0,5);
        int col = color(r,g,255);
        float w = neuron.weights[i];
        if (w<0) {
          // red -> almost white
          float p = map(w,-maxW,0,255,0);
          col = color(255,255-p*0.9,255-p*0.9);
        } else {
          // green -> almost white
          float p = map(w,maxW,0,255,0);
          col = color(255-p*0.9,255,255-p*0.9);
        }
        DrawableNeuron inputNeuron = (DrawableNeuron)neuron.inputs[i];
        //if (getName().equals("n20")) println(inputNeuron+" at "+inputNeuron.drawX+"/"+inputNeuron.drawY+" to "+posX+"/"+posY);
        if (Float.isNaN(inputNeuron.drawX)) {
          inputNeuron.drawBody( posX - diameter/2, posY + diameter, diameter/2 );
          strokeWeight(sw);
          stroke(col);
          line(inputNeuron.drawX, inputNeuron.drawY, posX, posY);
          inputNeuron.drawX = Float.NaN;
        } else {
          strokeWeight(sw);
          stroke(col);
          line(inputNeuron.drawX, inputNeuron.drawY, posX, posY);
        }
      }
    }
  }
}






class ValueNeuron extends DrawableNeuron {  

  boolean DEBUG = true;

  String name;
  float value;

  ValueNeuron(String name, float value) {
    this.name = name;
    this.value = value;
    if (DEBUG) println("created "+this);
  }
  float getNextOutput() {
    return value;
  }


  String getName() {
    return name;
  }
  public String toString() {
    return "ValueNeron("+name+")";
  }
}

class ActiveNeuron extends DrawableNeuron {

  boolean DEBUG = true;

  String name;
  float bias;
  Neuron[] inputs = null;
  float[] weights = null;
  float nextOutput;

  ActiveNeuron(String name, float bias) {
    this.name = name;
    // create one input: bias with weight
    this.inputs = new Neuron[] { new ValueNeuron("bias", bias) };
    this.weights = new float[] { randomWeight() };
    if (DEBUG) println("created "+this);
  }


  void addConnection(Neuron input) {
    increaseInputsAndWeights();
    inputs[inputs.length-1] = input;
    weights[weights.length-1] = randomWeight();
    if (DEBUG) println(" added input from "+input.getName());
  }

  float getNextOutput() {
    return nextOutput;
  }



  float randomWeight() {
    return random(-1.0, 1.0);
  }

  void feedforward() {
    float sum = 0;
    for (int i = 0; i < weights.length; i++) {
      sum += inputs[i].getNextOutput()*weights[i];
    }
    nextOutput = activate(sum);
  }

  float activate(float sum) {
    if (sum > 0) return 1;
    else return -1;
  }

  void increaseInputsAndWeights() {
    Neuron[] newInputs = new Neuron[inputs.length+1];
    arrayCopy(inputs, 0, newInputs, 0, inputs.length);
    inputs = newInputs;
    float[] newWeights = new float[weights.length+1];
    arrayCopy(weights, 0, newWeights, 0, weights.length);
    weights = newWeights;
  }

  String getName() {
    return name;
  }
  public String toString() {
    return "Neuron ("+name+")";
  }
}
