int MODE_CREATE_GAME = 0;
int MODE_PLAY_GAME = 1;
int MODE_PLAY_BRAIN = 2;
int mode = MODE_PLAY_GAME;
int lastTime;
RaceGame raceGame;
Network network;
NetworkToVehicle networkToVehicle = new NetworkToVehicle();
Trainer trainer = new Trainer();

void setup() {
  size(1000, 600);
  
  raceGame = new RaceGame();
  resetNetwork();
  if (mode==MODE_CREATE_GAME) raceGame.field.initMakePlayfield();
  
  lastTime = millis();
}

void resetNetwork() {
  network = new Network(3, 5, 4);
}

void mousePressed() {
  if (mode==MODE_CREATE_GAME) raceGame.field.addPoint(mouseX, mouseY);
}

void keyPressed() {
  if (mode==MODE_CREATE_GAME) {
    if (key=='f') {
      raceGame.field.finalize();
      mode = MODE_PLAY_GAME;
    }
  } else if (mode==MODE_PLAY_GAME) {
    if (key=='2') mode=MODE_PLAY_BRAIN;
    if (key=='r') {
      raceGame.restart();
    }
    if (key=='r') {
      resetNetwork();
    }
    if (key==CODED) {
      if (keyCode==LEFT) raceGame.vehicle.turnLeft(true);
      else if (keyCode==RIGHT) raceGame.vehicle.turnRight(true);
      else if (keyCode==UP) raceGame.vehicle.increaseSpeed(true);
      else if (keyCode==DOWN) raceGame.vehicle.decreaseSpeed(true);
    }
  }
  
  else if (mode==MODE_PLAY_BRAIN) {
    if (key=='1') {
      raceGame.restart();
      networkToVehicle.reset();
      mode=MODE_PLAY_GAME;
    }
    if (key=='r') {
      resetNetwork();
      raceGame.restart();
      networkToVehicle.reset();
      trainer.nextRound();
    }
  }
}

void keyReleased() {
  if (mode==MODE_PLAY_GAME) {
    if (keyCode==LEFT) raceGame.vehicle.turnLeft(false);
    else if (keyCode==RIGHT) raceGame.vehicle.turnRight(false);
    else if (keyCode==UP) raceGame.vehicle.increaseSpeed(false);
    else if (keyCode==DOWN) raceGame.vehicle.decreaseSpeed(false);
  }
}


void draw() {
  int ti = millis();
  float frameTime = 0.016;//(ti-lastTime)/1000.0;
  
  background(255);
  fill(0);
  //text(frameTime, 10, 20);
  
  if (mode==MODE_PLAY_BRAIN) {
    text("hit [r] to restart brain & game. time="+trainer.deltaTime(),10,20);
    networkToVehicle.update();
  }
  
  raceGame.integrate(frameTime);
  
  // brain
  boolean[] sensorActives = raceGame.vehicle.inSensorVecRange;
  float[] inputValues = new float[sensorActives.length];
  for (int i=0;i<inputValues.length;i++)
    inputValues[i] = sensorActives[i] ? 1 : -1;
  network.step(inputValues);
  
  raceGame.draw();

  lastTime = ti;
  
  translate(width/2+100,height/2);
  network.draw(40, 150, 80);
  
  if (mode==MODE_PLAY_BRAIN) {
    networkToVehicle.influenceVehicle();
  }
  //noLoop();
}
