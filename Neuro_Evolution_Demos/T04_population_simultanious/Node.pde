class Node {
  
  final int ID;
  final boolean biasNode;
  final String name;
  final String asString;
  
  float drawX=Float.NaN, drawY=Float.NaN;
  
  // are created during NodeBrain creation
  private ArrayList<NodeConnection> _inputConnections = new ArrayList<NodeConnection>();
  private ArrayList<NodeConnection> _outputConnections = new ArrayList<NodeConnection>();
  
  private float _feedForwardResult = 0;
  
  Node clone() {
    return new Node(ID, biasNode, name);
  }
  
  Node(int uniqueID, boolean biasNode, String name) {
    this.ID = uniqueID;
    this.biasNode = biasNode;
    if (biasNode) {
      _feedForwardResult = Z.BIAS;
    }
    this.name = name;
    this.asString = name+"("+ID+")";//ID+" ("+name+")";
  }
  
  
  
  // only used during NodeConnection creation
  void addInput(NodeConnection connection) {
    //println(ID+" Adding input to "+name);
    _inputConnections.add(connection);
  }
  void addOutput(NodeConnection connection) {
    //println(ID+" Adding output to "+name);
    _outputConnections.add(connection);
  }
  
  void calculateFeedForwardResult() {
    float sum = 0;
    for (NodeConnection input:_inputConnections) {
      sum += input.source.getFeedForwardResult() * input.weight;
    }
    _feedForwardResult = activate(sum);
  }
  
  // only for input neurons
  void setFeedForwardResult(float v) {
    _feedForwardResult = v;
  }
  
  float getFeedForwardResult() {
    return _feedForwardResult;
  }
  
  float activate(float val) {
    //if (val > 0) return 1;
    //else return -1;
    return sigmoid(val);
  }
  
  float sigmoid(float val) {
    float out = (float)(1.0 / (1+Math.pow(Math.E,-val))); // 0..1
    out = (out*2)-1; // -1..1;
    return out;
  }
  
  
  boolean hasInputConnections() {
    return _inputConnections.size()>0; 
  }
  
  public String toString() {
    return asString;
  }
  
  
  
  
  /*
  *
  *    SERIALISATION
  *
  */
  
  final static String KEY_ID = "ID";
  final static String KEY_BIAS_NODE = "biasNode";
  final static String KEY_NAME = "NAME";
  
  
  Node(JSONObject json) {
    this(json.getInt(KEY_ID), json.getBoolean(KEY_BIAS_NODE), json.getString(KEY_NAME));
  }
  
  public JSONObject toJSON() {
    JSONObject json = new JSONObject();
    json.setInt(KEY_ID,ID);
    json.setBoolean(KEY_BIAS_NODE,biasNode);
    json.setString(KEY_NAME,name);
    return json;
  }
  
}



Node getNodeByID(ArrayList<Node> nodes, int ID) {
  ArrayList<Node> out = new ArrayList<Node>();
  for (Node node:nodes) {
    if (ID==node.ID) out.add(node);
  }
  if (out.size()==0) throw new RuntimeException("Node with ID="+ID+" not found");
  else if (out.size()>1) throw new RuntimeException("Node with ID="+ID+" exists "+out.size()+" times!");
  else {
    return out.get(0);
  }
}
