/*
  *
 *    DRAWING
 *
 */


void drawNodeBrain(NodeBrain nodeBrain, float nwidth, float nheight) {

  ArrayList<ArrayList<Node>> neurons = nodeBrain._feedForwardOrder;
  //float diameter, float spaceX, float spaceY

  //nwidth = (layerCount+1)*spaceX + layerCount*diameter;
  //nwidth - layerCount*diameter = (layerCount+1)*spaceX;
  //spaceX = (nwidth - layerCount*diameter)/(layerCount+1)

  float diameter = Z.DRAW_NEURON_DIAMETER;

  int layerCount = neurons.size();

  int maxNeuronsPerLayer = 0;
  for (ArrayList<Node> layerNeurons : neurons) {
    maxNeuronsPerLayer = max(maxNeuronsPerLayer, layerNeurons.size());
  }

  float spaceX = (nwidth - layerCount*diameter)/(layerCount+1);
  float spaceY = (nheight - maxNeuronsPerLayer*diameter)/(maxNeuronsPerLayer+1);

  ellipseMode(DIAMETER);


  for (int renderPass=0; renderPass<2; renderPass++) {
    float posX = spaceX;

    for (int layer=0; layer<layerCount; layer++) {

      ArrayList<Node> layerNeurons = neurons.get(layer);
      int neuronAmount = layerNeurons.size();

      for (int n=0; n<neuronAmount; n++) {

        float thisLayerTotalY = neuronAmount*diameter + (neuronAmount-1)*spaceY; 
        float posY=nheight/2;

        if (neuronAmount>1)
          posY = nheight/2 + map(n, 0, neuronAmount-1, -thisLayerTotalY/2, thisLayerTotalY/2);

        if (renderPass==0) 
          drawNodeInputs(layerNeurons.get(n), posX, posY, diameter);
        else {
          String extraText = "";
          if (layer==0) extraText = " ("+nf( layerNeurons.get(n).getFeedForwardResult(), 1, 2 )+")";
          drawNodeBody(layerNeurons.get(n), posX, posY, diameter, layer==0?LEFT:(layer==layerCount-1?RIGHT:Integer.MIN_VALUE), extraText);
        }
      }

      posX += spaceX;
    }
  }
}

void drawNodeBody(Node node, float posX, float posY, float diameter, int textPosition, String extraText) {
  node.drawX = posX;
  node.drawY = posY;
  ellipseMode(DIAMETER);
  strokeWeight(1);
  stroke(Z.NEURON_BODY_STROKE);
  fill(Z.NEURON_BODY_FILL);
  ellipse(posX, posY, diameter, diameter);

  fill(Z.NEURON_TEXT_COLOR);
  textAlign(LEFT, CENTER);
  text(nf(node.getFeedForwardResult(), 1, 1)+extraText, (int)posX, (int)posY);

  if (textPosition==LEFT) {
    textAlign(RIGHT, BASELINE);
    text(node.name+extraText, (int)(posX-diameter/2-5), (int)posY);
  } else if (textPosition==RIGHT) {
    textAlign(LEFT, BASELINE);
    text(node.name+extraText, (int)(posX+diameter/2+5), (int)posY);
  }
}

void drawNodeInputs(Node node, float posX, float posY, float diameter) {

  //println(getName()+" drawInputs");
  
  int inputCount = node._inputConnections.size();//neuron.inputs.length;
  for (int i=0; i<inputCount; i++) {
    
    NodeConnection nc = node._inputConnections.get(i);
    //float wsq = sqrt(abs(neuron.weights[i]));
    float r = min(255, max(0, map(nc.weight, -1, 0, 255, 0)));
    float g = min(255, max(0, map(nc.weight, 0, 1, 0, 255)));
    float b = 0;

    float sw = map(abs(nc.weight), 0, 1, 1, Z.DRAW_NEURON_WEIGHT_TO_STROKE_WEIGHT);
    if (nc.weight==0) {
      sw=0;
    }
    sw = min(Z.DRAW_NEURON_STROKE_WEIGHT_MAX, sw);

    int col = color(r, g, b);
    
    //DrawableNeuron inputNeuron = (DrawableNeuron)neuron.inputs[i];
    //if (getName().equals("n20")) println(inputNeuron+" at "+inputNeuron.drawX+"/"+inputNeuron.drawY+" to "+posX+"/"+posY);
    
    Node inputNode = nc.source;
    
    if (Float.isNaN(inputNode.drawX)) {
      drawNodeBody( inputNode, posX - diameter/2, posY + diameter, diameter/2, Integer.MIN_VALUE, "" );
      stroke(col);
      strokeWeight(sw);
      if (sw>0) line(inputNode.drawX, inputNode.drawY, posX, posY);
      inputNode.drawX = Float.NaN;
    } else {
      stroke(col);
      strokeWeight(sw);
      if (sw>0) line(inputNode.drawX, inputNode.drawY, posX, posY);
    }
  }
}
