import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.*;

int GLOBAL_MODE_BREEDING = 1;
int GLOBAL_MODE_CREATE_PLAYFIELD = 0;
int mode = GLOBAL_MODE_BREEDING;

Generation generation;
PlayField drawField = null;
Zettings Z;
int targetFramerate = 60;

StreamingGraph streamingGraph;

void setup() {
  size(1200, 650, P3D);
  Z = new Zettings();
  
  streamingGraph = new StreamingGraph(Z.POPULATION_COUNT);

  frameRate(targetFramerate);

  if (mode==GLOBAL_MODE_BREEDING) initBreeding(null);
  else if (mode==GLOBAL_MODE_CREATE_PLAYFIELD) initCreatePlayfield();
}

void initCreatePlayfield() {
  drawField = new PlayField();
  drawField.initMakePlayfield();
}

void initBreeding(PlayField field) {
  streamingGraph.reset(Z.POPULATION_COUNT);
  generation = new Generation(field);
}

void mousePressed() {
  if (mode==GLOBAL_MODE_CREATE_PLAYFIELD) {
    drawField.addPoint(mouseX, mouseY);
  }
}

void keyPressed() {
  
  if (key=='@' && mode!=GLOBAL_MODE_CREATE_PLAYFIELD) {
    mode = GLOBAL_MODE_CREATE_PLAYFIELD;
    initCreatePlayfield();
  } else if (key=='!' && mode!=GLOBAL_MODE_BREEDING) {
    mode = GLOBAL_MODE_BREEDING;
    if (!drawField.isInTheMaking()) {
      initBreeding(drawField);
    } else {
      initBreeding(null);
    }
  }

  if (mode==GLOBAL_MODE_BREEDING) {
    generation.keyPressed();
    if (key=='-' && targetFramerate>=120) {
      targetFramerate-=60;
      frameRate(targetFramerate);
    }
    else if (key=='+') {
      targetFramerate+=60;
      frameRate(targetFramerate);
    }
  } else if (mode==GLOBAL_MODE_CREATE_PLAYFIELD) {
    if (key=='f') {
      drawField.finalize();
    }
    else if (key=='r') {
      initCreatePlayfield();
    }
  }
}

void draw() {
  float dt = 1.0/60.0;//0.016;//1.0/frameRate;
  background(Z.BACKGROUND_COLOR);

  if (mode==GLOBAL_MODE_CREATE_PLAYFIELD) {
    drawField.draw();
  } else if (mode==GLOBAL_MODE_BREEDING) {
    //float d = generation.field.getCourseDistance(new PVector(mouseX, mouseY));
    //println(nf(d,1,2));
    generation.integrate(dt);
    generation.draw();
    
    streamingGraph.draw(800,100,350,300);
    
    fill(255);
    textAlign(LEFT,BASELINE);
    text(generation.breeder.winnerInfo, 800,450);
    
  }
}
