class Breeder {

  String winnerInfo = "";


  ArrayList<Vehicle> breedNewGeneration(PlayField field, int newGenIndex, float fieldTotalDistance, ArrayList<Vehicle> population) {
    boolean DEBUG = false;
    
    //System.err.println("breedNewGenerationBrains() something wrong../");

    calculateFitnesses(population, fieldTotalDistance);
    sortByFitness(population);
    
    Vehicle best = population.get(0);
    //println("Best in this generation was "+best.name+", fitness="+nf(best.stats.fitness,1,4)+", max total distance = "+nf(best.stats.getMaxTotalCourseDistance(),1,1));
    
    winnerInfo = "Best in last generation: "+best.name
      +"\nfitness: "+nf(best.stats.fitness,1,3)
      +"\nmax total distance: "+nf(best.stats._maxTotalCourseDistance,1,1)
      +" ("+nf(floor(best.stats._maxTotalCourseDistance/field.totalDistance*100)/100.0,1,2)+" laps)"
      +"\naverage speed: "+nf(best.stats.getMaxTotalCourseDistance()/best.stats.totalAliveTime,1,1);
    //println(winnerInfo);
    
    float[] fitnesses = new float[population.size()];
    for (int i=0;i<fitnesses.length;i++) {
      fitnesses[i] = population.get(i).stats.fitness;
    }
    //Arrays.sort(fitnesses);
    //println(nf(fitnesses[0],1,4),"...",nf(fitnesses[fitnesses.length-1],1,4));
    streamingGraph.addValues(fitnesses); //new float[] { best.stats.fitness }

    int debugTop = 5;
    if (DEBUG) println();
    if (DEBUG) println();
    if (DEBUG) println("breeding GENERATION "+newGenIndex);
    if (DEBUG) println("top "+debugTop+" ---------------------------------------");
    for (int i=0; i<debugTop; i++) {
      if (DEBUG) println(i+": "+population.get(i).name+", fitness="+population.get(i).stats.fitness);
    }

    ArrayList<Vehicle> newGeneration = carryOverPreviousGeneration(population);
    //println("new gen: "+newGeneration.size());

    ArrayList<Stack> stacks = makeDistributionStack(population);//
    int deltaAmount = Z.POPULATION_COUNT - newGeneration.size();
    ArrayList<Vehicle> children = reproduce(stacks, deltaAmount, newGenIndex);

    for (int i=0; i<children.size(); i++) {
      //children.get(i).setField(field);
      newGeneration.add(children.get(i));
    }

    if (DEBUG) println("New generation: -------------------------------------");
    for (Vehicle v : newGeneration) {
      if (DEBUG) println(v.name+": fitness="+v.stats.fitness);
    }

    if (DEBUG) println();
    
    
    //System.err.println("TODO something still wrong, best not carried over");
    return newGeneration;
  }



  /*
  * carry over if not too old
   */
  ArrayList<Vehicle> carryOverPreviousGeneration(ArrayList<Vehicle> population) {

    int bestCount=Z.BEST_CARRY_TO_NEW_GENERATION;
    int leastCount=Z.LEAST_CARRY_TO_NEW_GENERATION;

    ArrayList<Vehicle> newGeneration = new ArrayList<Vehicle>();

    // add BEST
    bestCount = min(bestCount, population.size());
    for (int i=0; i<bestCount; i++) {     
      newGeneration.add( population.get(i).clone() );
      //newGeneration.add( population.get(i) );
    }

    // add Worst
    leastCount = min(leastCount, population.size());
    for (int i=0; i<leastCount; i++) {
      int index = population.size()-i-1;
      newGeneration.add( population.get(index).clone() );
      //newGeneration.add( population.get(index) );
    }

    return newGeneration;
  }






  void calculateFitnesses(ArrayList<Vehicle> population, float fieldTotalDistance) {
    for (Vehicle v : population) {
      float fitness = calcFitness_fromDistanceAndSpeed(fieldTotalDistance, v.stats.getMaxTotalCourseDistance(), v.stats.totalAliveTime);
      v.stats.fitness = fitness*fitness;
    }
  }

  void sortByFitness(ArrayList<Vehicle> population) {
    Collections.sort(population, new Comparator<Vehicle>() {
      public int compare(Vehicle v0, Vehicle v1) {
        float f0 = v0.stats.fitness;
        float f1 = v1.stats.fitness;
        if (f0>f1) return -1;
        else if (f0<f1) return 1;
        else return 0;
      }
    }
    );
  }

  /*
  *
   *  use total distance and speed to make fitness
   *
   */
  ArrayList<Stack> makeDistributionStack(ArrayList<Vehicle> population) {

    float maxTotalFitness = 0;
    for (Vehicle v : population) {
      float fitness = v.stats.fitness;//getFitness_fromDistanceAndSpeed(fieldTotalDistance, v.stats.getTotalCourseDistance(), v.stats.totalAliveTime);
      maxTotalFitness = max(maxTotalFitness, fitness);
    }

    ArrayList<Stack> distributionStackTotalDistance = new ArrayList<Stack>();

    //float bias = 0.001;

    float min = 0;
    float max = 0;

    for (Vehicle v : population) {
      max += v.stats.fitness;//getFitness_fromDistanceAndSpeed(fieldTotalDistance, v.stats.getTotalCourseDistance(), v.stats.totalAliveTime);
      distributionStackTotalDistance.add( new Stack(v, min, max) );
      min += v.stats.fitness;//getFitness_fromDistanceAndSpeed(fieldTotalDistance, v.stats.getTotalCourseDistance(), v.stats.totalAliveTime);
    }

    return distributionStackTotalDistance;
  }


  float calcFitness_fromDistanceAndSpeed(float fieldTotalDistance, float totalDistance, float totalTime) {
    //println(fieldTotalDistance,totalDistance,totalTime);
    if (Float.isNaN(totalDistance)) {
      return 0.00001;
    } 
    if (totalTime==0) {
      return 0.00001;
    } else {
      
      float laps = max(0,totalDistance/fieldTotalDistance); // 0..n (xn=1 lap)
      float clippedDistancePercentage = min(1, laps); // 0..1
      
      //boolean finishedRace = clippedDistancePercentage==1;     
      //float finishedRaceFac = finishedRace ? 1 : 0;
      
      float averageSpeed = totalDistance / totalTime;
      float speedPercentage = max(0, averageSpeed / Z.FOR_FITNESS_MAX_AVERAGE_SPEED); // 0..? (where 1=MAX AVERAGE SPEED)
      
      // fitness =
      // amount of laps
      // +
      // exponantially during lap increased speedPercentage
      // in the beginning speed = no influence, at one lap speed has lot's of influence
      float fitness = laps + pow(clippedDistancePercentage,3)*speedPercentage;// + (distancePercentage*distancePercentage)*speedPercentage;
      //if (laps>1) {
      //  fitness += speedPercentage;// + (distancePercentage*distancePercentage)*speedPercentage;
      //}
      
      //println(fitness);
      //if (Float.isNaN(fitness)) println("ERROR HERE?");
      return fitness;
    }
  }







  ArrayList<Vehicle> reproduce(ArrayList<Stack> stacks, int amount, int newGenIndex) {

    //int leastBreedingTargetSize = population.size();
    //int maxBreedingTargetSize = (int)(Z.BREEDING_RATE_MIN*population.size());
    //int targetPairs = max(leastBreedingTargetSize, min(maxBreedingTargetSize, Z.MAX_GENERATION_SIZE));

    //println(generationIndex+" BREED NEW: target="+targetPairs+", adding "+deltaPairs);

    ArrayList<Vehicle> children = new ArrayList<Vehicle>();
    for (int pairIndex=0; pairIndex<amount; pairIndex++) {

      //println("make pair "+pairIndex);
      // find parents
      Stack parent0Data = chooseParent( stacks, null );
      Stack parent1Data = chooseParent( stacks, Z.SELF_BREEDING ? null : parent0Data );  
      Vehicle parent0 = parent0Data.vehicle;
      Vehicle parent1 = parent1Data.vehicle;
      if (random(1.0) < Z.RANDOM_PARENT_CHANCE) {
        do {
          parent1 = stacks.get( (int)random(stacks.size()) ).vehicle;
        } while (!Z.SELF_BREEDING && parent0==parent1);
      }

      // make DNA
      //JSONObject parent0JSON = parent0.nodeBrain.toJSON();
      //JSONObject parent1JSON = parent1.nodeBrain.toJSON();
      //JSONObject childJSON = crossOverAndMutate(parent0JSON, parent1JSON);
      NodeBrain childBrain = crossOverAndMutate(parent1.nodeBrain, parent0.nodeBrain);
      
      // turn NetworkWeights into a flat array
      //float[] parent0SortedWeights = new NetworkWeights( parent0.brain ).sortedWeightsToFloatArray();
      //float[] parent1SortedWeights = new NetworkWeights( parent1.brain ).sortedWeightsToFloatArray();
      //float[] childWeights = crossOverAndMutate(parent0SortedWeights, parent1SortedWeights);
      //NetworkWeights childBrainWeights = new NetworkWeights( parent0.brain ).createNewFromSortedWeights(childWeights);

      String pname0 = parent0.name.split(" ")[0];
      String pname1 = parent1.name.split(" ")[0];

      Vehicle vehicle = new Vehicle();
      vehicle.name = "g"+newGenIndex+"_"+pairIndex+" child of "+pname0+" and "+pname1;
      vehicle.nodeBrain = childBrain;
      //vehicle.brain.applyWeights( childBrainWeights );
      children.add(vehicle);
    }

    return children;
  }



  /*
  *
   *  Chooses a parent from the distribution stack
   *  if stack total = 0, choose randomly
   *
   */
  Stack chooseParent(ArrayList<Stack> distributionStack, Stack otherParent) {
    int stackSize = distributionStack.size();
    float max = distributionStack.get( stackSize-1 ).max;
    if (max==0 || (otherParent!=null && otherParent.min==0 && otherParent.max==max)) {

      return distributionStack.get( (int)random(stackSize) );
    } else {

      Stack out = otherParent;

      while ((otherParent==null&&out==null) || (out!=null&&otherParent!=null&&out.vehicle == otherParent.vehicle)) {

        float rnd = random(max);

        for (Stack range : distributionStack) {
          if (rnd>=range.min && rnd<range.max) {
            out = range;
          }
        }
      }
      return out;

      //throw new RuntimeException("Meh, something is wrong from the stack. rnd="+rnd);
    }
  }



  /*
  *
   *  CROSSOVER AND MUTATE
   *
   */
   
   
   NodeBrain crossOverAndMutate(NodeBrain b0, NodeBrain b1) {
     
     NodeBrain childBrain = b0.clone();
     
     ArrayList<NodeConnection> connections0 = b0._connections;
     ArrayList<NodeConnection> connections1 = b1._connections;
     ArrayList<NodeConnection> childConnections = childBrain._connections;
     
     int dnaLength = connections0.size();
     int splitPoint = (int)random(dnaLength+1);
     
     for (int genIndex=0;genIndex<dnaLength;genIndex++) {
       NodeConnection nc0 = connections0.get(genIndex);
       int ID = nc0.ID;
       NodeConnection nc1 = getNodeConnectionByID( connections1, ID );
       NodeConnection childConnection = getNodeConnectionByID( childConnections, ID );
       
       // crossover
      float childWeight = genIndex<splitPoint ? nc0.weight : nc1.weight;
      
      // mutation
      if (random(1.0)<Z.MUTATION_CHANCE_PER_WEIGHT) {
        childWeight += Z.MUTATION_RANGE * randomGaussian();//random(-MUTATION_RANGE/2, MUTATION_RANGE/2);
      }
      else if (random(1.0)<Z.FREAK_MUTATION_CHANCE_PER_WEIGHT) {
        childWeight = Z.FREAK_MUTATION_RANGE * randomGaussian();
      }
      
      childConnection.weight = childWeight;
     }
     
     return childBrain;
     
   }

//  float[] crossOverAndMutate(float[] parent0SortedWeights, float[] parent1SortedWeights) {


//    int dnaLength = parent0SortedWeights.length;
//    int splitPoint = (int)random(dnaLength+1);
//    float[] childDna = new float[dnaLength];

//    for (int genIndex=0; genIndex<dnaLength; genIndex++) {

//      // crossover
//      float childWeight = genIndex<splitPoint ? parent0SortedWeights[genIndex] : parent1SortedWeights[genIndex];

//      // mutation
//      if (random(1.0)<Z.MUTATION_CHANCE_PER_WEIGHT) {
//        childWeight += Z.MUTATION_RANGE/2 * randomGaussian();//random(-MUTATION_RANGE/2, MUTATION_RANGE/2);
//      }

//      childDna[genIndex] = childWeight;
//    }

//    return childDna;
//  }
}



class Stack {
  Vehicle vehicle;
  float min;
  float max;
  Stack(Vehicle v, float min, float max) {
    this.vehicle=v;
    this.min=min;
    this.max=max;
  }
  public String toString() { 
    return "Stack from vehicle("+vehicle.name+") range = "+min+" ... "+max;
  }
}
