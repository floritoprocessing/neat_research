class Help {

  float x=10;
  float y=70;
  long closeTimeMs = Long.MIN_VALUE;
  boolean visible = false;
  
  String[] helpLines = new String[] {
    "[h] Toggle this help screen toggle",
    "",
    "[space] restart drawing",
    "[n]/[m] prev/next file", 
    "[d] randomize settings",
    "[-]/[+] one generation less/more", 
    "",
    "[r] toggle auto rotate", 
    "[l] toggle draw only last generation",
    "[q]/[w] increase/decrease growth rate", 
    "",
    "[s] print settings to console", 
    "[S] save settings to temp file"
    
  };
  
  float padding = 10;
  float lineSpacing=3;
  float wid, hei, singleLineHeight;
  
  Help() {
    float maxTextWidth = 0;   
    for (String line:helpLines) {
      maxTextWidth = max(maxTextWidth, textWidth(line));
    }
    wid = maxTextWidth + 2*padding;
    
    singleLineHeight = textAscent()+textDescent()+lineSpacing;
    float totalTextHeight = helpLines.length * singleLineHeight;
    hei = totalTextHeight + 2*padding;
    show();
  }
  
  
  void toggleVisibility() {
    visible = !visible;
    closeTimeMs = Long.MIN_VALUE;
  }
  
  void show() {
    show(Long.MIN_VALUE);
  }
  void show(long deltaTimeMs) {
    if (deltaTimeMs == Long.MIN_VALUE) {
      closeTimeMs = Long.MIN_VALUE;
    } else {
      closeTimeMs = System.currentTimeMillis() + deltaTimeMs;
    }
    visible = true;
  }
  void hide() {
    visible = true;
  }
  
  void update() {
    if (visible) {
      if (closeTimeMs!=Long.MIN_VALUE && System.currentTimeMillis() > closeTimeMs) {
        visible = false;
      } else {
        drawHelpScreen();
      }
    }
  }
  
  void drawHelpScreen() {
    rectMode(CORNER);
    strokeWeight(1);
    stroke(0);
    fill(255);
    rect(x,y,wid,hei);
    fill(textColor);
    textAlign(LEFT,TOP);
    for (int i=0;i<helpLines.length;i++) {
      text(helpLines[i], x+padding, y+padding+i*singleLineHeight);
    }
  }
  
  
  
  void print() {
    for (String line:helpLines)
      println(line);
  }
  
}
