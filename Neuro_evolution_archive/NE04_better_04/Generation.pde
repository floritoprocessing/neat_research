class Generation {

  GenerationRunningMode runningMode = GenerationRunningMode.NOT_INITIALIZED;

  String uniqueName;

  PlayField field;

  ArrayList<Vehicle> population;

  Breeder breeder = new Breeder();

  boolean showVehicleInfo = true;
  Vehicle activeShowVehicle = null;

  boolean paused = false;
  float time;
  int genIndex = 0;

  int integrationLoops = 1;


  Generation(PlayField newField) {
    uniqueName = "Breeding start "+makeDateString()+" "+makeTimeString();
    if (newField==null)
      field = new PlayField();
    else
      field = newField;
  }



  void keyPressed() {
    if (key=='r') {
      resetToNotInitialized();
    } else if (key=='i') {
      showVehicleInfo = !showVehicleInfo;
    } else if (key=='p') {
      paused = !paused;
    } else if (key=='k') {
      if (activeShowVehicle!=null) {
        activeShowVehicle.stats._totalCourseDistance = 0;
        activeShowVehicle.dead = true;
      }
    } else if (key=='1') integrationLoops = 1;
    else if (key=='2') integrationLoops = 2;
    else if (key=='3') integrationLoops = 4;
    else if (key=='4') integrationLoops = 8;
    else if (key=='5') integrationLoops = 16;
    else if (key=='6') integrationLoops = 32;
    else if (key=='7') integrationLoops = 64;
    else if (key=='8') integrationLoops = 128;
    else if (key=='9') integrationLoops = 256;
  }
  
  
  void resetToNotInitialized() {
    runningMode = GenerationRunningMode.NOT_INITIALIZED;
  }


  void integrate(float dt) {

    if (paused) return;

    switch(runningMode) {

    case NOT_INITIALIZED: 
      initializeGeneration();
      runningMode = GenerationRunningMode.RUN_GENERATION;
      break;

    case RUN_GENERATION:
      runGeneration(dt);
      break;

    case BREEDING_NEXT_GENERATION:
      population = breeder.breedNewGeneration(field, genIndex+1, field.totalDistance, population);
      genIndex++;
      runningMode = GenerationRunningMode.END_BREEDING_NEXT_GENERATION;
      //population = 
      break;

    case END_BREEDING_NEXT_GENERATION:
      runningMode = GenerationRunningMode.NOT_INITIALIZED;
      break;
    }
  }


  void initializeGeneration() {
    time = 0;
    if (population==null) {
      population = new ArrayList<Vehicle>();
      for (int i=0; i<Z.POPULATION_COUNT; i++) {
        Vehicle vehicle = new Vehicle();
        //vehicle.setField(field);
        vehicle.name = "g"+genIndex+"_"+i;
        population.add(vehicle);
      }
    }
    setVehiclesOnStartLine();
    activeShowVehicle = null;
  }


  void setVehiclesOnStartLine() {
    for (Vehicle v : population) {
      v.initForStart(field);
      v.position = field.getStartPosition();
    }
  }



  void runGeneration(float dt) {

    for (int i=0; i<integrationLoops; i++) {
      time += dt;
      switch(runningMode) {
      case RUN_GENERATION:

        for (Vehicle v : population) {
          if (!v.dead) {
            v.useBrain();
            v.integrate(dt);
            //v.increaseSpeed(random(1.0)<0.01);
            v.checkSensors();
            v.checkDeathConditions();
          }
        }

        if (activeShowVehicle!=null && activeShowVehicle.dead) {
          activeShowVehicle=null;
        }
        if (countAliveVehicles()==0 || time>Z.BREEDING_MAX_GENERATION_TIME) {
          runningMode = GenerationRunningMode.BREEDING_NEXT_GENERATION;
        }
        break;

      default:
        break;
      }
    }
  }




  final String SAVE_PATH = "output";

  void saveGeneration() {

    println("saveGeneration()");

    JSONObject out = toJSON();
    
    String title = "Generation "+genIndex+".json";
    String fullPath = SAVE_PATH + "/" + uniqueName + "/" + title;
    
    println("Saving to "+fullPath);
    saveJSONObject( out, fullPath, "compact" );
  }


  void loadGeneration(String breedingStartName, int generation) {

    println("loadGeneration()");

    String filename = breedingStartName + "/" + "Generation "+generation+".json";
    String fullPath = SAVE_PATH + "/" + filename;
    JSONObject json = loadJSONObject(fullPath);

    initFromJSON(json);
  }





  void draw() {

    field.draw();

    fill(255);

    textAlign(LEFT, BASELINE);
    startText("Generation "+genIndex, 10, 20);
    textIndent("  ");
    text("runningMode = "+runningMode);
    text("[1]-[9] speed: "+integrationLoops+"x");
    text("[-]/[+] frameRate = "+(int)targetFramerate+" ("+(int)frameRate+")");
    text("time = "+nf(time, 1, 2)+"s");
    text("alive vehicles = "+countAliveVehicles());
    text("[k] to kill selected vehicle (and make fitness 0)");
    text("draw vehicle [i]nfo: "+showVehicleInfo);
    if (paused) {
      if (millis()%1000<500) {
        text("--- [P]AUSED ---");
      }
    }

    switch(runningMode) {
    case RUN_GENERATION:
      for (Vehicle v : population) {
        if (!v.dead) {
          v.draw();
        }
      }
      if (showVehicleInfo) {
        if (activeShowVehicle!=null && mousePressed) {
          activeShowVehicle=null;
        }

        if (activeShowVehicle!=null) {
          activeShowVehicle.drawInfo();
        } else {
          Vehicle v = getClosestVehicle();
          if (v!=null && mousePressed) {
            activeShowVehicle = v;
          }
          if (v!=null) 
            v.drawInfo();
        }
      }

      break;

    default:
      break;
    }
  }

  int countAliveVehicles() {
    int count = 0;
    for (Vehicle v : population) count += v.dead?0:1;
    return count;
  }

  Vehicle getClosestVehicle() {
    Vehicle closestVehicle = null;
    float closestDistance = Z.DRAW_VEHICLE_INFO_MIN_DISTANCE_SQ;//Float.MAX_VALUE;
    for (Vehicle v : population) {
      if (!v.dead) {
        PVector pos = v.position;
        float dx = pos.x-mouseX;
        float dy = pos.y-mouseY;
        float dsq = dx*dx+dy*dy;
        if (dsq<closestDistance) {
          closestDistance=dsq;
          closestVehicle = v;
        }
      }
    }

    return closestVehicle;
  }




  /*
  *
   *    SERIALISATION
   *
   */

  final String KEY_UNIQUE_NAME = "uniqueName";
  final String KEY_GENERATION_INDEX = "generationIndex";
  final String KEY_ZETTINGS = "zettings";
  final String KEY_VEHICLES = "vehicles";


  void initFromJSON(JSONObject json) {

    JSONObject zettings = json.getJSONObject(KEY_ZETTINGS);
    Z.fromJSON(zettings);

    genIndex = json.getInt( KEY_GENERATION_INDEX );
    uniqueName = json.getString( KEY_UNIQUE_NAME );

    JSONArray vehicles = json.getJSONArray( KEY_VEHICLES );
    population.clear();
    for (int i=0; i<vehicles.size(); i++) {
      JSONObject vehicleJSON = vehicles.getJSONObject(i);
      Vehicle v = new Vehicle( vehicleJSON );
      population.add(v);
    }
  }
  
  

  // returns zettings and entire population as JSON
  JSONObject toJSON() {
    JSONObject out = new JSONObject();

    out.setJSONObject( KEY_ZETTINGS, Z.toJSON() );

    out.setString( KEY_UNIQUE_NAME, uniqueName );
    out.setInt( KEY_GENERATION_INDEX, genIndex );

    JSONArray vehicles = new JSONArray();
    for (int i=0; i<population.size(); i++) {
      Vehicle v = population.get(i);
      JSONObject vehicleJson = v.toJSON();
      vehicles.setJSONObject(i, vehicleJson);
    }

    out.setJSONArray( KEY_VEHICLES, vehicles );

    return out;
  }
  
  
  
}


enum GenerationRunningMode {
  NOT_INITIALIZED, 
    RUN_GENERATION, 
    BREEDING_NEXT_GENERATION, 
    END_BREEDING_NEXT_GENERATION
}
