class StreamingGraph {

  int MAX_HORIZONTAL_VALUES = 40;//Integer.MAX_VALUE;
  int VALUE_MARKER_DIVISIONS = 5;
  int MAX_AMOUNT_HORIZONTAL_TEXT = 10;
  float VERTICAL_OVERHEAD = 0.1;

  int horizontalIndexOffset;
  float allValuesMin;
  float allValuesMax;
  Values[] values;

  StreamingGraph(int nrOfValueLines) {
    reset(nrOfValueLines);
  }
  
  void reset() {
    if (values==null) throw new RuntimeException("reset() can only be called after reset(int nrOfValues)");
    else {
      reset(values.length);
    }
  }

  void reset(int nrOfValueLines) {
    horizontalIndexOffset = 0;
    allValuesMin = Float.MAX_VALUE;
    allValuesMax = Float.MIN_VALUE;

    values = new Values[nrOfValueLines];
    for (int i=0; i<nrOfValueLines; i++) {
      values[i] = new Values();
      values[i].add(0);
    }
    updateAllValuesMinMax();
  }



  void addValues(float[] timeValues) {


    if (timeValues.length!=values.length) {
      throw new RuntimeException("Meep, timeValues("+timeValues.length+") not same length as values("+values.length+")");
    } else {


      if (values[0].size()==MAX_HORIZONTAL_VALUES) {
        for (int i=0; i<values.length; i++) {
          values[i].removeFirst();
        }
        horizontalIndexOffset++;
      }
      for (int i=0; i<values.length; i++) {
        values[i].add(timeValues[i]);
      }
      updateAllValuesMinMax();
    }
  }





  void updateAllValuesMinMax() {
    allValuesMin = Float.MAX_VALUE;
    allValuesMax = Float.MIN_VALUE;
    for (int i=0; i<values.length; i++) {
      allValuesMin = min(allValuesMin, values[i]._min);
      allValuesMax = max(allValuesMax, values[i]._max);
    }
  }

  /*
  *
   *    Drawing
   *
   */


  void draw(float sx, float sy, float w, float h) {

    strokeWeight(1);
    stroke(255);
    fill(0);



    pushMatrix();

    translate(sx, sy);
    rectMode(CORNER);
    rect(0, 0, w, h);

    fill(255);

    int gens = values[0].size();
    if (gens>1) {

      // make time/generation markers     
      int divToDraw = 1;
      while (gens>MAX_AMOUNT_HORIZONTAL_TEXT*divToDraw) {
        divToDraw*=2;
      }      
      textAlign(CENTER, TOP);
      for (int i=0; i<gens; i++) {
        float x = map(i, 0, gens-1, 0, w);
        line(x, h, x, h+5);
        boolean drawText = i%divToDraw==0 || i==gens-1;
        if (drawText) {
          text(i+horizontalIndexOffset, x, h+10);
        }
      }



      // make value markers
      textAlign(RIGHT, BASELINE);
      for (int i=0; i<VALUE_MARKER_DIVISIONS+1; i++) {
        float y = map(i, 0, VALUE_MARKER_DIVISIONS, h, h - (h)/(1.0+VERTICAL_OVERHEAD));
        line(0, y, -5, y);
        String val = nf(map(i, 0, VALUE_MARKER_DIVISIONS, 0, allValuesMax), 1, 3);
        text(val, -10, y);
      }


      // draw lines
      // iterate per line (i)
      stroke(255, 24);
      for (int i=0; i<values.length; i++) {

        Values vals = values[i];
        beginShape(LINE_STRIP);
        // iterate per gen
        for (int t=0; t<gens; t++) {
          float x = map(t, 0, gens-1, 0, w);
          float y = map(vals.get(t), 0, allValuesMax, h, h - (h)/(1.0+VERTICAL_OVERHEAD));
          vertex(x, y);
        }
        endShape();
      }
    }


    popMatrix();
  }


  // Values Class

  class Values {
    float _min = Float.MAX_VALUE; 
    float _max = Float.MIN_VALUE; 
    ArrayList<Float> _values = new ArrayList<Float>(); 

    void add(float value) {
      _values.add(value); 
      _min = min(_min, value); 
      _max = max(_max, value);
    }

    float get(int index) {
      return _values.get(index);
    }

    int size() {
      return _values.size();
    }
    
    void removeFirst() {
      _values.remove(0);
    }
  }
} // end of StreamingGraph class
