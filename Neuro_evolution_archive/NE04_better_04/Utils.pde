String makeDateString() {
  return nf(year(), 4)+nf(month(), 2)+nf(day(), 2);
}
String makeTimeString() {
  return nf(hour(), 2)+nf(minute(), 2)+nf(second(), 2);
}

import java.awt.geom.Line2D;

boolean lineSegmentsIntersect(Line l0, Line l1) {
  return lineSegmentsIntersect(l0.getP0(), l0.getP1(), l1.getP0(), l1.getP1());
}

boolean lineSegmentsIntersect(PVector p0, PVector p1, PVector p2, PVector p3) {
  return Line2D.Float.linesIntersect(p0.x, p0.y, p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
}


PVector lineLineIntersection(Line a, Line b) {
  return lineLineIntersection(a.getP0(), a.getP1(), b.getP0(), b.getP1());
}

PVector lineLineIntersection(PVector A, PVector B, PVector C, PVector D) 
{ 
  // Line AB represented as a1x + b1y = c1 
  float a1 = B.y - A.y; 
  float b1 = A.x - B.x; 
  float c1 = a1*(A.x) + b1*(A.y); 

  // Line CD represented as a2x + b2y = c2 
  float a2 = D.y - C.y; 
  float b2 = C.x - D.x; 
  float c2 = a2*(C.x)+ b2*(C.y); 

  float determinant = a1*b2 - a2*b1; 

  if (determinant == 0) 
  { 
    // The lines are parallel. This is simplified 
    // by returning a pair of FLT_MAX 
    return null;//new PVector(Double.MAX_VALUE, Double.MAX_VALUE);
  } else
  { 
    float x = (b2*c1 - b1*c2)/determinant; 
    float y = (a1*c2 - a2*c1)/determinant; 
    return new PVector(x, y);
  }
} 



class RunningAverage {

  int count;
  float[] values;
  int pointer;
  boolean fullArray;

  RunningAverage(int count) {
    this.count = count;
    reset();
  }

  void reset() {
    values = new float[count];
    pointer = 0;
    fullArray = false;
  }
  
  void add(float v) {
    values[pointer] = v;
    pointer++;
    if (pointer==count) {
      pointer = 0;
      fullArray = true;
    }
  }
  
  float getAverage() {
    int c = fullArray ? count : pointer;
    float av=0;
    for (int i=0;i<c;i++) {
      av += values[i];
    }
    av/=c;
    return av;
  }
  
  boolean isComplete() {
    return fullArray;
  }
}


void println(ArrayList arrayList) {
  for (int i=0;i<arrayList.size();i++) println(i+" "+arrayList.get(i).toString());
}

void print(ArrayList arrayList) {
  for (int i=0;i<arrayList.size();i++) print(i+" "+arrayList.get(i).toString()+" ");
  println();
}




/*
*
*    SCREEN TEXT
*
*/

float screenTextX, screenTextY;

void startText(String text, float x, float y) {
  text(text,x,y);
  screenTextX=x;
  screenTextY=y;
  screenTextNextLine();
}

void textIndent(String s) {
  screenTextX += textWidth(s);
}

void text(String text) {
  text(text, screenTextX, screenTextY);
  screenTextNextLine();
}

void screenTextNextLine() {
   screenTextY += textAscent()+textDescent();
}
