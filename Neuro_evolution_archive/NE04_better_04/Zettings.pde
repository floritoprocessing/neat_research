import java.lang.reflect.*;

class Zettings {

  int VEHICLE_SENSORS = 5;
  float VEHICLE_SENSOR_DEGREES = 40;
  float VEHICLE_SIZE_TO_SENSOR_FAC = 5;
  float VEHICLE_SIZE_TO_CENTRAL_SENSOR_FAC = 6;

  float VEHICLE_TURNING_SPEED = 180; // 120
  float VEHICLE_THRUST_SPEED = 1000;
  float VEHICLE_BREAK_SPEED = 4000; // 800
  float VEHICLE_SIZE = 6;

  int VEHICLE_AVERAGE_SPEED_FRAMES = 120;
  float VEHICLE_AVERAGE_SPEED_DEATH = 15.0;

  // ===== NEURONS ===============================

  float RANDOM_INIT_WEIGHT_VALUE = 0.0;

  // ===== NETWORK ===============================

  int[] HIDDEN_NEURONS = new int[] { 8 };
  boolean ADD_BIAS_CONNECTION = true;
  float BIAS = 1;

  // ===== BREEDING ==============================

  float BREEDING_MAX_GENERATION_TIME = 60;
  float FOR_FITNESS_MAX_AVERAGE_SPEED = 300;

  int POPULATION_COUNT = 100;
  int BEST_CARRY_TO_NEW_GENERATION = 10;
  int LEAST_CARRY_TO_NEW_GENERATION = 1;

  float RANDOM_PARENT_CHANCE = 0.01; // chance that a fit parent gets a random parent

  boolean SELF_BREEDING = true;

  float MUTATION_CHANCE_PER_WEIGHT = 0.02; // 0.05 0.1
  float MUTATION_RANGE = 0.5; // 0.05 0.2 1.0

  float FREAK_MUTATION_CHANCE_PER_WEIGHT = 0.002;
  float FREAK_MUTATION_RANGE = 4;

  // ===== DRAWING ===============================

  int TRACK_BACKGROUND_COLOR = 0xff0D283E;
  int TRACK_DEBUG_BACKGROUND_COLOR = 0xff2B678E;
  int BACKGROUND_COLOR = 0xff051B2C;

  int VEHICLE_INFO_RECT_FILL = 0x88444444;

  float DRAW_VEHICLE_INFO_MIN_DISTANCE_SQ = 30*30;

  float DRAW_NEURON_DIAMETER = 10;
  float DRAW_NEURON_WEIGHT_TO_STROKE_WEIGHT = 1;
  float DRAW_NEURON_STROKE_WEIGHT_MAX = 5;

  int NEURON_BODY_STROKE = 0xffffffff;
  int NEURON_BODY_FILL = 0xff888888;
  int NEURON_TEXT_COLOR = 0xffffffff;




  JSONObject toJSON() {
    JSONObject out = new JSONObject();
    getAllVariables(this, out);
    return out;
  }
  
  
  void fromJSON(JSONObject json) {
    setAllVariables(this, json);
    //println("from JSON");
    //println("variables set to:");
    //println(toJSON());
  }


  void setAllVariables(Object obj, JSONObject json) {
    Class c = obj.getClass();
    Field[] fields = c.getDeclaredFields();
    for (Field field : fields) {
      
      String varName = field.getName();
      Class varClass = field.getType();
      
      try {
        if (varClass==int.class) {
          field.setInt(obj, json.getInt(varName));
        } else if (varClass==float.class) {
          field.setFloat(obj, json.getFloat(varName));
        } else if (varClass==boolean.class) {
          field.setBoolean(obj, json.getBoolean(varName));
        } else if (varClass==int[].class) { 
          JSONArray jsonArray = json.getJSONArray(varName);
          int[] array = jsonArray.getIntArray();
          field.set(obj,array);        
        } else {
          
        }
      } 
      catch (IllegalAccessException e) {
        e.printStackTrace();
      }
      
    } // end for fields
  }
  
  void getAllVariables(Object obj, JSONObject json) {
    Class c = obj.getClass();
    Field[] fields = c.getDeclaredFields();
    for (Field field : fields) {
      
      String varName = field.getName();
      Class varClass = field.getType();

      try {
        if (varClass==int.class) {
          json.setInt(varName, field.getInt(obj));
        } else if (varClass==float.class) {
          json.setFloat(varName, field.getFloat(obj));
        } else if (varClass==boolean.class) {
          json.setBoolean(varName, field.getBoolean(obj));
        } else if (varClass==int[].class) {
          int[] array = (int[])field.get(obj);
          JSONArray jsonArray = new JSONArray();
          for (int i=0;i<array.length;i++)
            jsonArray.setInt(i,array[i]);
          json.setJSONArray(varName,jsonArray);
        } else {
          
        }
      } 
      catch (IllegalAccessException e) {
        e.printStackTrace();
      }
      
    } // end for fields
  }
  
}
