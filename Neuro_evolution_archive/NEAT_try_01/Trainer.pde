class Trainer {
  
  int startTime;
  
  Trainer() {
    nextRound();
  }
  
  void nextRound() {
    startTime = millis();
  }
  
  float deltaTime() {
    return (millis()-startTime)/1000.0;
  }
  
  
  
}
