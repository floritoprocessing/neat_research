class Breeder {



  void breedNewGeneration(PlayField field, int newGenIndex, float fieldTotalDistance, ArrayList<Vehicle> population) {

    System.err.println("breedNewGenerationBrains() something wrong../");

    calculateFitnesses(population, fieldTotalDistance);
    sortByFitness(population);

    ArrayList<Vehicle> newGeneration = new ArrayList<Vehicle>();

    carryOverPreviousGeneration(population, newGeneration);
    println("Carried over:");
    for (Vehicle v:newGeneration) {
      println(v.name+": fitness="+v.stats.fitness);
    }
    System.err.println("TODO continue coding");
    if (true) return;
    
    //int carriedOver = newGeneration.size();

    ArrayList<Stack> stacks = makeDistributionStack(population);//

    int deltaAmount = Z.POPULATION_COUNT - newGeneration.size();
    ArrayList<Vehicle> children = reproduce(stacks, population, deltaAmount, newGenIndex);

    for (int i=0; i<children.size(); i++) {
      children.get(i).setField(field);
    }
    
    System.err.println("TODO naming from parents, replace population");
    
    
  }



  /*
  * carry over if not too old
   */
  void carryOverPreviousGeneration(ArrayList<Vehicle> population, ArrayList<Vehicle> newGeneration) {

    int bestCount=Z.BEST_CARRY_TO_NEW_GENERATION;
    int leastCount=Z.LEAST_CARRY_TO_NEW_GENERATION;

    // add BEST
    bestCount = min(bestCount, population.size());
    for (int i=0; i<bestCount; i++) {
      newGeneration.add( population.get(i) );
    }

    // add Works
    leastCount = min(leastCount, population.size());
    for (int i=0; i<leastCount; i++) {
      int index = population.size()-i-1;
      newGeneration.add( population.get(index) );
    }
  }






  void calculateFitnesses(ArrayList<Vehicle> population, float fieldTotalDistance) {
    for (Vehicle v:population) {
      float fitness = calcFitness_fromDistanceAndSpeed(fieldTotalDistance, v.stats.getTotalCourseDistance(), v.stats.totalAliveTime);
      v.stats.fitness = fitness;
    }
  }
  
  void sortByFitness(ArrayList<Vehicle> population) {
    Collections.sort(population, new Comparator<Vehicle>() {
      public int compare(Vehicle v0, Vehicle v1) {
        float f0 = v0.stats.fitness;
        float f1 = v1.stats.fitness;
        if (f0>f1) return -1;
        else if (f1<f0) return 1;
        else return 0;
      }
    });
  }

  /*
  *
   *  use total distance and speed to make fitness
   *
   */
  ArrayList<Stack> makeDistributionStack(ArrayList<Vehicle> population) {

    float maxTotalFitness = 0;
    for (Vehicle v : population) {
      float fitness = v.stats.fitness;//getFitness_fromDistanceAndSpeed(fieldTotalDistance, v.stats.getTotalCourseDistance(), v.stats.totalAliveTime);
      maxTotalFitness = max(maxTotalFitness, fitness);
    }

    ArrayList<Stack> distributionStackTotalDistance = new ArrayList<Stack>();

    //float bias = 0.001;

    float min = 0;
    float max = 0;

    for (Vehicle v : population) {
      max += v.stats.fitness;//getFitness_fromDistanceAndSpeed(fieldTotalDistance, v.stats.getTotalCourseDistance(), v.stats.totalAliveTime);
      distributionStackTotalDistance.add( new Stack(v, min, max) );
      min += v.stats.fitness;//getFitness_fromDistanceAndSpeed(fieldTotalDistance, v.stats.getTotalCourseDistance(), v.stats.totalAliveTime);
    }

    return distributionStackTotalDistance;
  }


  float calcFitness_fromDistanceAndSpeed(float fieldTotalDistance, float totalDistance, float totalTime) {
    //println(fieldTotalDistance,totalDistance,totalTime);
    if (Float.isNaN(totalDistance)) {
      return 0.00001;
    } 
    if (totalTime==0) {
      return 0.00001;
    } else {
      float distancePercentage = max(0, min(1, totalDistance / fieldTotalDistance));
      boolean finishedRace = distancePercentage==1;
      float finishedRaceFac = finishedRace ? 1 : 0;
      float averageSpeed = totalDistance / totalTime;
      float speedPercentage = max(0, min(1, averageSpeed / Z.FOR_FITNESS_MAX_AVERAGE_SPEED));
      float fitness = finishedRaceFac + distancePercentage + (distancePercentage*distancePercentage)*speedPercentage;
      //println(fitness);
      //if (Float.isNaN(fitness)) println("ERROR HERE?");
      return fitness;
    }
  }







  ArrayList<Vehicle> reproduce(ArrayList<Stack> stacks, ArrayList<Vehicle> population, int amount, int newGenIndex) {

    //int leastBreedingTargetSize = population.size();
    //int maxBreedingTargetSize = (int)(Z.BREEDING_RATE_MIN*population.size());
    //int targetPairs = max(leastBreedingTargetSize, min(maxBreedingTargetSize, Z.MAX_GENERATION_SIZE));
    
    //println(generationIndex+" BREED NEW: target="+targetPairs+", adding "+deltaPairs);

    ArrayList<Vehicle> children = new ArrayList<Vehicle>();
    for (int pairIndex=0; pairIndex<amount; pairIndex++) {

      //println("make pair "+pairIndex);
      // find parents
      Stack parent0Data = chooseParent( stacks, null );
      Stack parent1Data = chooseParent( stacks, Z.SELF_BREEDING ? null : parent0Data );
      Vehicle parent0 = parent0Data.vehicle;
      Vehicle parent1 = parent1Data.vehicle;

      // make DNA
      // turn NetworkWeights into a flat array
      float[] parent0SortedWeights = new NetworkWeights( parent0.brain ).sortedWeightsToFloatArray();
      float[] parent1SortedWeights = new NetworkWeights( parent1.brain ).sortedWeightsToFloatArray();
      float[] childWeights = crossOverAndMutate(parent0SortedWeights, parent1SortedWeights);
      NetworkWeights childBrainWeights = new NetworkWeights( parent0.brain ).createNewFromSortedWeights(childWeights);

      Vehicle vehicle = new Vehicle();
      
      String pname0 = parent0.name.split(" ")[0];
      String pname1 = parent1.name.split(" ")[0];
      
      vehicle.name = "g"+newGenIndex+"_"+pairIndex+", child of "+pname0+" and "+pname1;
      children.add(vehicle);
      vehicle.brain.applyWeights( childBrainWeights );
    }
    
    return children;
  }



  /*
  *
   *  Chooses a parent from the distribution stack
   *  if stack total = 0, choose randomly
   *
   */
  Stack chooseParent(ArrayList<Stack> distributionStack, Stack otherParent) {
    int stackSize = distributionStack.size();
    float max = distributionStack.get( stackSize-1 ).max;
    if (max==0 || (otherParent!=null && otherParent.min==0 && otherParent.max==max)) {

      return distributionStack.get( (int)random(stackSize) );
    } else {

      Stack out = otherParent;

      while ((otherParent==null&&out==null) || (out!=null&&otherParent!=null&&out.vehicle == otherParent.vehicle)) {

        float rnd = random(max);

        for (Stack range : distributionStack) {
          if (rnd>=range.min && rnd<range.max) {
            out = range;
          }
        }
      }
      return out;

      //throw new RuntimeException("Meh, something is wrong from the stack. rnd="+rnd);
    }
  }



  /*
  *
   *  CROSSOVER AND MUTATE
   *
   */

  float[] crossOverAndMutate(float[] parent0SortedWeights, float[] parent1SortedWeights) {


    int dnaLength = parent0SortedWeights.length;
    int splitPoint = (int)random(dnaLength+1);
    float[] childDna = new float[dnaLength];

    for (int genIndex=0; genIndex<dnaLength; genIndex++) {

      // crossover
      float childWeight = genIndex<splitPoint ? parent0SortedWeights[genIndex] : parent1SortedWeights[genIndex];

      // mutation
      if (random(1.0)<Z.MUTATION_CHANCE_PER_WEIGHT) {
        childWeight += Z.MUTATION_RANGE/2 * randomGaussian();//random(-MUTATION_RANGE/2, MUTATION_RANGE/2);
      }

      childDna[genIndex] = childWeight;
    }

    return childDna;
  }
}



class Stack {
  Vehicle vehicle;
  float min;
  float max;
  Stack(Vehicle v, float min, float max) {
    this.vehicle=v;
    this.min=min;
    this.max=max;
  }
  public String toString() { 
    return "Stack from vehicle("+vehicle.name+") range = "+min+" ... "+max;
  }
}
