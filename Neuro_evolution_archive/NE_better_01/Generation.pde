class Generation {

  GenerationRunningMode runningMode = GenerationRunningMode.NOT_INITIALIZED;

  PlayField field;

  ArrayList<Vehicle> population;

  Breeder breeder = new Breeder();

  boolean showVehicleInfo = true;
  Vehicle activeShowVehicle = null;

  boolean paused = false;
  float time;
  int genIndex = 0;


  Generation(PlayField newField) {
    if (newField==null)
      field = new PlayField();
    else
      field = newField;
  }



  void keyPressed() {
    if (key=='r') {
      runningMode = GenerationRunningMode.NOT_INITIALIZED;
    }
    if (key=='i') {
      showVehicleInfo = !showVehicleInfo;
    }
    if (key=='p') {
      paused = !paused;
    }
    if (key=='k') {
      if (activeShowVehicle!=null) {
        activeShowVehicle.stats._totalCourseDistance = 0;
        activeShowVehicle.dead = true;
      }
    }
  }


  void integrate(float dt) {

    if (paused) return;

    switch(runningMode) {

    case NOT_INITIALIZED: 
      initializeGeneration();
      runningMode = GenerationRunningMode.RUN_GENERATION;
      break;

    case RUN_GENERATION:
      runGeneration(dt);
      break;

    case BREEDING_NEXT_GENERATION:
      population = breeder.breedNewGeneration(field, genIndex+1, field.totalDistance, population);
      genIndex++;
      runningMode = GenerationRunningMode.END_BREEDING_NEXT_GENERATION;
      //population = 
      break;

    case END_BREEDING_NEXT_GENERATION:
    runningMode = GenerationRunningMode.NOT_INITIALIZED;
      break;
    }
  }


  void initializeGeneration() {
    time = 0;
    if (population==null) {
      population = new ArrayList<Vehicle>();
      for (int i=0; i<Z.POPULATION_COUNT; i++) {
        Vehicle vehicle = new Vehicle();
        vehicle.setField(field);
        vehicle.name = "g"+genIndex+"_"+i;
        population.add(vehicle);
      }
    }
    setVehiclesOnStartLine();
    activeShowVehicle = null;
  }


  void setVehiclesOnStartLine() {
    for (Vehicle v : population) {
      v.initForStart();
      v.position = field.getStartPosition();
    }
  }



  void runGeneration(float dt) {
    time += dt;
    switch(runningMode) {
    case RUN_GENERATION:
      for (Vehicle v : population) {
        if (!v.dead) {
          v.useBrain();
          v.integrate(dt);
          //v.increaseSpeed(random(1.0)<0.01);
          v.checkSensors();
          v.checkDeathConditions();
        }
      }
      if (activeShowVehicle!=null && activeShowVehicle.dead) {
        activeShowVehicle=null;
      }
      if (countAliveVehicles()==0) {
        runningMode = GenerationRunningMode.BREEDING_NEXT_GENERATION;
      }
      break;

    default:
      break;
    }
  }






  void draw() {
    
    field.draw();
    
    fill(255);

    startText("Generation "+genIndex, 10, 20);
    textIndent("  ");
    text("runningMode = "+runningMode);
    text("time = "+nf(time, 1, 2)+"s");
    text("alive vehicles = "+countAliveVehicles());
    text("[k] to kill selected vehicle (and make fitness 0)");
    text("draw vehicle [i]nfo: "+showVehicleInfo);
    text("[-]/[+] frameRate = "+(int)targetFramerate+" ("+(int)frameRate+")");
    if (paused) {
      if (millis()%1000<500) {
        text("--- [P]AUSED ---");
      }
    }

    switch(runningMode) {
    case RUN_GENERATION:
      for (Vehicle v : population) {
        if (!v.dead) {
          v.draw();
        }
      }
      if (showVehicleInfo) {
        if (activeShowVehicle!=null && mousePressed) {
          activeShowVehicle=null;
        }

        if (activeShowVehicle!=null) {
          activeShowVehicle.drawInfo();
        } else {
          Vehicle v = getClosestVehicle();
          if (v!=null && mousePressed) {
            activeShowVehicle = v;
          }
          if (v!=null) 
            v.drawInfo();
        }
      }

      break;

    default:
      break;
    }
  }

  int countAliveVehicles() {
    int count = 0;
    for (Vehicle v : population) count += v.dead?0:1;
    return count;
  }

  Vehicle getClosestVehicle() {
    Vehicle closestVehicle = null;
    float closestDistance = Z.DRAW_VEHICLE_INFO_MIN_DISTANCE_SQ;//Float.MAX_VALUE;
    for (Vehicle v : population) {
      if (!v.dead) {
        PVector pos = v.position;
        float dx = pos.x-mouseX;
        float dy = pos.y-mouseY;
        float dsq = dx*dx+dy*dy;
        if (dsq<closestDistance) {
          closestDistance=dsq;
          closestVehicle = v;
        }
      }
    }

    return closestVehicle;
  }
}


enum GenerationRunningMode {
  NOT_INITIALIZED, 
    RUN_GENERATION, 
    BREEDING_NEXT_GENERATION, 
    END_BREEDING_NEXT_GENERATION
}
