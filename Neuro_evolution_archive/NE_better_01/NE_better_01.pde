import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.*;

int GLOBAL_MODE_BREEDING = 1;
int GLOBAL_MODE_CREATE_PLAYFIELD = 0;
int mode = GLOBAL_MODE_BREEDING;

Generation generation;
PlayField drawField = null;
Zettings Z;
int targetFramerate = 60;

void setup() {
  size(800, 600, P3D);
  Z = new Zettings();

  frameRate(targetFramerate);

  if (mode==GLOBAL_MODE_BREEDING) initBreeding(null);
  else if (mode==GLOBAL_MODE_CREATE_PLAYFIELD) initCreatePlayfield();
}

void initCreatePlayfield() {
  drawField = new PlayField();
  drawField.initMakePlayfield();
}

void initBreeding(PlayField field) {
  generation = new Generation(field);
}

void mousePressed() {
  if (mode==GLOBAL_MODE_CREATE_PLAYFIELD) {
    drawField.addPoint(mouseX, mouseY);
  }
}

void keyPressed() {
  
  if (key=='0' && mode!=GLOBAL_MODE_CREATE_PLAYFIELD) {
    mode = GLOBAL_MODE_CREATE_PLAYFIELD;
    initCreatePlayfield();
  } else if (key=='1' && mode!=GLOBAL_MODE_BREEDING) {
    mode = GLOBAL_MODE_BREEDING;
    if (!drawField.isInTheMaking()) {
      initBreeding(drawField);
    } else {
      initBreeding(null);
    }
  }

  if (mode==GLOBAL_MODE_BREEDING) {
    generation.keyPressed();
    if (key=='-' && targetFramerate>15) {
      targetFramerate-=15;
      frameRate(targetFramerate);
    }
    else if (key=='+') {
      targetFramerate+=15;
      frameRate(targetFramerate);
    }
  } else if (mode==GLOBAL_MODE_CREATE_PLAYFIELD) {
    if (key=='f') {
      drawField.finalize();
    }
    else if (key=='r') {
      initCreatePlayfield();
    }
  }
}

void draw() {
  float dt = 0.016;//1.0/frameRate;
  background(Z.BACKGROUND_COLOR);

  if (mode==GLOBAL_MODE_CREATE_PLAYFIELD) {
    drawField.draw();
  } else if (mode==GLOBAL_MODE_BREEDING) {
    generation.integrate(dt);
    generation.draw();
  }
}
