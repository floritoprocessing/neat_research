class NodeBrain {

  ArrayList<Node> _nodes;
  ArrayList<NodeConnection> _connections;
  ArrayList<ArrayList<Node>> _feedForwardOrder;
  boolean _feedForwardOrderCreated = false;


  NodeBrain() {
    clear();
  }



  void clear() {
    // clear nodes and connections
    _nodes = new ArrayList<Node>();
    _connections = new ArrayList<NodeConnection>();
    _feedForwardOrder = new ArrayList<ArrayList<Node>>();
  }



  /*
  *
  *    CREATE STANDARD
  *
  */
  

  void createStandard(int[] nodesPerLayer, String[][] nodeNames) {

    clear();

    int uniqueID = 0;
    int uniqueConnectionID = 0;
    int layerCount = nodesPerLayer.length;

    ArrayList<Node> previousLayerNodes = null;

    // iterate through layers
    for (int i=0; i<layerCount; i++) {

      boolean firstLayer = i==0;
      String[] layerNodeNames = nodeNames[i];
      ArrayList<Node> layerNodes = new ArrayList<Node>();

      // iterate through nodes per layer
      int nodeCount = nodesPerLayer[i];
      for (int j=0; j<nodeCount; j++) {

        // create node:
        int nodeID = uniqueID;
        uniqueID++;
        String nodeName = "n"+nf(i, 2)+"_"+nf(j, 2);
        if (layerNodeNames!=null) {
          nodeName = layerNodeNames[j];
        }

        Node node = new Node(nodeID, false, nodeName);
        layerNodes.add(node);
        _nodes.add(node);
        
        // add bias node with connection
        if (Z.ADD_BIAS_CONNECTION && !firstLayer) {
          nodeID = uniqueID;
          uniqueID++;
          Node biasNode = new Node(nodeID, true, nodeName+"_bias");
          
          _nodes.add(biasNode);
          NodeConnection connection = createConnection(uniqueConnectionID,biasNode, node);
          uniqueConnectionID++;
          _connections.add(connection);
          //println(connection);
        }


        // create connections for 2nd and rest layers
        if (previousLayerNodes!=null) {
          for (Node previousLayerNode : previousLayerNodes) {
            NodeConnection connection = createConnection(uniqueConnectionID, previousLayerNode, node);
            uniqueConnectionID++;
            _connections.add(connection);
          }
        } // end create connections
        
        
        
      } // end for nodes in layer
      

      previousLayerNodes = layerNodes;
    } // end for iterating through layers

    createFeedForwardOrder();
  } // end create standard




  /*
  *
  *    CREATE FEED FORWARD ORDER
  *
  */


  void createFeedForwardOrder() {
    boolean DEBUG = false;
    // find nodes without input

    if (DEBUG) println("createFeedForwardOrder()");

    ArrayList<Node> firstLayer = new ArrayList<Node>();
    for (Node node : _nodes) {
      //println("Testing node "+node+" for input and bias: inputs:"+node.hasInputConnections()+" && bias:"+node.biasNode);
      if (!node.hasInputConnections() && !node.biasNode) {
        firstLayer.add(node);
      }
    }
    _feedForwardOrder.add(firstLayer);
    if (DEBUG) println("First layer");
    if (DEBUG) println(firstLayer);


    boolean secondLayerHasNodes; 


    do {

      // get connections that are connected to the first Layer
      ArrayList<NodeConnection> connections = new ArrayList<NodeConnection>();
      for (Node node : firstLayer) {
        connections.addAll( node._outputConnections );
      }
      //println(connections);

      // get nodes that are connected to these connections
      ArrayList<Node> secondLayer = new ArrayList<Node>();
      for (NodeConnection nc : connections) {
        Node receiver = nc.destination;
        if (!secondLayer.contains(receiver))
          secondLayer.add(receiver);
      }
      secondLayerHasNodes = secondLayer.size()>0;

      if (secondLayerHasNodes) {
        //check for nodes that feed back to previous nodes to avoid recursion
        removeFeedbackNodes(secondLayer, _feedForwardOrder);
        _feedForwardOrder.add(secondLayer);
      }

      if (DEBUG) println();
      if (DEBUG) println("Second layer ("+(secondLayerHasNodes?secondLayer.size():"-empty-")+")");
      if (DEBUG) println(secondLayer);

      firstLayer = secondLayer;
    } while (secondLayerHasNodes);


    if (DEBUG) println("end createFeedForwardOrder()");
    
    _feedForwardOrderCreated = true;
  }
  
  
  
  void removeFeedbackNodes(ArrayList<Node> nodes, ArrayList<ArrayList<Node>> alreadyRegisteredNodes) {
    for (int i=0;i<nodes.size();i++) {    
      Node node = nodes.get(i);
      if (existsIn(alreadyRegisteredNodes,node)) {
        nodes.remove(i);
        i--;
      }  
    }
  }
  
  
  boolean existsIn(ArrayList<ArrayList<Node>> alreadyRegisteredNodes, Node testNode) {
    for (ArrayList<Node> nodes : alreadyRegisteredNodes) {
      for (Node node:nodes) {
        if (node==testNode) return true;
      }
    }
    return false;
  }
  
  
  
  
  
  /*
  *
  *    FEED FORWARD
  *
  */
  
  
  
  
  
  
  void feedForward(float[] inputValues) {
    
    if (!_feedForwardOrderCreated) {
      throw new RuntimeException("Feed forward order not created!");
    }
    
    // iterate layers
    int layers = _feedForwardOrder.size();
    for (int i=0;i<layers;i++) {
      
      ArrayList<Node> layer = _feedForwardOrder.get(i);
      int nodeCount = layer.size();
      
      // first layer
      if (i==0) {
        if (inputValues.length != nodeCount) {
          throw new RuntimeException("Input values not the same as input node count");
        } else {
          for (int j=0;j<nodeCount;j++) {
            Node node = layer.get(j);
            node.setFeedForwardResult( inputValues[j] );
          }
        }
      }
      
      // next layers
      else {
        for (int j=0;j<nodeCount;j++) {
          Node node = layer.get(j);
          node.calculateFeedForwardResult();
        }
      }
      
      
    }
  }
  
  
  
  
  float[] getLastLayerFeedForwardResult() {
    ArrayList<Node> lastLayer = _feedForwardOrder.get(_feedForwardOrder.size()-1);
    float[] out = new float[lastLayer.size()];
    for (int i=0;i<out.length;i++) {
      Node node = lastLayer.get(i);
      out[i] = node.getFeedForwardResult();
    }
    return out;
  }
  
  
  
  
  
  
  
  
  void printFeedForwardStructure() {
    printFeedForwardStructure(false);
  }
  
  void printFeedForwardStructure(boolean detailed) {
    println("Brain structure (feed forward without showing bias nodes):");
    for (int i=0;i<_feedForwardOrder.size();i++) {
      println("Layer "+i);
      print(_feedForwardOrder.get(i));
    }
    if (detailed) {
      for (int i=0;i<_connections.size();i++) {
        NodeConnection nc = _connections.get(i);
        print("Conenction "+i+": "+nc.toComplexString());
      }
    }
  }
  
  
  
  /*
  *
  *    SERIALISATION: CREATE FROM JSON
  *
  */
  
  final static String KEY_NODE_COUNT = "nodeCount";
  final static String KEY_NODES = "nodes";
  final static String KEY_CONNECTION_COUNT = "connectionCount";
  final static String KEY_CONNECTIONS = "connections";
  
  NodeBrain(JSONObject json) {
    clear();
    
    int nodeCount = json.getInt(KEY_NODE_COUNT);
    JSONArray nodes = json.getJSONArray(KEY_NODES);
    for (int i=0;i<nodeCount;i++) {
      JSONObject nodeJSON = nodes.getJSONObject(i);
      Node node = new Node(nodeJSON);
      _nodes.add(node);
    }
    
    int connectionCount = json.getInt(KEY_CONNECTION_COUNT);
    JSONArray connections = json.getJSONArray(KEY_CONNECTIONS);
    for (int i=0;i<connectionCount;i++) {
      JSONObject connectionJson = connections.getJSONObject(i);
      NodeConnection connection = new NodeConnection(connectionJson, _nodes);
      _connections.add(connection);
    }
    
    createFeedForwardOrder();
  }
  
  
  
  
  
  
  
  JSONObject toJSON() {
    JSONObject json = new JSONObject();
    
    int nodeCount = _nodes.size();
    json.setInt(KEY_NODE_COUNT, nodeCount);
    
    JSONArray nodes = new JSONArray();
    json.setJSONArray(KEY_NODES,nodes);
    
    for (int i=0;i<nodeCount;i++) {
      Node node = _nodes.get(i);
      nodes.setJSONObject(i,node.toJSON());
    }
    
    
    
    int connectionCount = _connections.size();
    json.setInt(KEY_CONNECTION_COUNT, connectionCount);
    
    JSONArray connections = new JSONArray();
    json.setJSONArray(KEY_CONNECTIONS,connections);
    
    for (int i=0;i<connectionCount;i++) {
      NodeConnection connection = _connections.get(i);
      connections.setJSONObject(i,connection.toJSON());
    }
    
    return json;
  }
  
  
}
