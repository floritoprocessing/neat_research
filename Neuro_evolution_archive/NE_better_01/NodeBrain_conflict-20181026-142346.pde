class NodeBrain {
  
  ArrayList<Node> _nodes;
  ArrayList<NodeConnection> _connections;
  ArrayList<ArrayList<Node>> _nodesToFeedForward;
  
  
  
  NodeBrain() {
    clear();
  }
  
  
  
  void clear() {
    // clear nodes and connections
    _nodes = new ArrayList<Node>();
    _connections = new ArrayList<NodeConnection>();
    _nodesToFeedForward = new ArrayList<ArrayList<Node>>();
  }
  
  
  
  void createStandard(int[] nodesPerLayer, String[][] nodeNames) {
    
    clear();
    
    int uniqueID = 0;
    int layerCount = nodesPerLayer.length;
    
    ArrayList<Node> previousLayerNodes = null;
    
    // iterate through layers
    for (int i=0;i<layerCount;i++) {
      
      String[] layerNodeNames = nodeNames[i];
      ArrayList<Node> layerNodes = new ArrayList<Node>();
      
      // iterate through nodes per layer
      int nodeCount = nodesPerLayer[i];
      for (int j=0;j<nodeCount;j++) {
        
        // create node:
        int nodeID = uniqueID;
        uniqueID++;
        String nodeName = "n"+nf(i,2)+"_"+nf(j,2);
        if (layerNodeNames!=null) {
          nodeName = layerNodeNames[j];
        }
        
        Node node = new Node(nodeID, nodeName);
        layerNodes.add(node);
        _nodes.add(node);
        
        
        // create connections for 2nd and rest layers
        if (previousLayerNodes!=null) {
          for (Node previousLayerNode : previousLayerNodes) {
            NodeConnection connection = createConnection(previousLayerNode, node);
            _connections.add(connection);
          }         
        } // end create connections
        
      } // end for nodes in layer
      
      previousLayerNodes = layerNodes;
      
    } // end for iterating through layers
    
    createFeedForwardOrder();
  } // end create standard
  
  
  
  
  
  
  void createFeedForwardOrder() {
    // find nodes without input
    
    println("createFeedForwardOrder()");
    
    ArrayList<Node> firstLayer = new ArrayList<Node>();
    for (Node node:_nodes) {
      if (! node.hasInputConnections()) {
        firstLayer.add(node);
      }
    }
    println(firstLayer);
    
    
    
    // get connections from first Layer
    ArrayList<NodeConnection> connections = new ArrayList<NodeConnection>();
    
    
    
    println("end createFeedForwardOrder()");
  }
  
}
