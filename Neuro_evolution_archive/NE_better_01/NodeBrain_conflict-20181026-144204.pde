class NodeBrain {

  ArrayList<Node> _nodes;
  ArrayList<NodeConnection> _connections;
  ArrayList<ArrayList<Node>> _nodesToFeedForward;



  NodeBrain() {
    clear();
  }



  void clear() {
    // clear nodes and connections
    _nodes = new ArrayList<Node>();
    _connections = new ArrayList<NodeConnection>();
    _nodesToFeedForward = new ArrayList<ArrayList<Node>>();
  }



  void createStandard(int[] nodesPerLayer, String[][] nodeNames) {

    clear();

    int uniqueID = 0;
    int layerCount = nodesPerLayer.length;

    ArrayList<Node> previousLayerNodes = null;

    // iterate through layers
    for (int i=0; i<layerCount; i++) {

      String[] layerNodeNames = nodeNames[i];
      ArrayList<Node> layerNodes = new ArrayList<Node>();

      // iterate through nodes per layer
      int nodeCount = nodesPerLayer[i];
      for (int j=0; j<nodeCount; j++) {

        // create node:
        int nodeID = uniqueID;
        uniqueID++;
        String nodeName = "n"+nf(i, 2)+"_"+nf(j, 2);
        if (layerNodeNames!=null) {
          nodeName = layerNodeNames[j];
        }

        Node node = new Node(nodeID, nodeName);
        layerNodes.add(node);
        _nodes.add(node);


        // create connections for 2nd and rest layers
        if (previousLayerNodes!=null) {
          for (Node previousLayerNode : previousLayerNodes) {
            NodeConnection connection = createConnection(previousLayerNode, node);
            _connections.add(connection);
          }
        } // end create connections
      } // end for nodes in layer

      previousLayerNodes = layerNodes;
    } // end for iterating through layers

    createFeedForwardOrder();
  } // end create standard






  void createFeedForwardOrder() {
    boolean DEBUG = false;
    // find nodes without input

    if (DEBUG) println("createFeedForwardOrder()");

    ArrayList<Node> firstLayer = new ArrayList<Node>();
    for (Node node : _nodes) {
      if (! node.hasInputConnections()) {
        firstLayer.add(node);
      }
    }
    _nodesToFeedForward.add(firstLayer);
    if (DEBUG) println("First layer");
    if (DEBUG) println(firstLayer);


    boolean secondLayerHasNodes; 


    do {

      // get connections that are connected to the first Layer
      ArrayList<NodeConnection> connections = new ArrayList<NodeConnection>();
      for (Node node : firstLayer) {
        connections.addAll( node._outputConnections );
      }
      //println(connections);

      // get nodes that are connected to these connections
      ArrayList<Node> secondLayer = new ArrayList<Node>();
      for (NodeConnection nc : connections) {
        Node receiver = nc.destination;
        if (!secondLayer.contains(receiver))
          secondLayer.add(receiver);
      }
      secondLayerHasNodes = secondLayer.size()>0;

      if (secondLayerHasNodes) {
        //check for nodes that feed back to previous nodes to avoid recursion
        removeFeedbackNodes(secondLayer, _nodesToFeedForward);
        _nodesToFeedForward.add(secondLayer);
      }

      if (DEBUG) println();
      if (DEBUG) println("Second layer ("+(secondLayerHasNodes?secondLayer.size():"-empty-")+")");
      if (DEBUG) println(secondLayer);

      firstLayer = secondLayer;
    } while (secondLayerHasNodes);


    if (DEBUG) println("end createFeedForwardOrder()");
  }
  
  
  
  void removeFeedbackNodes(ArrayList<Node> nodes, ArrayList<ArrayList<Node>> alreadyRegisteredNodes) {
    for (int i=0;i<nodes.size();i++) {    
      Node node = nodes.get(i);
      if (existsIn(alreadyRegisteredNodes,node)) {
        nodes.remove(i);
        i--;
      }  
    }
  }
  
  
  boolean existsIn(ArrayList<ArrayList<Node>> alreadyRegisteredNodes, Node testNode) {
    for (ArrayList<Node> nodes : alreadyRegisteredNodes) {
      for (Node node:nodes) {
        if (node==testNode) return true;
      }
    }
    return false;
  }
  
  
  
  
  
  
  
  
  void printFeedForwardStructure() {
    println("Brain structure (feed forward):");
    for (int i=0;i<_nodesToFeedForward.size();i++) {
      
    }
  }
  
  
  
}
