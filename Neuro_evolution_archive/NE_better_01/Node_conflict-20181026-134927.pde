class Node {
  
  final int ID;
  
  ArrayList<NodeConnection> inputConnections = new ArrayList<NodeConnection>();
  
  Node(int uniqueID) {
    ID = uniqueID;
  }
  
  void addInput(NodeConnection connection) {
    inputConnections.add(connection);
  }
  
  void feedForward() {
    float sum = 0;
    for (NodeConnection input:inputConnections) {
      NodeConnection conenction = inputConnections.get(i);
      sum += inputs[i].getNextOutput()*weights[i];
    }
    nextOutput = activate(sum);
  }
  
}
