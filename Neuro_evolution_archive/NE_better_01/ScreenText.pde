float screenTextX, screenTextY;

void startText(String text, float x, float y) {
  text(text,x,y);
  screenTextX=x;
  screenTextY=y;
  screenTextNextLine();
}

void textIndent(String s) {
  screenTextX += textWidth(s);
}

void text(String text) {
  text(text, screenTextX, screenTextY);
  screenTextNextLine();
}

void screenTextNextLine() {
   screenTextY += textAscent()+textDescent();
}
