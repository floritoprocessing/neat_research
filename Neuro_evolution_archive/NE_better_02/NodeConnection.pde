class NodeConnection {

  final int ID;
  final Node source;
  final Node destination;
  float weight;
  final String asString;
  final String complexString;

  NodeConnection clone(ArrayList<Node> nodes) {
    NodeConnection out = new NodeConnection(
      ID,
      source==null ? null : getNodeByID(nodes,source.ID),
      destination==null ? null : getNodeByID(nodes,destination.ID),
      weight);
    return out;
  }

  NodeConnection(int uniqueID, Node source, Node destination, float weight) {
    this.ID = uniqueID;
    this.source = source;
    this.destination = destination;
    this.weight = weight;

    destination.addInput(this);
    source.addOutput(this);

    asString = source.name+"->"+destination.name;
    complexString = source.toString()+" --("+nf(weight, 1, 5)+")--> "+destination.toString();
  }

  public String toString() {
    return asString;
  }
  public String toComplexString() {
    return complexString;
  }
  
  /*
  *
  *    SERIALISATION
  *
  */
  
  final static String KEY_ID = "ID";
  final static String KEY_SOURCE_ID = "sourceID";
  final static String KEY_DESTINATION_ID = "destinationID";
  final static String KEY_WEIGHT = "weight";
  
  
  
  NodeConnection(JSONObject json, ArrayList<Node> nodes) {
    this(
      json.getInt(KEY_ID), 
      getNodeByID(nodes, json.getInt(KEY_SOURCE_ID)), 
      getNodeByID(nodes, json.getInt(KEY_DESTINATION_ID)), 
      json.getFloat(KEY_WEIGHT));
  }
  
  JSONObject toJSON() {
    JSONObject out = new JSONObject();
    out.setInt(KEY_ID,ID);
    out.setInt(KEY_SOURCE_ID,source.ID);
    out.setInt(KEY_DESTINATION_ID,destination.ID);
    out.setFloat(KEY_WEIGHT,weight);
    return out;
  }
  
}

NodeConnection getNodeConnectionByID(ArrayList<NodeConnection> connections, int ID) {
  ArrayList<NodeConnection> out = new ArrayList<NodeConnection>();
  for (NodeConnection connection:connections) {
    if (ID==connection.ID) out.add(connection);
  }
  if (out.size()==0) throw new RuntimeException("connections with ID="+ID+" not found");
  else if (out.size()>1) throw new RuntimeException("connections with ID="+ID+" exists "+out.size()+" times!");
  else {
    return out.get(0);
  }
}

NodeConnection createConnection(int uniqueID, Node source, Node destination) {
  NodeConnection nc = new NodeConnection(uniqueID, source, destination, initWeight());
  return nc;
}

float initWeight() {
  return random(-Z.RANDOM_INIT_WEIGHT_VALUE, Z.RANDOM_INIT_WEIGHT_VALUE);
}
