// VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE





class Vehicle {

  private PlayField _field;

  final float turningSpeed; // degrees per second
  final float thrustSpeed; // acceleration in pixels/sˆ2
  final float breakSpeed; // break in pixels/sˆ2
  final float size;
  final PVector[] points;
  final PVector[] localSensorVecs;

  String name = "noName";

  boolean singleFrameHighlight;

  RunningAverage averageSpeed = new RunningAverage(Z.VEHICLE_AVERAGE_SPEED_FRAMES);

  PVector lastPosition = new PVector();
  PVector position = new PVector();
  PVector velocity = new PVector();
  float direction;
  float speed;

  boolean[] inSensorVecRange; 
  float[] inSensorVecDistance; // percentages

  boolean dead;
  VehicleStats stats = new VehicleStats();
  NodeBrain nodeBrain;

  boolean turningLeft=false, turningRight=false, increasingSpeed=false, decreasingSpeed=false;

  Vehicle() {
    this(Z.VEHICLE_TURNING_SPEED, Z.VEHICLE_THRUST_SPEED, Z.VEHICLE_BREAK_SPEED, Z.VEHICLE_SIZE);
  }

  Vehicle(float turningSpeed, float thrustSpeed, float breakSpeed, float size) {
    this.turningSpeed = turningSpeed;
    this.thrustSpeed = thrustSpeed;
    this.breakSpeed = breakSpeed;
    this.size = size;
    this.points = new PVector[] {  new PVector(-size, size), new PVector(0, -size*1.5), new PVector(size, size) };

    localSensorVecs = new PVector[Z.VEHICLE_SENSORS];
    for (int i=0; i<Z.VEHICLE_SENSORS; i++) {
      float rd = map(i, 0, Z.VEHICLE_SENSORS-1, -radians(Z.VEHICLE_SENSOR_DEGREES), radians(Z.VEHICLE_SENSOR_DEGREES));
      float len = size*(i==(Z.VEHICLE_SENSORS-1)/2 ? Z.VEHICLE_SIZE_TO_CENTRAL_SENSOR_FAC : Z.VEHICLE_SIZE_TO_SENSOR_FAC);
      PVector sensorVec = new PVector(0, -len);
      sensorVec.rotate(rd);
      localSensorVecs[i] = sensorVec;
    }

    reset();
    //initForStart();
    //initDirectionAndSpeed(radians(45), 0);


    initBrain();
  }


  void initForStart() {
    reset();
    position = _field.getStartPosition();
    initDirectionAndSpeed( _field.getStartDirection(), 0);
  }

  void reset() {
    singleFrameHighlight = false;
    averageSpeed.reset();

    lastPosition = new PVector();
    position = new PVector();
    velocity = new PVector();
    direction = 0;
    speed = 0;

    inSensorVecRange = new boolean[Z.VEHICLE_SENSORS];
    inSensorVecDistance = new float[Z.VEHICLE_SENSORS];

    dead = false;
    stats.reset();
  }


  void setField(PlayField field) {
    this._field = field;
  }

  void initBrain() {

    String[] inputNames = new String[Z.VEHICLE_SENSORS+1];
    int half = (Z.VEHICLE_SENSORS-1)/2;
    for (int i=0; i<inputNames.length-1; i++) {
      if (i<half) inputNames[i]="left"+(i);
      else if (i==half) inputNames[i]="center";
      else if (i>half) inputNames[i]="right"+(inputNames.length-half-i+2);
    }
    inputNames[inputNames.length-1] = "speed";

    String[][] names = new String[][] {
      inputNames, 
      null, 
      //null,
      new String[] { "left", "right", "thrust", "break" }
    };

    int hiddenNeuronAmount0 = (int)(inputNames.length * Z.INPUT_NEURON_AMOUNT_TO_HIDDEN_NEURON_AMOUNT_FACTOR);
    //int hiddenNeuronAmount1 = (int)(4 * OUTPUT_NEURON_AMOUNT_TO_HIDDEN_NEURON_AMOUNT_FACTOR);

    //brain = new Network(new int[] {Z.VEHICLE_SENSORS+1, hiddenNeuronAmount0, 4}, names);

    nodeBrain = new NodeBrain();
    nodeBrain.createStandard(new int[] {Z.VEHICLE_SENSORS+1, hiddenNeuronAmount0, 4}, names);
  }



  void turnAndThrust(boolean left, boolean right, boolean thrust, boolean brek) {
    turningLeft=left;
    turningRight=right;
    increasingSpeed=thrust;
    decreasingSpeed=brek;
  }
  void turnLeft(boolean turn) {
    turningLeft = turn;
  }
  void turnRight(boolean turn) {
    turningRight = turn;
  }
  void increaseSpeed(boolean thrust) {
    increasingSpeed = thrust;
  }
  void decreaseSpeed(boolean doBreak) {
    decreasingSpeed = doBreak;
  }


  void initDirectionAndSpeed(float dir, float spd) {
    this.direction = dir;
    this.speed = spd;
    setVelocityFromSpeedAndDirection();
  }


  void setVelocityFromSpeedAndDirection() {
    velocity = new PVector(0, -1);
    velocity.rotate(this.direction);
    velocity.setMag(this.speed);
  }


  void checkDeathConditions() {
    boolean boundaryCross = _field.checkBoundaryCrossing(lastPosition, position);
    if (boundaryCross) {
      dead = true;
      stats.deathReason = VehicleDeathReason.BOUNDARY_CROSS;
    } else if (averageSpeed.isComplete() && averageSpeed.getAverage()<Z.VEHICLE_AVERAGE_SPEED_DEATH) {
      dead = true;
      stats.deathReason = VehicleDeathReason.AVERAGE_SPEED_TOO_LOW;
    }
  }



  void useBrain() {

    float[] inputValues = new float[inSensorVecDistance.length+1];
    for (int i=0; i<inputValues.length-1; i++) {
      inputValues[i] = inSensorVecDistance[i];
    }
    inputValues[inputValues.length-1] = speed / 200.0;
    nodeBrain.feedForward(inputValues);

    float[] outputs = nodeBrain.getLastLayerFeedForwardResult();
    boolean left = outputs[0]>0;
    boolean right = outputs[1]>0;
    boolean thrust = outputs[2]>0;
    boolean brek = outputs[3]>0;
    turnAndThrust(left, right, thrust, brek);
  }




  void integrate(float seconds) { 
    lastPosition.set(position);
    applyTurningAndSpeed(seconds);
    averageSpeed.add(speed);
    updateStatsTotalCourseDistance();
    stats.age(seconds);
  }


  void updateStatsTotalCourseDistance() {
    float lastTotalCourseDistance = stats.singleRoundDistance;
    float currentCourseDistance = _field.getCourseDistance( position );
    // check cross startLine  
    Line startLine = _field.getStartLine();
    boolean crossStartLine = lineSegmentsIntersect( lastPosition, position, startLine.getP0(), startLine.getP1() );
    if (crossStartLine) {
      if (lastTotalCourseDistance>currentCourseDistance) {
        //println("CROSSSS!!!!!!!!!!! in correct direction");
        stats.courseRounds++;
      } else {
        //println("CROSSSS!!!!!!!!!!! in REVERSE direction");
        stats.courseRounds--;
      }
    }
    stats.singleRoundDistance = currentCourseDistance;
    stats.updateTotalCourseDistance( _field );
  }



  void applyTurningAndSpeed(float seconds) {
    if (turningLeft||turningRight||increasingSpeed||decreasingSpeed) {
      float deltaTurnRad = 0;
      float deltaSpeed = 0;
      if (turningLeft||turningRight) {
        float deltaTurnDeg = ((turningRight?1:0)+(turningLeft?-1:0)) * turningSpeed * seconds;
        deltaTurnRad = radians(deltaTurnDeg);
      }
      if (increasingSpeed||decreasingSpeed) {
        deltaSpeed = ((increasingSpeed?thrustSpeed:0)-(decreasingSpeed?breakSpeed:0)) * seconds;
      }
      float newSpeed = max(0, speed+deltaSpeed);
      initDirectionAndSpeed( direction+deltaTurnRad, newSpeed );
    }

    position = PVector.add( position, PVector.mult( velocity, seconds ) );
  }




  void checkSensors() {

    //Line[][] LR = new Line[][] { leftLines, rightLines };
    TrackSegment[] segments = _field.trackSegments;
    for (int sensor=0; sensor<Z.VEHICLE_SENSORS; sensor++) {

      //local to global:
      PVector sensorPoint = localSensorVecs[sensor].copy();
      sensorPoint.rotate(direction);
      sensorPoint.add( position );
      Line sensorLine = new Line( position, sensorPoint );

      boolean intersect = false;


      float sensorDistance=0;
      float closestDistancePerc = Float.MAX_VALUE;

      for (int i=0; i<segments.length; i++) {
        if (lineSegmentsIntersect(segments[i].leftLine, sensorLine)) {
          PVector intersectionPoint = lineLineIntersection(sensorLine, segments[i].leftLine);
          float distToIntersectionPoint = PVector.sub(intersectionPoint, sensorLine.getP0()).mag();
          float distPerc = distToIntersectionPoint/sensorLine.getMagnitude();
          if (distPerc<closestDistancePerc) {
            closestDistancePerc=distPerc;
            sensorDistance = 1-distPerc;
          }
          intersect=true;

          //break;
        }
        if (lineSegmentsIntersect(segments[i].rightLine, sensorLine)) {
          PVector intersectionPoint = lineLineIntersection(sensorLine, segments[i].rightLine);
          float distToIntersectionPoint = PVector.sub(intersectionPoint, sensorLine.getP0()).mag();
          float distPerc = distToIntersectionPoint/sensorLine.getMagnitude();
          if (distPerc<closestDistancePerc) {
            closestDistancePerc=distPerc;
            sensorDistance = 1-distPerc;
          }
          intersect=true;
          break;
        }
      }
      inSensorVecRange[sensor] = intersect;
      inSensorVecDistance[sensor] = sensorDistance;
    }
  }









  void draw() {
    strokeWeight(1);
    pushMatrix();

    translate(position.x, position.y);

    for (int i=0; i<localSensorVecs.length; i++) {
      PVector globalSensorVec = localSensorVecs[i].copy();
      globalSensorVec.rotate(direction);
      stroke(inSensorVecDistance[i]*255, 255-inSensorVecDistance[i]*255, 0);
      line(0, 0, globalSensorVec.x, globalSensorVec.y);
    }


    rotate(direction);

    if (singleFrameHighlight) {
      fill(200, 100, 100);
      stroke(255, 0, 0);
      translate(0, 0, 1);
    } else {
      stroke(255);
      noFill();
    }

    beginShape(TRIANGLES);
    for (int i=0; i<points.length; i++) {
      vertex(points[i].x, points[i].y);
    }
    endShape();




    popMatrix();

    singleFrameHighlight = false;
  }




  void drawInfo() {

    singleFrameHighlight = true;
    float xOff = 30;
    float x = position.x+xOff;
    float y = position.y;
    float w = 350;
    float h = 250;
    if (x+w>width-10) x=position.x-xOff-w; // left of vehicle
    if (y+h>height-10) y=height-10-h;
    x=(int)x;
    y=(int)y;


    rectMode(CORNER);

    stroke(255);
    pushMatrix(); 

    translate(0, 0, 1);
    fill(Z.VEHICLE_INFO_RECT_FILL);
    rect(x, y, w, h);
    fill(255);
    startText(name, x+10, y+10+textAscent());
    String avspeed = "running average speed=" 
      + (averageSpeed.isComplete()?nf(averageSpeed.getAverage(), 1, 1):"-undef-");
    text(avspeed);

    translate(x+20, screenTextY);
    //brain.draw(w,h-(screenTextY-y));
    drawNodeBrain(nodeBrain, w, h-(screenTextY-y));

    popMatrix();
  }
}








class VehicleStats {

  VehicleDeathReason deathReason = VehicleDeathReason.NOT_SET;
  int courseRounds = 0;
  float singleRoundDistance = 0;
  private float _totalCourseDistance = 0;
  private float _maxTotalCourseDistance = 0;
  float totalAliveTime = 0;
  float fitness = 0;

  void reset() {
    courseRounds = 0;
    singleRoundDistance = 0;
    _totalCourseDistance = 0;
    courseRounds = 0;
    deathReason = VehicleDeathReason.NOT_SET;
    totalAliveTime = 0;
    fitness = 0;
  }

  void updateTotalCourseDistance(PlayField field) {
    _totalCourseDistance = 0;
    if (courseRounds>=0) {
      _totalCourseDistance = courseRounds*field.totalDistance + singleRoundDistance;
    } else {
      _totalCourseDistance = (courseRounds+1)*field.totalDistance - (field.totalDistance-singleRoundDistance);
    }
    _maxTotalCourseDistance = max(_maxTotalCourseDistance, _totalCourseDistance);
  }

  void age(float dt) {
    totalAliveTime+=dt;
  }

  float getTotalCourseDistance() {
    return _totalCourseDistance;
  }

  float getMaxTotalCourseDistance() {
    return _maxTotalCourseDistance;
  }
}

enum VehicleDeathReason {
  NOT_SET, 
    BOUNDARY_CROSS, 
    AVERAGE_SPEED_TOO_LOW
}
