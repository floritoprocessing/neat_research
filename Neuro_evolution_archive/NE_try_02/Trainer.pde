class Trainer {
  
  float idleTimeMaximum;
  float idleTimeEnd;
  float minimumAverageSpeed;
  float totalTime;
  float endTime;
  boolean endTraining = false;
  boolean finalEnd = false;
  
  ArrayList<TrainerResults> results = new ArrayList<TrainerResults>();
  TrainerResults currentResults;
  
  Trainer(float idleTimeMaximum, float idleTimeEnd, float minimumAverageSpeed, Network network) {
    this.idleTimeMaximum = idleTimeMaximum;
    this.idleTimeEnd = idleTimeEnd;
    this.minimumAverageSpeed = minimumAverageSpeed;
    nextRound(network);
  }
  
  void nextRound(Network network) {
    currentResults = new TrainerResults(new NetworkWeights(network),0);
    results.add(currentResults);
    totalTime = 0;
    endTraining = false;
    endTime = 0;
    finalEnd = false;
  }
  
  void integrate(Vehicle vehicle, float frameTime) {
    totalTime += frameTime;
    
    // check average speed
    float av = vehicle.stats.averageSpeed;
    if (!Float.isNaN(av)) {
      //prprintln("Average speed="+av);
      if (av<minimumAverageSpeed) endTraining=true;
    }
    
    // check idle
    if (vehicle.stats.totalDistanceSinceStart<5 && totalTime>idleTimeMaximum && endTraining==false) {
      endTraining = true;
      endTime = totalTime;
    }
    // check end
    if (endTraining && totalTime > endTime+idleTimeEnd) {
      currentResults.totalDistance = vehicle.stats.totalDistanceSinceStart;
      println("Results: "+results.size()+" (TODO sort by totalDistance)");
      finalEnd = true;
    }
    
  }
  
  
  
  
}



class TrainerResults {
  
  NetworkWeights networkWeights;
  float totalDistance;
  
  TrainerResults(NetworkWeights networkWeights, float totalDistance) {
    this.networkWeights = networkWeights;
    this.totalDistance = totalDistance;
  }
  
}
