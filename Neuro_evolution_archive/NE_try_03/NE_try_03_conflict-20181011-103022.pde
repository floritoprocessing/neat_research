int MODE_CREATE_GAME = 0;
int MODE_PLAY_GAME = 1;
int MODE_PLAY_BRAIN = 2;
int MODE_TRAINING_ROUNDS = 3;
int mode = MODE_PLAY_BRAIN;

int lastTime;
RaceGame raceGame;
Network network;
NetworkToVehicle networkToVehicle = new NetworkToVehicle();
Trainer trainer;

void setup() {
  size(1000, 600);

  raceGame = new RaceGame();
  resetNetwork();
  trainer = new Trainer(1, 0, 5);
  trainer.maxRounds = 30;
  if (mode==MODE_CREATE_GAME) raceGame.field.initMakePlayfield();
  if (mode==MODE_PLAY_BRAIN || mode==MODE_TRAINING_ROUNDS) {
    restartBrainGame();
  }
  lastTime = millis();
}

void resetNetwork() {
  network = new Network(3, 5, 4);
}

void mousePressed() {
  if (mode==MODE_CREATE_GAME) raceGame.field.addPoint(mouseX, mouseY);
}

void keyPressed() {
  if (mode==MODE_CREATE_GAME) {
    if (key=='f') {
      raceGame.field.finalize();
      mode = MODE_PLAY_GAME;
    }
  } else if (mode==MODE_PLAY_GAME) {
    if (key=='r') {
      raceGame.restartSingleVehicle();
    }
    if (key==CODED) {
      if (keyCode==LEFT) raceGame.vehicle.turnLeft(true);
      else if (keyCode==RIGHT) raceGame.vehicle.turnRight(true);
      else if (keyCode==UP) raceGame.vehicle.increaseSpeed(true);
      else if (keyCode==DOWN) raceGame.vehicle.decreaseSpeed(true);
    }
  } else if (mode==MODE_PLAY_BRAIN) {
    if (key=='r') {
      restartBrainGame();
    }
    else if (key=='l') {
      restartBrainGame();
      String[] fileLines = loadStrings("000 Candidate 2 totalDistance 319.txt");
      String[] networkSettings=null;
      for (int i=0;i<fileLines.length;i++) {
        if (fileLines[i].equals("networkWeights")) {
          int indexStart = i+1;
          int lines = (fileLines.length-indexStart);
          networkSettings = new String[lines];
          arrayCopy(fileLines,indexStart,networkSettings,0,lines);
          
          break;
        }
      }
      println(networkSettings);
      NetworkWeights networkWeights = new NetworkWeights(networkSettings);
      //networkWeights.applyTo(network);
    }
    
  }
}



void restartBrainGame() {
  resetNetwork();
  raceGame.restartSingleVehicle();
  networkToVehicle.reset();
  trainer.nextRound(raceGame.vehicle, network);
}



void keyReleased() {
  if (mode==MODE_PLAY_GAME) {
    if (keyCode==LEFT) raceGame.vehicle.turnLeft(false);
    else if (keyCode==RIGHT) raceGame.vehicle.turnRight(false);
    else if (keyCode==UP) raceGame.vehicle.increaseSpeed(false);
    else if (keyCode==DOWN) raceGame.vehicle.decreaseSpeed(false);
  }
}





void draw() {
  int ti = millis();
  float frameTime = 0.016;//(ti-lastTime)/1000.0;
  
  // integrate racegame
  raceGame.integrateSingleVehicle(frameTime);
  
  // integrate trainer
  if (mode==MODE_PLAY_BRAIN || mode==MODE_TRAINING_ROUNDS) {
    trainer.integrate(frameTime);
  }

  // feedForward brain
  boolean[] sensorActives = raceGame.vehicle.inSensorVecRange;
  float[] inputValues = new float[sensorActives.length];
  for (int i=0; i<inputValues.length; i++)
    inputValues[i] = sensorActives[i] ? 1 : -1;
  network.feedForward(inputValues);

  // brain game: BRAIN -> VEHICLE
  // brain game: END CONDITIONS
  if (mode==MODE_PLAY_BRAIN || mode==MODE_TRAINING_ROUNDS) {
    networkToVehicle.influenceVehicle();
    if (trainer.finalEnd) {
      if (mode==MODE_PLAY_BRAIN || trainer.results.size()<trainer.maxRounds) {
        trainer.sortResults();
        restartBrainGame();
      }
      else {
        trainer.sortResults();
        trainer.saveAllResults();
        exit();
      }
    }
  }
  
  //
  //
  // DRAWING
  //
  //
  
  background(255);
  fill(0);
  

  // DRAW brain game
  if (mode==MODE_PLAY_BRAIN || mode==MODE_TRAINING_ROUNDS) {
    text("Round "+(trainer.currentRound)+"/"+trainer.maxRounds+": Time="+nf(trainer.totalTime, 1, 2), 10, 20);
    if (!trainer.endTraining) {
      text("Training running. Distance travelled="+nf(raceGame.vehicle.stats.totalDistanceSinceStart, 1, 1), 10, 35);
    }
    if (trainer.endTraining && !trainer.finalEnd) {
      text("Ending game...", 10, 35);
    }
    if (trainer.results.size()>1) {
      text("Best so far is "+trainer.results.get(0).identifier+" with "+nf(trainer.results.get(0).totalDistance,1,1),10,50);
    }
  }
  
  // DRAW raceGame
  raceGame.draw();

  // DRAW network
  pushMatrix();
  translate(width/2+100, height/2);
  network.draw(40, 150, 80);
  popMatrix();


  


  lastTime = ti;
  //noLoop();
}
