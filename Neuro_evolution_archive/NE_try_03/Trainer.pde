import java.util.Comparator;  

class Trainer {

  float MINIMUM_DISTANCE_TO_TRAVEL = 5;
  float idleTimeMaximum;
  float idleTimeEnd;
  float minimumAverageSpeed;
  float totalTime;
  float endTime;
  boolean endTraining = false;
  boolean finalEnd = false;
  int currentRound = 0;
  int maxRounds = 0;

  Network currentNetwork;
  Vehicle currentVehicle;
  
  ArrayList<TrainerResults> results = new ArrayList<TrainerResults>();
  //TrainerResults currentResults;

  Trainer(float idleTimeMaximum, float idleTimeEnd, float minimumAverageSpeed) {
    this.idleTimeMaximum = idleTimeMaximum;
    this.idleTimeEnd = idleTimeEnd;
    this.minimumAverageSpeed = minimumAverageSpeed;
  }


  void nextRound(Vehicle vehicle, Network network) {
    currentVehicle = vehicle;
    currentNetwork = network;
    totalTime = 0;
    endTraining = false;
    endTime = 0;
    finalEnd = false;
    currentRound++;
  }



  void integrate(float frameTime) {
    totalTime += frameTime;

    if (!endTraining) {
      //println(vehicle.stats.totalDistanceSinceStart);
      //currentResults.totalDistance = vehicle.stats.totalDistanceSinceStart;
    }

    // check average speed
    float av = currentVehicle.stats.averageSpeed;
    if (!Float.isNaN(av)) {
      if (av<minimumAverageSpeed) {
        doEndTraining("Slower than average speed of "+minimumAverageSpeed);
      }
    }

  
  
    // check idle
    if (currentVehicle.stats.totalDistanceSinceStart<MINIMUM_DISTANCE_TO_TRAVEL && totalTime>idleTimeMaximum && !endTraining) {
      doEndTraining("Did not travel enougth ("+MINIMUM_DISTANCE_TO_TRAVEL+") within ("+idleTimeMaximum+") seconds.");
    }
    
    // raceGame.vehicle.dead
    else if (raceGame.vehicle.dead && !endTraining) {
      doEndTraining("Vehicle is dead");
    }

    // check end
    if (endTraining && totalTime > endTime+idleTimeEnd) {
      //println("Results: "+results.size()+" (TODO sort by totalDistance)");
      finalEnd = true;
    }
  }


  void doEndTraining(String reason) {
    if (!endTraining) {
      
      
      String identifier = "Candidate "+(results.size()+1);
      println("doEndTraining() for "+identifier);
      println("Reason: "+reason);
      
      NetworkWeights networkWeights = new NetworkWeights(currentNetwork);
      float totalDistance = currentVehicle.stats.totalDistanceSinceStart;
      TrainerResults result = new TrainerResults(identifier, reason, networkWeights, totalDistance, totalTime);
      //println(result.toSavableString());   
      
      results.add(result);
      endTraining = true;
      endTime = totalTime;
      
    }
  }


  void sortResults() {
    results.sort( new Comparator<TrainerResults>() {
      public int compare(TrainerResults r0, TrainerResults r1) {
        if (r0.totalDistance>r1.totalDistance) return -1;
        else if (r0.totalDistance<r1.totalDistance) return 1;
        else return 0;
      }
    } 
    );
  }

  void saveAllResults() {
    String NL = System.getProperty("line.separator");
    String dateTimeString = nf(year(), 4)+"_"+nf(month()+1, 2)+"_"+nf(day(), 2)+"_"+nf(hour(), 2)+"_"+nf(minute(), 2)+"_"+nf(second(), 2);
    for (int i=0; i<results.size(); i++) {
      TrainerResults result = results.get(i); 
      String ranking = nf(i, 3);
      String folderName = "Trainer results "+dateTimeString+"/";
      String fileName = ranking+" "+result.identifier+" totalDistance "+(int)result.totalDistance+".txt";
      String fullPath = folderName+fileName;
      println("Saving "+fullPath);
      saveStrings(fullPath, result.toSavableString().split(NL));
    }
  }
}




class TrainerResults {

  final String identifier;
  final String endReason;
  final NetworkWeights networkWeights;
  final float totalDistance;
  final float totalTime;

  TrainerResults(String identifier, String endReason, NetworkWeights networkWeights, float totalDistance, float totalTime) {
    this.identifier = identifier;
    this.endReason = endReason;
    this.networkWeights = networkWeights;
    this.totalDistance = totalDistance;
    this.totalTime = totalTime;
  }

  String toSavableString() {
    String NL = System.getProperty("line.separator");
    String PROPERTY_SEPARATOR = NL+"#"+NL;
    StringBuilder sb = new StringBuilder();
    
    sb.append("name");
    sb.append(NL);
    sb.append(identifier);
    sb.append(PROPERTY_SEPARATOR);
    
    sb.append("endReason");
    sb.append(NL);
    sb.append(endReason);
    sb.append(PROPERTY_SEPARATOR);

    sb.append("totalDistance");
    sb.append(NL);
    sb.append(totalDistance);
    sb.append(PROPERTY_SEPARATOR);
    
    sb.append("totalTime");
    sb.append(NL);
    sb.append(totalTime);
    sb.append(PROPERTY_SEPARATOR);

    sb.append("networkWeights");
    sb.append(NL);
    sb.append(networkWeights.toSavableString());
    return sb.toString();
  }

  //public static Comparator<TrainerResults> distanceComparator = new Comparator<TrainerResults>() {
  //  public int compare(TrainerResults r0, TrainerResults r1) {
  //    if (r0.totalDistance<r1.totalDistance) return -1;
  //    else if (r0.totalDistance>r1.totalDistance) return 1;
  //    else return 0;
  //  }
  //}
}
