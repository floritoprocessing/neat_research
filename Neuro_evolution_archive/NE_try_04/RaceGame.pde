import java.awt.geom.Line2D;

class RaceGame {

  RaceGamePlayfield field;
  Vehicle vehicle;


  RaceGame() {
    field = new RaceGamePlayfield();
    vehicle = new Vehicle(120, 200, 800, 6);
    restartSingleVehicle();
  }

  void restartSingleVehicle() {
    vehicle.dead = false;
    vehicle.position = field.getStartPosition();
    vehicle.initDirectionAndSpeed( field.getStartDirection(), 0, 0);
    vehicle.stats.reset();
  }

  void integrateSingleVehicle(float frameTime) {
    if (!vehicle.dead) {
      vehicle.checkTotalDistance(field);
      vehicle.integrate(field.leftLines, field.rightLines, frameTime);
    }
  }




  void draw() {
    field.draw();
    vehicle.draw();
  }
}





class VehicleStats {

  int AVERAGE_SPEED_DATA_POINTS = 60;

  float startDistance;
  float totalDistanceSinceStart;
  float noSpeedTotalTime;
  ArrayList<Float> speedMeasurements;
  float averageSpeed;

  void reset() {
    startDistance = Float.NaN;
    totalDistanceSinceStart=0;
    noSpeedTotalTime=0;
    averageSpeed = Float.NaN;
    speedMeasurements = new ArrayList<Float>();
  }

  void updateFirstStartDistance(float d) {
    if (Float.isNaN(startDistance)) {
      startDistance = d;
    }
  }
  void updateDistances(float totaldDistance) {
    totalDistanceSinceStart = (totaldDistance-startDistance);
  }

  void updateAverageSpeed(float currentSpeed) {
    speedMeasurements.add(currentSpeed);
    while (speedMeasurements.size()>AVERAGE_SPEED_DATA_POINTS) {
      speedMeasurements.remove(0);
    }
    if (speedMeasurements.size()==AVERAGE_SPEED_DATA_POINTS) {
      float total = 0;
      for (Float f : speedMeasurements) {
        total += f;
      }
      total /= AVERAGE_SPEED_DATA_POINTS;
      averageSpeed = total;
    }
  }
}








// VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE





class Vehicle {

  boolean DRAW_IMAGE = true;
  float size;
  float sizeToSensorFac = 5;
  float sizeToCentralSensorFac = 6;
  float sensorDegrees = 20;
  float turningSpeed; // degrees per second
  float thrustSpeed; // acceleration in pixels/sˆ2
  float breakSpeed; // break in pixels/sˆ2
  PVector position = new PVector();
  PVector velocity = new PVector();
  float direction;
  float speed;
  PVector[] points;
  boolean[] inSensorVecRange;
  PVector[] localSensorVecs;
  boolean dead;

  VehicleStats stats = new VehicleStats();

  boolean turningLeft=false, turningRight=false, increasingSpeed=false, decreasingSpeed=false;

  Vehicle(float turningSpeed, float thrustSpeed, float breakSpeed, float size) {
    this.turningSpeed = turningSpeed;
    this.thrustSpeed = thrustSpeed;
    this.breakSpeed = breakSpeed;
    this.size = size;
    initDirectionAndSpeed(radians(45), 0, 0);
    points = new PVector[] {  new PVector(-size, size), new PVector(0, -size*1.5), new PVector(size, size) };
    localSensorVecs = new PVector[3];
    inSensorVecRange = new boolean[3];
    for (int i=0; i<3; i++) {
      float rd = map(i, 0, 2, -radians(20), radians(20));
      float len = size*(i==1 ? sizeToCentralSensorFac : sizeToSensorFac);
      PVector sensorVec = new PVector(0, -len);
      sensorVec.rotate(rd);
      localSensorVecs[i] = sensorVec;
      inSensorVecRange[i] = false;
    }
  }

  void turnAndThrust(boolean left, boolean right, boolean thrust, boolean brek) {
    turningLeft=left;
    turningRight=right;
    increasingSpeed=thrust;
    decreasingSpeed=brek;
  }
  void turnLeft(boolean turn) {
    turningLeft = turn;
  }
  void turnRight(boolean turn) {
    turningRight = turn;
  }
  void increaseSpeed(boolean thrust) {
    increasingSpeed = thrust;
  }
  void decreaseSpeed(boolean doBreak) {
    decreasingSpeed = doBreak;
  }

  void initDirectionAndSpeed(float dir, float spd, float frameTime) {

    // from moving to speed=0 -> reset no speed total time
    if (this.speed!=0 && spd==0) {
      stats.noSpeedTotalTime = 0;
    }
    // from speeding to no speeding -> reset no speed total time
    else if (this.speed==0 && speed!=0) {
      stats.noSpeedTotalTime = 0;
    } else if (spd==0) {
      stats.noSpeedTotalTime += frameTime;
    }

    this.direction = dir;
    this.speed = spd;
    setVelocityFromSpeedAndDirection();
  }

  void setVelocityFromSpeedAndDirection() {
    velocity = new PVector(0, -1);
    velocity.rotate(this.direction);
    velocity.setMag(this.speed);
  }

  void checkTotalDistance(RaceGamePlayfield field) {
    // calc total distance & end game
    //float lastTotalDistance = stats.totalDistanceSinceStartl
    float totalDistance = calcTotalDistance(field);
    stats.updateFirstStartDistance(totalDistance);
    
    boolean illegalDistance = Float.isNaN(totalDistance);
    if (illegalDistance) dead=true;
    
    if (!dead) 
    {
      //println("Dead du
      stats.updateDistances(totalDistance);
    }
  }




  float calcTotalDistance(RaceGamePlayfield field) {
    PVector[] highestPointOnCentralLine = new PVector[] { new PVector() };
    int highestLineIndex = field.checkIfVehicleIsInsideTrack(this, highestPointOnCentralLine );
    if (highestLineIndex>=0) {

      PVector highestPoint = highestPointOnCentralLine[0];
      //point(highestPoint.x, highestPoint.y);

      float totalDistance = field.getCentralLineDistancesUntil(highestLineIndex);
      float perc = field.centralLines[highestLineIndex].getPercentageFromX(highestPoint.x);
      totalDistance += perc * field.centralLines[highestLineIndex].magnitude();

      return totalDistance;
    }

    return Float.NaN;
  }


  //PVector[] getLeftAndRightDirection() {
  //  PVector left = new PVector(0,-1);
  //  left.rotate(this.direction-radians(90));
  //  PVector right = new PVector(0,-1);
  //  right.rotate(this.direction+radians(90));
  //  return new PVector[] { left, right };
  //}


  void integrate(Line[] leftLines, Line[] rightLines, float seconds) { 
    applyTurningAndSpeed(seconds);
    checkSensors(leftLines, rightLines);
    stats.updateAverageSpeed(speed);
  }



  void applyTurningAndSpeed(float seconds) {
    if (turningLeft||turningRight||increasingSpeed||decreasingSpeed) {
      float deltaTurnRad = 0;
      float deltaSpeed = 0;
      if (turningLeft||turningRight) {
        float deltaTurnDeg = ((turningRight?1:0)+(turningLeft?-1:0)) * turningSpeed * seconds;
        deltaTurnRad = radians(deltaTurnDeg);
      }
      if (increasingSpeed||decreasingSpeed) {
        deltaSpeed = ((increasingSpeed?thrustSpeed:0)-(decreasingSpeed?breakSpeed:0)) * seconds;
      }
      float newSpeed = max(0, speed+deltaSpeed);
      initDirectionAndSpeed( direction+deltaTurnRad, newSpeed, seconds);
    }

    position = PVector.add( position, PVector.mult( velocity, seconds ) );
  }




  void checkSensors(Line[] leftLines, Line[] rightLines) {
    Line[][] LR = new Line[][] { leftLines, rightLines };
    for (int sensor=0; sensor<3; sensor++) {

      //local to global:
      PVector sensorPoint = localSensorVecs[sensor].copy();
      sensorPoint.rotate(direction);
      sensorPoint.add( position );
      Line sensorLine = new Line( position, sensorPoint );

      boolean intersect = false;
    outer:
      for (int l=0; l<2; l++) {
        Line[] lines = LR[l];
        for (int i=0; i<lines.length; i++) {
          if (linesIntersect(lines[i], sensorLine)) {
            intersect=true;
            break outer;
          }
        }
      }
      inSensorVecRange[sensor] = intersect;

      //strokeWeight(5);
      //point(sensorPoint.x, sensorPoint.y);
    }
  }



  boolean linesIntersect(Line l0, Line l1) {
    return linesIntersect(l0.p0, l0.p1, l1.p0, l1.p1);
  }

  boolean linesIntersect(PVector p0, PVector p1, PVector p2, PVector p3) {
    return Line2D.Float.linesIntersect(p0.x, p0.y, p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
  }





  void draw() {
    strokeWeight(1);
    pushMatrix();

    translate(position.x, position.y);

    if (!DRAW_IMAGE) {
      for (int i=0; i<localSensorVecs.length; i++) {
        PVector globalSensorVec = localSensorVecs[i].copy();
        globalSensorVec.rotate(direction);
        stroke(inSensorVecRange[i] ? color(255, 0, 0) : color(0, 128, 0));
        line(0, 0, globalSensorVec.x, globalSensorVec.y);
      }
    }

    rotate(direction);

    if (!DRAW_IMAGE) {
      stroke(0);
      fill(255);
      beginShape(TRIANGLES);
      for (int i=0; i<points.length; i++) {
        vertex(points[i].x, points[i].y);
      }
      endShape();
    } else {
      float aspRat = (float)img.width/(float)img.height;
      float w = size*10;
      float h = size*10/aspRat;
      rotate(-HALF_PI);
      scale(-1, 1);
      image(img, -w/2, -h/2, w, h);
    }



    popMatrix();
  }
}











/*

 
 FIELD
 
 
 */


class RaceGamePlayfield {

  boolean makePlayfield = false;
  Line[] centralLines;
  Line[] leftLines, rightLines;
  float roadWidth = 20;

  float[] FIELD1 = new float[] {
    88, 96, 122, 76, 206, 69, 429, 111, 478, 253, 467, 416, 413, 469, 381, 452, 378, 394, 395, 337, 
    392, 278, 381, 220, 362, 185, 328, 179, 293, 200, 302, 321, 304, 421, 289, 449, 254, 459, 152, 461, 
    92, 449, 81, 361, 100, 331, 165, 301, 182, 271, 146, 214, 125, 201, 77, 178, 72, 128};

  float[] FIELD2_ZIG_ZAG = new float[] {
    106, 117, 141, 119, 187, 135, 245, 128, 288, 88, 336, 93, 357, 134, 391, 158, 430, 138, 471, 167, 
    485, 220, 466, 276, 422, 297, 381, 288, 379, 237, 323, 223, 292, 288, 308, 340, 329, 352, 339, 402, 
    315, 451, 219, 467, 145, 466, 132, 438, 163, 393, 226, 376, 233, 325, 195, 293, 85, 303, 67, 282, 
    53, 195, 86, 138};

  RaceGamePlayfield() {
    createLines(FIELD2_ZIG_ZAG);
  }

  void initMakePlayfield() {
    makePlayfield = true;
    centralLines = new Line[0];
  }

  void addPoint(float x, float y) {
    if (centralLines.length==0) {
      createLines(x, y);
    } else {
      ArrayList<PVector> points = new ArrayList<PVector>();
      for (int i=0; i<centralLines.length; i++) {
        points.add(centralLines[i].p0);
      }
      points.add(centralLines[centralLines.length-1].p1);
      makeLines(points);
    }
  }

  void createLines(float... xyPairs) {
    ArrayList<PVector> points = new ArrayList<PVector>();
    for (int i=0; i<xyPairs.length; i+=2) {
      points.add( new PVector(xyPairs[i], xyPairs[i+1]) );
    }
    makeLines(points);
  }

  void finalize() {
    ArrayList<PVector> points = new ArrayList<PVector>();
    for (int i=0; i<centralLines.length; i++) {
      points.add(centralLines[i].p0);
    }
    print("float[] xyPoints = new float[] {");
    for (int i=0; i<points.size(); i++) {
      if (i%10==0) {
        println();
      }
      print("   "+int(points.get(i).x)+", "+int(points.get(i).y));
      if (i<points.size()-1) print(",");
    }
    print("};");
    makeLines(points);
    makePlayfield = false;
  }

  void makeLines(ArrayList<PVector> points) {
    centralLines = new Line[points.size()];
    for (int i=0; i<points.size(); i++) {
      PVector p0 = points.get(i);
      PVector p1 = points.get( ((i+1)<points.size()?(i+1):0) );
      centralLines[i] = new Line(p0, p1);
    }
    makeSideLines(centralLines, roadWidth);
  }


  void makeSideLines(Line[] lines, float wid) {

    leftLines = new Line[lines.length];
    rightLines = new Line[lines.length];
    for (int i=0; i<lines.length; i++) {
      Line line = lines[i];
      PVector dir = PVector.sub(line.p1, line.p0);
      PVector dirLeft = dir.copy();
      dirLeft.rotate(radians(-90));
      dirLeft.setMag( wid );
      leftLines[i] = new Line( PVector.add(line.p0, dirLeft), PVector.add(line.p1, dirLeft) );
      PVector dirRight = dir.copy();
      dirRight.rotate(radians(90));
      dirRight.setMag( wid );
      rightLines[i] = new Line( PVector.add(line.p0, dirRight), PVector.add(line.p1, dirRight) );
    }
    // now adjust all left and right lines for intersections
    Line[][] leftAndRight = new Line[][] { leftLines, rightLines };
    for (Line[] sideLines : leftAndRight) {
      //Line[] sideLines = leftLines;
      for (int i=0; i<sideLines.length; i++) {
        Line l0 = sideLines[i];
        Line l1 = sideLines[i<lines.length-1 ? i+1 : 0];
        PVector intersectionPoint = lineLineIntersection(l0.p0, l0.p1, l1.p0, l1.p1);
        if (intersectionPoint!=null) {
          l0.p1 = intersectionPoint;
          l1.p0 = intersectionPoint;
        }
      }
    }
  }

  PVector lineLineIntersection(PVector A, PVector B, PVector C, PVector D) 
  { 
    // Line AB represented as a1x + b1y = c1 
    float a1 = B.y - A.y; 
    float b1 = A.x - B.x; 
    float c1 = a1*(A.x) + b1*(A.y); 

    // Line CD represented as a2x + b2y = c2 
    float a2 = D.y - C.y; 
    float b2 = C.x - D.x; 
    float c2 = a2*(C.x)+ b2*(C.y); 

    float determinant = a1*b2 - a2*b1; 

    if (determinant == 0) 
    { 
      // The lines are parallel. This is simplified 
      // by returning a pair of FLT_MAX 
      return null;//new PVector(Double.MAX_VALUE, Double.MAX_VALUE);
    } else
    { 
      float x = (b2*c1 - b1*c2)/determinant; 
      float y = (a1*c2 - a2*c1)/determinant; 
      return new PVector(x, y);
    }
  } 



  PVector getStartPosition() {
    return PVector.mult( PVector.add( leftLines[1].p0, rightLines[1].p0 ), 0.5 );
  }

  float getStartDirection() {
    return PVector.angleBetween(PVector.sub( centralLines[1].p1, centralLines[1].p0 ), new PVector(0, -1));
  }






  /**
   intersectionPointsOnCentralLine is a returning array
   @returns the highest track piece index
   */
  int checkIfVehicleIsInsideTrack(Vehicle vehicle, PVector[] intersectionPointsOnCentralLine ) {
    //PVector[] LR = vehicle.getLeftAndRightDirection();

    int highestLineIndex = -1;
    PVector highestIntersectionPointOnCentralLine = null;
    boolean isInsideTrack = false;

    for (int i=0; i<centralLines.length; i++) {

      Line[] leftRightLine = new Line[] { leftLines[i], rightLines[i], centralLines[i] };
      PVector[] intersectionPoints = new PVector[3];
      float[] positionsOnSides = new float[3];

      for (int side=0; side<3; side++) {
        Line line = leftRightLine[ side ];
        PVector lookToSide = new PVector(1, 0);
        lookToSide.rotate(line.heading() + (side==0?-HALF_PI:HALF_PI) );
        PVector pointLookingToSide = PVector.add(vehicle.position, lookToSide);
        intersectionPoints[side] = lineLineIntersection(vehicle.position, pointLookingToSide, line.p0, line.p1);
        positionsOnSides[side] = line.getPercentageFromX(intersectionPoints[side].x);
      }

      Line lineFromSideToSide = new Line(intersectionPoints[0], intersectionPoints[1]);
      float percentageOnLineFromSideToSide = lineFromSideToSide.getPercentageFromX(vehicle.position.x);

      //if (percentageOnLineFromSideToSide>=0&&percentageOnLineFromSideToSide<=1) {
      if ( (positionsOnSides[0]>=0&&positionsOnSides[0]<=1) || (positionsOnSides[1]>=0&&positionsOnSides[1]<=1) ) {

        if ((percentageOnLineFromSideToSide>=0&&percentageOnLineFromSideToSide<=1)) {
          isInsideTrack = true;
          if (i>highestLineIndex) {
            highestLineIndex = i;
            highestIntersectionPointOnCentralLine = intersectionPoints[2];
          }

          //if (draw) {
          //  strokeWeight(5);
          //  stroke(0, 128, 0);
          //  point(intersectionPoints[0].x, intersectionPoints[0].y);
          //  stroke(255, 0, 0);
          //  point(intersectionPoints[1].x, intersectionPoints[1].y);
          //  stroke(0, 0, 255);
          //  point(intersectionPoints[2].x, intersectionPoints[2].y);
          //  strokeWeight(1);
          //  stroke(0, 0, 255);
          //  lineFromSideToSide.draw();
          //}
        }
      }
    }

    intersectionPointsOnCentralLine[0] = highestIntersectionPointOnCentralLine;
    return highestLineIndex;
  }



  float getCentralLineDistancesUntil(int index) {
    float out = 0;
    for (int i=0; i<index; i++) {
      out += centralLines[i].magnitude();
    }
    return out;
  }




  void draw() {
    strokeWeight(1);
    if (makePlayfield) {

      // draw cursor
      stroke(0);
      line(mouseX, mouseY-20, mouseX, mouseY-5);
      line(mouseX, mouseY+20, mouseX, mouseY+5);
      line(mouseX-20, mouseY, mouseX-5, mouseY);
      line(mouseX+20, mouseY, mouseX+5, mouseY);

      //move last point
      if (centralLines.length>0) {
        centralLines[centralLines.length-1].p1 = new PVector(mouseX, mouseY);
      }
      //if (centralLines.length
    }

    stroke(128);
    for (int i=0; i<centralLines.length; i++) {
      centralLines[i].draw();
    }
    stroke(0);
    for (int i=0; i<centralLines.length; i++) {
      leftLines[i].draw();
      rightLines[i].draw();
    }
    stroke(64);
    line(leftLines[0].p0.x, leftLines[0].p0.y, rightLines[0].p0.x, rightLines[0].p0.y);
  }
}





class Line {
  PVector p0, p1;
  Line(PVector p0, PVector p1) {
    this.p0=p0;
    this.p1=p1;
  }
  float heading() {
    return PVector.sub(p1, p0).heading();
  }
  float getPercentageFromX(float x) {
    return (x-p0.x)/(p1.x-p0.x);
  }
  float magnitude() {
    return PVector.sub(p1, p0).mag();
  }

  void draw() {
    line(p0.x, p0.y, p1.x, p1.y);
  }
}



/*
List<Point> getCircleLineIntersectionPoint(Point pointA,
 Point pointB, Point center, double radius) {
 double baX = pointB.x - pointA.x;
 double baY = pointB.y - pointA.y;
 double caX = center.x - pointA.x;
 double caY = center.y - pointA.y;
 
 double a = baX * baX + baY * baY;
 double bBy2 = baX * caX + baY * caY;
 double c = caX * caX + caY * caY - radius * radius;
 
 double pBy2 = bBy2 / a;
 double q = c / a;
 
 double disc = pBy2 * pBy2 - q;
 if (disc < 0) {
 return Collections.emptyList();
 }
 // if disc == 0 ... dealt with later
 double tmpSqrt = Math.sqrt(disc);
 double abScalingFactor1 = -pBy2 + tmpSqrt;
 double abScalingFactor2 = -pBy2 - tmpSqrt;
 
 Point p1 = new Point(pointA.x - baX * abScalingFactor1, pointA.y
 - baY * abScalingFactor1);
 if (disc == 0) { // abScalingFactor1 == abScalingFactor2
 return Collections.singletonList(p1);
 }
 Point p2 = new Point(pointA.x - baX * abScalingFactor2, pointA.y
 - baY * abScalingFactor2);
 return Arrays.asList(p1, p2);
 }
 */
