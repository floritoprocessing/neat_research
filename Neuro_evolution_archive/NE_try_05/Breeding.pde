//int MAX_GENERATION_AGE = 3;
int INITIAL_GENERATION_SIZE = 5;
int MAX_GENERATION_SIZE = 10;

float BREEDING_RATE_MIN = 1.5;


int BEST_CARRY_TO_NEW_GENERATION = 3;
int LEAST_CARRY_TO_NEW_GENERATION = 0;

float MUTATION_CHANCE_PER_WEIGHT = 0.1; // 0.05
float MUTATION_RANGE = 1.0; // 0.05 0.2

float DISTRIBUTION_DISTANCE_BIAS_PERCENTAGE = 0.1;


class Generation {


  TrainerResults[] oldGeneration;
  ArrayList<NetworkWeights> newGeneration;
  ArrayList<TrainerResultsRange> distributionStackTotalDistance;

  // relies on the fact that the results are sorted by fittest first
  Generation(TrainerResults[] oldGeneration) {
    this.oldGeneration = oldGeneration;
  }

  void breedNew() {

    newGeneration = new ArrayList<NetworkWeights>();

    carryOverPreviousGeneration(BEST_CARRY_TO_NEW_GENERATION, LEAST_CARRY_TO_NEW_GENERATION);

    makeDistributionStackTotalDistance(DISTRIBUTION_DISTANCE_BIAS_PERCENTAGE);

    int leastBreedingTargetSize = oldGeneration.length;
    int maxBreedingTargetSize = (int)(BREEDING_RATE_MIN*oldGeneration.length);
    int targetPairs = max(leastBreedingTargetSize, min(maxBreedingTargetSize, MAX_GENERATION_SIZE));
    int deltaPairs = targetPairs - newGeneration.size();
    println("BREED NEW: target="+targetPairs+", adding "+deltaPairs);
    
    for (int pairIndex=0; pairIndex<deltaPairs; pairIndex++) {

      // find parents
      TrainerResultsRange parent0Data = chooseParent( distributionStackTotalDistance, null );
      TrainerResultsRange parent1Data = chooseParent( distributionStackTotalDistance, parent0Data );
      TrainerResults parent0 = parent0Data.result;
      TrainerResults parent1 = parent1Data.result;
      println("Parents: "+parent0.identifier+" with "+parent1.identifier);

      // make DNA
      // turn NetworkWeights into a flat array
      float[] parent0SortedWeights = parent0.networkWeights.sortedWeightsToFloatArray();
      float[] parent1SortedWeights = parent1.networkWeights.sortedWeightsToFloatArray();
      float[] childWeights = crossOverAndMutate(parent0SortedWeights, parent1SortedWeights);
      NetworkWeights child = parent0.networkWeights.createNewFromSortedWeights(childWeights);

      newGeneration.add( child );
    }
  }




  /*
  * increase the ages of the current generation
   * & carry over if not too old
   */
  void carryOverPreviousGeneration(int bestCount, int leastCount) {

    // add BEST
    bestCount = min(bestCount, oldGeneration.length);
    for (int i=0; i<bestCount; i++) {
      newGeneration.add(oldGeneration[i].networkWeights);
    }
    
    // add Works
    leastCount = min(leastCount, oldGeneration.length);
    for (int i=0; i<leastCount; i++) {
      int index = oldGeneration.length-i-1;
      newGeneration.add(oldGeneration[index].networkWeights);
    }
  }


  /*
  *
   *  use total distance to make a distributionStack
   *  using totalDistance linearly as fitness
   *
   */
  void makeDistributionStackTotalDistance(float biasPercentage) {

    float maxTotalDistance = 0;
    for (TrainerResults result : oldGeneration) {
      maxTotalDistance = max(maxTotalDistance, result.totalDistance);
    }
    float bias = maxTotalDistance*biasPercentage;
    
    distributionStackTotalDistance = new ArrayList<TrainerResultsRange>();

    float min = 0;
    float max = 0;

    for (TrainerResults result : oldGeneration) {
      max += result.totalDistance+bias;
      distributionStackTotalDistance.add( new TrainerResultsRange(result, min, max) );
      min += result.totalDistance+bias;
    }
  }



  /*
  *
   *  Chooses a parent from the distribution stack
   *  if stack total = 0, choose randomly
   *
   */
  TrainerResultsRange chooseParent(ArrayList<TrainerResultsRange> distributionStack, TrainerResultsRange otherParent) {
    int stackSize = distributionStack.size();
    float max = distributionStack.get( stackSize-1 ).max;
    if (max==0 || (otherParent!=null && otherParent.min==0 && otherParent.max==max)) {

      return distributionStack.get( (int)random(stackSize) );
    } else {

      TrainerResultsRange out = otherParent;

      while ((otherParent==null&&out==null) || (out!=null&&otherParent!=null&&out.result == otherParent.result)) {

        float rnd = random(max);

        for (TrainerResultsRange range : distributionStack) {
          if (rnd>=range.min && rnd<range.max) {
            out = range;
          }
        }
      }
      return out;

      //throw new RuntimeException("Meh, something is wrong from the stack. rnd="+rnd);
    }
  }



  /*
  *
   *  CROSSOVER AND MUTATE
   *
   */

  float[] crossOverAndMutate(float[] parent0SortedWeights, float[] parent1SortedWeights) {


    int dnaLength = parent0SortedWeights.length;
    int splitPoint = (int)random(dnaLength+1);
    float[] childDna = new float[dnaLength];

    for (int genIndex=0; genIndex<dnaLength; genIndex++) {

      // crossover
      float childWeight = genIndex<splitPoint ? parent0SortedWeights[genIndex] : parent1SortedWeights[genIndex];

      // mutation
      if (random(1.0)<MUTATION_CHANCE_PER_WEIGHT) {
        childWeight += random(-MUTATION_RANGE/2, MUTATION_RANGE/2);
      }

      childDna[genIndex] = childWeight;
    }

    return childDna;
  }
}








class TrainerResultsRange {
  TrainerResults result;
  float min, max;
  TrainerResultsRange(TrainerResults result, float min, float max) {
    this.result = result;
    this.min=min;
    this.max=max;
    println(this);
  }
  String toString() {
    return result.identifier+"("+nf(max-min, 1, 2)+"): "+min+".."+max;
  }
}
