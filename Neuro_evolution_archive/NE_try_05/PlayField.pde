


/*

 
 FIELD
 
 
 */

import java.util.Arrays;


class PlayField {

  boolean makePlayfield = false;
  //Line[] centralLines;
  //Line[] leftLines, rightLines;
  final float roadWidth = 20;

  TrackSegment[] trackSegments;

  float[] FIELD1 = new float[] {
    88, 96, 122, 76, 206, 69, 429, 111, 478, 253, 467, 416, 413, 469, 381, 452, 378, 394, 395, 337, 
    392, 278, 381, 220, 362, 185, 328, 179, 293, 200, 302, 321, 304, 421, 289, 449, 254, 459, 152, 461, 
    92, 449, 81, 361, 100, 331, 165, 301, 182, 271, 146, 214, 125, 201, 77, 178, 72, 128};

  float[] FIELD2_ZIG_ZAG = new float[] {
    106, 117, 141, 119, 187, 135, 245, 128, 288, 88, 336, 93, 357, 134, 391, 158, 430, 138, 471, 167, 
    485, 220, 466, 276, 422, 297, 381, 288, 379, 237, 323, 223, 292, 288, 308, 340, 329, 352, 339, 402, 
    315, 451, 219, 467, 145, 466, 132, 438, 163, 393, 226, 376, 233, 325, 195, 293, 85, 303, 67, 282, 
    53, 195, 86, 138};

  PlayField() {
    createLines(FIELD2_ZIG_ZAG);
  }

  void initMakePlayfield() {
    makePlayfield = true;
    trackSegments = new TrackSegment[0];
  }



  void addPoint(float x, float y) {
    if (trackSegments.length==0) {
      createLines(x, y);
    } else {
      ArrayList<PVector> points = new ArrayList<PVector>();
      for (int i=0; i<trackSegments.length; i++) {
        points.add(trackSegments[i].centralLine.getP0());
      }
      points.add(trackSegments[trackSegments.length-1].centralLine.getP1());
      makeLines(points);
    }
  }



  void createLines(float... xyPairs) {
    ArrayList<PVector> points = new ArrayList<PVector>();
    for (int i=0; i<xyPairs.length; i+=2) {
      points.add( new PVector(xyPairs[i], xyPairs[i+1]) );
    }
    makeLines(points);
  }



  void finalize() {
    ArrayList<PVector> points = new ArrayList<PVector>();
    for (int i=0; i<trackSegments.length; i++) {
      points.add(trackSegments[i].centralLine.getP0());
    }
    print("float[] xyPoints = new float[] {");
    for (int i=0; i<points.size(); i++) {
      if (i%10==0) {
        println();
      }
      print("   "+int(points.get(i).x)+", "+int(points.get(i).y));
      if (i<points.size()-1) print(",");
    }
    print("};");
    makeLines(points);
    makePlayfield = false;
  }



  void makeLines(ArrayList<PVector> points) {
    trackSegments = new TrackSegment[points.size()];
    for (int i=0; i<points.size(); i++) {
      trackSegments[i] = new TrackSegment();
      PVector p0 = points.get(i);
      PVector p1 = points.get( ((i+1)<points.size()?(i+1):0) );
      trackSegments[i].centralLine = new Line(p0, p1);
    }
    makeSideLines(trackSegments, roadWidth);
  }



  void makeSideLines(TrackSegment[] trackSegments, float wid) {


    for (int i=0; i<trackSegments.length; i++) {
      Line line = trackSegments[i].centralLine;
      PVector dir = PVector.sub(line.getP1(), line.getP0());
      PVector dirLeft = dir.copy();
      dirLeft.rotate(radians(-90));
      dirLeft.setMag( wid );
      trackSegments[i].leftLine = new Line( PVector.add(line.getP0(), dirLeft), PVector.add(line.getP1(), dirLeft) );
      PVector dirRight = dir.copy();
      dirRight.rotate(radians(90));
      dirRight.setMag( wid );
      trackSegments[i].rightLine = new Line( PVector.add(line.getP0(), dirRight), PVector.add(line.getP1(), dirRight) );
    }

    // now adjust all left and right lines for intersections
    //Line[][] leftAndRight = new Line[][] { leftLines, rightLines };
    //for (Line[] sideLines : leftAndRight) {
    //for (TrackSegment segment:trackSegments) {
    for (int left=0; left<2; left++) {

      for (int i=0; i<trackSegments.length; i++) {

        int index0 = i;
        int index1 = i<trackSegments.length-1 ? i+1 : 0;

        Line l0 = left==0 ? trackSegments[index0].leftLine : trackSegments[index0].rightLine;
        Line l1 = left==0 ? trackSegments[index1].leftLine : trackSegments[index1].rightLine;

        PVector intersectionPoint = lineLineIntersection(l0.getP0(), l0.getP1(), l1.getP0(), l1.getP1());
        if (intersectionPoint!=null) {
          l0.setP1( intersectionPoint );
          l1.setP0( intersectionPoint );
        }
      }
    }
  }



  float getCourseDistance(Vehicle vehicle) {

    PVector P = vehicle.position;
    float totalDistance = 0;

    for (int i=0; i<trackSegments.length; i++) {
      TrackSegment segment = trackSegments[i];
     
      // in Segment AB CD, find line GH that is parallel to AB and CD that goes through P
      // then find percentage of P on GH, meaning ratio GP:GH
      PVector A = segment.rightLine.getP0();
      PVector B = segment.rightLine.getP1();
      PVector C = segment.leftLine.getP0();
      PVector D = segment.leftLine.getP1();


      if (B.x-A.x==0) {
        System.err.println("TODO: infinite inclination, use reverse?");
      } else {
        float inclinationAB = (B.y-A.y)/(B.x-A.x);
        // GH: line with same inclination as AB that goes through the point P
        // GH: y = (x-P.x) * inclinationAB + P.y
        PVector GH0 = new PVector(0, (0-P.x)*inclinationAB + P.y);
        PVector GH1 = new PVector(width, (width-P.x)*inclinationAB + P.y);
        
        // find G: intersection AC with GH
        // find H: intersection BD with GH
        boolean hasG = lineSegmentsIntersect(A, C, GH0, GH1);
        boolean hasH = lineSegmentsIntersect(B, D, GH0, GH1);
        if (hasG && hasH) {
          PVector G = lineLineIntersection(A, C, GH0, GH1);
          PVector H = lineLineIntersection(B, D, GH0, GH1);
          // now find percentage of P on line GH
          Line GH = new Line(G,H);
          float segmentPercentage = GH.calcPercentageFromX(P.x);
          if (segmentPercentage>=0 && segmentPercentage<1) {
            totalDistance += segment.centralLine.getMagnitude() * segmentPercentage;
            return totalDistance;
          }
        }
      }
      totalDistance += segment.centralLine.getMagnitude();
    } // end for track segment loop


    return Float.NaN;
  }



  float getCourseDistance0(Vehicle vehicle) {

    ArrayList<Float> percentages = new ArrayList<Float>();
    ArrayList<TrackSegment> segments = findTrackSegments(vehicle, percentages);

    if (segments.size()==0) {
      throw new RuntimeException("Meh, no segment found");
    }


    println(percentages.size()+" results!");
    for (int resultIndex=0; resultIndex<percentages.size(); resultIndex++) {
      float totalDistance = 0;
      float highestSegmentPercentage = percentages.get(resultIndex);
      TrackSegment resultSegment = segments.get(resultIndex); 
      for (int i=0; i<trackSegments.length; i++) {
        TrackSegment thisSegment = trackSegments[i];
        if (thisSegment!=resultSegment) {
          totalDistance += resultSegment.centralLine.getMagnitude();
        } else {
          break;
        }
      }
      totalDistance += highestSegmentPercentage * resultSegment.centralLine.getMagnitude();
      println(resultIndex+": "+totalDistance);
    }

    return 0;
  }




  /**
   * Finds the TrackSegment that contains the position. Returns null for no position
   */
  ArrayList<TrackSegment> findTrackSegments(Vehicle vehicle, ArrayList<Float> percentages) {
    // for each track segment

    println("findTrackSegment for vehicle at "+vehicle.position+" with speed "+vehicle.velocity);
    ArrayList<TrackSegment> foundSegments = new ArrayList<TrackSegment>();

    for (int i=0; i<trackSegments.length; i++) {
      TrackSegment segment = trackSegments[i];
      // get intersection between 
      // line that is parallel to the track normal and goes through the vehicle and is longer than the track is wide 
      // and the track center line
      Line centralLine = segment.centralLine;
      Line leftLine = segment.leftLine;
      Line rightLine = segment.rightLine;
      PVector longNormal = centralLine.getNormal().copy();
      longNormal.mult(2*roadWidth);
      PVector p0 = PVector.add(vehicle.position, longNormal);
      PVector p1 = PVector.sub(vehicle.position, longNormal);
      PVector left0 = leftLine.getP0();
      PVector left1 = leftLine.getP1();
      PVector right0 = rightLine.getP0();
      PVector right1 = rightLine.getP1();
      boolean leftIntersects = lineSegmentsIntersect(p0, p1, left0, left1);
      boolean rightIntersects = lineSegmentsIntersect(p0, p1, right0, right1);
      boolean doSegmentsIntersect = leftIntersects || rightIntersects;

      // last check: check if normal intersection line is on either left or right side
      if (doSegmentsIntersect) {
        PVector leftLineIntersectionPoint = lineLineIntersection(p0, p1, left0, left1);
        PVector rightLineIntersectionPoint = lineLineIntersection(p0, p1, right0, right1);
        if (leftLineIntersectionPoint!=null && rightLineIntersectionPoint!=null) {
          float leftPerc = leftLine.calcPercentageFromX(leftLineIntersectionPoint.x);
          float rightPerc = rightLine.calcPercentageFromX(rightLineIntersectionPoint.x);
          if ( (leftPerc>=0&&leftPerc<1) || (rightPerc>=0&&rightPerc<1) ) {
            float perc = (leftPerc+rightPerc)/2;

            segment.debugDrawFlag = true;
            percentages.add(perc);
            foundSegments.add(segment);
            //return segment;
            //}
          }
        }

        //println("trackSegment "+i+" intersects: "+doSegmentsIntersect);
      }
    }


    return foundSegments;
  }





  PVector lineLineIntersection(PVector A, PVector B, PVector C, PVector D) 
  { 
    // Line AB represented as a1x + b1y = c1 
    float a1 = B.y - A.y; 
    float b1 = A.x - B.x; 
    float c1 = a1*(A.x) + b1*(A.y); 

    // Line CD represented as a2x + b2y = c2 
    float a2 = D.y - C.y; 
    float b2 = C.x - D.x; 
    float c2 = a2*(C.x)+ b2*(C.y); 

    float determinant = a1*b2 - a2*b1; 

    if (determinant == 0) 
    { 
      // The lines are parallel. This is simplified 
      // by returning a pair of FLT_MAX 
      return null;//new PVector(Double.MAX_VALUE, Double.MAX_VALUE);
    } else
    { 
      float x = (b2*c1 - b1*c2)/determinant; 
      float y = (a1*c2 - a2*c1)/determinant; 
      return new PVector(x, y);
    }
  } 



  PVector getStartPosition() {
    return PVector.mult( PVector.add( trackSegments[1].leftLine.getP0(), trackSegments[1].rightLine.getP0() ), 0.5 );
  }


  float getStartDirection() {
    return PVector.angleBetween(PVector.sub( trackSegments[1].centralLine.getP1(), trackSegments[1].centralLine.getP0() ), new PVector(0, -1));
  }





  boolean checkBoundaryCrossing(PVector p0, PVector p1) {
    Line movement = new Line(p0, p1);
    for (TrackSegment segment : trackSegments)
      if (lineSegmentsIntersect(movement, segment.leftLine) || lineSegmentsIntersect(movement, segment.rightLine))
        return true;
    return false;
  }




  /**
   intersectionPointsOnCentralLine is a returning array
   @returns the highest track piece index
   */


  //int checkIfVehicleIsInsideTrack(Vehicle vehicle, PVector[] intersectionPointsOnCentralLine ) {
  //  //PVector[] LR = vehicle.getLeftAndRightDirection();

  //  int highestLineIndex = -1;
  //  PVector highestIntersectionPointOnCentralLine = null;
  //  boolean isInsideTrack = false;

  //  for (int i=0; i<trackSegments.length; i++) {

  //    Line[] leftRightLine = new Line[] { trackSegments[i].leftLine, trackSegments[i].rightLine, trackSegments[i].centralLine };
  //    PVector[] intersectionPoints = new PVector[3];
  //    float[] positionsOnSides = new float[3];

  //    for (int side=0; side<3; side++) {
  //      Line line = leftRightLine[ side ];
  //      PVector lookToSide = new PVector(1, 0);
  //      lookToSide.rotate(line.heading() + (side==0?-HALF_PI:HALF_PI) );
  //      PVector pointLookingToSide = PVector.add(vehicle.position, lookToSide);
  //      intersectionPoints[side] = lineLineIntersection(vehicle.position, pointLookingToSide, line.p0, line.p1);
  //      positionsOnSides[side] = line.getPercentageFromX(intersectionPoints[side].x);
  //    }

  //    Line lineFromSideToSide = new Line(intersectionPoints[0], intersectionPoints[1]);
  //    float percentageOnLineFromSideToSide = lineFromSideToSide.getPercentageFromX(vehicle.position.x);

  //    //if (percentageOnLineFromSideToSide>=0&&percentageOnLineFromSideToSide<=1) {
  //    if ( (positionsOnSides[0]>=0&&positionsOnSides[0]<=1) || (positionsOnSides[1]>=0&&positionsOnSides[1]<=1) ) {

  //      if ((percentageOnLineFromSideToSide>=0&&percentageOnLineFromSideToSide<=1)) {
  //        isInsideTrack = true;
  //        if (i>highestLineIndex) {
  //          highestLineIndex = i;
  //          highestIntersectionPointOnCentralLine = intersectionPoints[2];
  //        }
  //      }
  //    }
  //  }

  //  intersectionPointsOnCentralLine[0] = highestIntersectionPointOnCentralLine;
  //  return highestLineIndex;
  //}



  //float getCentralLineDistancesUntil(int index) {
  //  float out = 0;
  //  for (int i=0; i<index; i++) {
  //    out += trackSegments[i].centralLine.magnitude();
  //  }
  //  return out;
  //}




  void draw() {
    strokeWeight(1);
    if (makePlayfield) {

      // draw cursor
      stroke(0);
      line(mouseX, mouseY-20, mouseX, mouseY-5);
      line(mouseX, mouseY+20, mouseX, mouseY+5);
      line(mouseX-20, mouseY, mouseX-5, mouseY);
      line(mouseX+20, mouseY, mouseX+5, mouseY);

      //move last point
      if (trackSegments.length>0) {
        trackSegments[trackSegments.length-1].centralLine.setP1( new PVector(mouseX, mouseY) );
      }
      //if (centralLines.length
    }

    for (int i=0; i<trackSegments.length; i++) {
      trackSegments[i].draw();
    }

    stroke(64);
    line(trackSegments[0].leftLine.getP0().x, trackSegments[0].leftLine.getP0().y, 
      trackSegments[0].rightLine.getP0().x, trackSegments[0].rightLine.getP0().y);
  }
}





class Line {

  private final PVector _p0, _p1;
  private PVector _normal;
  private float _heading;
  private float _magnitude;

  Line(PVector p0, PVector p1) {
    this._p0=p0;
    this._p1=p1;
    calcHeadingAndMagnitude();
  }

  private void calcHeadingAndMagnitude() {
    PVector dir = PVector.sub(_p1, _p0);
    _heading = dir.heading();
    _magnitude = dir.mag();
    _normal = dir.copy();
    _normal.rotate(HALF_PI);
    _normal.normalize();
  }

  void setP0(PVector p) {
    _p0.set(p);
    calcHeadingAndMagnitude();
  }

  void setP1(PVector p) {
    _p1.set(p);
    calcHeadingAndMagnitude();
  }

  PVector getP0() {
    return _p0;
  }
  PVector getP1() {
    return _p1;
  }
  PVector getNormal() {
    return _normal;
  }

  float getHeading() {
    return _heading;
  }
  float getMagnitude() {
    return _magnitude;
  }

  float calcPercentageFromX(float x) {
    return (x-_p0.x)/(_p1.x-_p0.x);
  }


  void draw() {
    line(_p0.x, _p0.y, _p1.x, _p1.y);
  }


  String toString() {
    return "Line ("+_p0.toString()+", "+_p1.toString()+")";
  }
}






class TrackSegment {

  boolean debugDrawFlag = false;
  Line leftLine, centralLine, rightLine;

  TrackSegment() {
  }

  float getLength() {
    return centralLine.getMagnitude();
  }

  void draw() {

    if (debugDrawFlag) {
      noStroke();
      fill(255, 128, 128);
      beginShape(QUADS);
      PVector p0 = leftLine.getP0();
      PVector p1 = leftLine.getP1();
      PVector p2 = rightLine.getP1();
      PVector p3 = rightLine.getP0();
      vertex(p0.x, p0.y);
      vertex(p1.x, p1.y);
      vertex(p2.x, p2.y);
      vertex(p3.x, p3.y);
      endShape();
      debugDrawFlag = false;
    }

    stroke(0);
    leftLine.draw();
    rightLine.draw();
    stroke(128);
    centralLine.draw();
  }
}
