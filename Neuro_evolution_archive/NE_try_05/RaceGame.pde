
class RaceGame {

  PlayField field;
  Vehicle vehicle;


  RaceGame() {
    field = new PlayField();
    vehicle = new Vehicle(120, 200, 800, 6);
    restartSingleVehicle();
  }

  void restartSingleVehicle() {
    vehicle.dead = false;
    vehicle.position = field.getStartPosition();
    vehicle.initDirectionAndSpeed( field.getStartDirection(), 0, 0);
    vehicle.stats.reset();
  }

  void integrateSingleVehicle(float frameTime) {
    if (!vehicle.dead) {

      //vehicle.checkTotalDistance(field);

      vehicle.integrate(frameTime);
      vehicle.checkSensors(field);
      vehicle.checkIfIsInsidePlayfield(field);
      if (!vehicle.dead) {
        float courseDistance = field.getCourseDistance( vehicle );
        println(courseDistance);
      }
      
    }
  }




  void draw() {
    field.draw();
    vehicle.draw();
  }
}




import java.awt.geom.Line2D;

boolean lineSegmentsIntersect(Line l0, Line l1) {
  return lineSegmentsIntersect(l0.getP0(), l0.getP1(), l1.getP0(), l1.getP1());
}

boolean lineSegmentsIntersect(PVector p0, PVector p1, PVector p2, PVector p3) {
  return Line2D.Float.linesIntersect(p0.x, p0.y, p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
}
