


// VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE VEHICLE





class Vehicle {

  final float sizeToSensorFac = 5;
  final float sizeToCentralSensorFac = 6;
  final float sensorDegrees = 20;

  boolean DRAW_IMAGE = false;
  float size;

  float turningSpeed; // degrees per second
  float thrustSpeed; // acceleration in pixels/sˆ2
  float breakSpeed; // break in pixels/sˆ2

  PVector lastPosition = new PVector();
  PVector position = new PVector();
  PVector velocity = new PVector();
  float direction;
  float speed;
  PVector[] points;
  boolean[] inSensorVecRange;
  PVector[] localSensorVecs;

  boolean dead;

  VehicleStats stats = new VehicleStats();

  boolean turningLeft=false, turningRight=false, increasingSpeed=false, decreasingSpeed=false;

  Vehicle(float turningSpeed, float thrustSpeed, float breakSpeed, float size) {
    this.turningSpeed = turningSpeed;
    this.thrustSpeed = thrustSpeed;
    this.breakSpeed = breakSpeed;
    this.size = size;
    initDirectionAndSpeed(radians(45), 0, 0);
    points = new PVector[] {  new PVector(-size, size), new PVector(0, -size*1.5), new PVector(size, size) };
    localSensorVecs = new PVector[3];
    inSensorVecRange = new boolean[3];
    for (int i=0; i<3; i++) {
      float rd = map(i, 0, 2, -radians(sensorDegrees), radians(sensorDegrees));
      float len = size*(i==1 ? sizeToCentralSensorFac : sizeToSensorFac);
      PVector sensorVec = new PVector(0, -len);
      sensorVec.rotate(rd);
      localSensorVecs[i] = sensorVec;
      inSensorVecRange[i] = false;
    }
  }

  void turnAndThrust(boolean left, boolean right, boolean thrust, boolean brek) {
    turningLeft=left;
    turningRight=right;
    increasingSpeed=thrust;
    decreasingSpeed=brek;
  }
  void turnLeft(boolean turn) {
    turningLeft = turn;
  }
  void turnRight(boolean turn) {
    turningRight = turn;
  }
  void increaseSpeed(boolean thrust) {
    increasingSpeed = thrust;
  }
  void decreaseSpeed(boolean doBreak) {
    decreasingSpeed = doBreak;
  }


  void initDirectionAndSpeed(float dir, float spd, float frameTime) {

    // from moving to speed=0 -> reset no speed total time
    if (this.speed!=0 && spd==0) {
      stats.noSpeedTotalTime = 0;
    }
    // from speeding to no speeding -> reset no speed total time
    else if (this.speed==0 && speed!=0) {
      stats.noSpeedTotalTime = 0;
    } else if (spd==0) {
      stats.noSpeedTotalTime += frameTime;
    }

    this.direction = dir;
    this.speed = spd;
    setVelocityFromSpeedAndDirection();
  }

  void setVelocityFromSpeedAndDirection() {
    velocity = new PVector(0, -1);
    velocity.rotate(this.direction);
    velocity.setMag(this.speed);
  }



  void checkIfIsInsidePlayfield(PlayField field) {
    boolean boundaryCross = field.checkBoundaryCrossing(lastPosition, position);
    if (boundaryCross) {
      dead = true;
      println("Dead due to boundary crossing");
    }
  }






  void integrate(float seconds) { 

    lastPosition.set(position);
    applyTurningAndSpeed(seconds);
    stats.updateAverageSpeed(speed);
  }



  void applyTurningAndSpeed(float seconds) {
    if (turningLeft||turningRight||increasingSpeed||decreasingSpeed) {
      float deltaTurnRad = 0;
      float deltaSpeed = 0;
      if (turningLeft||turningRight) {
        float deltaTurnDeg = ((turningRight?1:0)+(turningLeft?-1:0)) * turningSpeed * seconds;
        deltaTurnRad = radians(deltaTurnDeg);
      }
      if (increasingSpeed||decreasingSpeed) {
        deltaSpeed = ((increasingSpeed?thrustSpeed:0)-(decreasingSpeed?breakSpeed:0)) * seconds;
      }
      float newSpeed = max(0, speed+deltaSpeed);
      initDirectionAndSpeed( direction+deltaTurnRad, newSpeed, seconds);
    }

    position = PVector.add( position, PVector.mult( velocity, seconds ) );
  }




  void checkSensors(PlayField field) {

    //Line[][] LR = new Line[][] { leftLines, rightLines };
    TrackSegment[] segments = field.trackSegments;
    for (int sensor=0; sensor<3; sensor++) {

      //local to global:
      PVector sensorPoint = localSensorVecs[sensor].copy();
      sensorPoint.rotate(direction);
      sensorPoint.add( position );
      Line sensorLine = new Line( position, sensorPoint );

      boolean intersect = false;


      for (int i=0; i<segments.length; i++) {
        if (lineSegmentsIntersect(segments[i].leftLine, sensorLine) || lineSegmentsIntersect(segments[i].rightLine, sensorLine)) {
          intersect=true;
          break;
        }
      }
      inSensorVecRange[sensor] = intersect;
    }
  }









  void draw() {
    strokeWeight(1);
    pushMatrix();

    translate(position.x, position.y);

    if (!DRAW_IMAGE) {
      for (int i=0; i<localSensorVecs.length; i++) {
        PVector globalSensorVec = localSensorVecs[i].copy();
        globalSensorVec.rotate(direction);
        stroke(inSensorVecRange[i] ? color(255, 0, 0) : color(0, 128, 0));
        line(0, 0, globalSensorVec.x, globalSensorVec.y);
      }
    }

    rotate(direction);

    if (!DRAW_IMAGE) {
      stroke(0);
      fill(255);
      beginShape(TRIANGLES);
      for (int i=0; i<points.length; i++) {
        vertex(points[i].x, points[i].y);
      }
      endShape();
    } else {
      float aspRat = (float)img.width/(float)img.height;
      float w = size*10;
      float h = size*10/aspRat;
      rotate(-HALF_PI);
      scale(-1, 1);
      image(img, -w/2, -h/2, w, h);
    }



    popMatrix();
  }
}








class VehicleStats {

  int AVERAGE_SPEED_DATA_POINTS = 60;

  float noSpeedTotalTime;
  ArrayList<Float> speedMeasurements;
  float averageSpeed;

  int courseRounds = 0;
  float singleRoundDistance = 0;
  private float _totalCourseDistance = 0;

  void reset() {
    courseRounds = 0;
    singleRoundDistance = 0;
    _totalCourseDistance = 0;
    noSpeedTotalTime=0;
    averageSpeed = Float.NaN;
    speedMeasurements = new ArrayList<Float>();
    courseRounds = 0;
  }

  void updateTotalCourseDistance(PlayField field) {
    _totalCourseDistance = 0;
    if (courseRounds>=0) {
      _totalCourseDistance = courseRounds*field.totalDistance + singleRoundDistance;
    } else {
      _totalCourseDistance = (courseRounds+1)*field.totalDistance - (field.totalDistance-singleRoundDistance);
    }
  }
  
  float getTotalCourseDistance() {
    return _totalCourseDistance;
  }

  void updateAverageSpeed(float currentSpeed) {
    speedMeasurements.add(currentSpeed);
    while (speedMeasurements.size()>AVERAGE_SPEED_DATA_POINTS) {
      speedMeasurements.remove(0);
    }
    if (speedMeasurements.size()==AVERAGE_SPEED_DATA_POINTS) {
      float total = 0;
      for (Float f : speedMeasurements) {
        total += f;
      }
      total /= AVERAGE_SPEED_DATA_POINTS;
      averageSpeed = total;
    }
  }
}
