class Network {

  boolean DEBUG = true;
  float BIAS = 1;
  int layerCount;

  Neuron[][] neurons;

  Network(int[] neuronsPerLayer, String[][] names) {

    layerCount = neuronsPerLayer.length;
    neurons = new Neuron[layerCount][];

    for (int layerIndex=0; layerIndex<layerCount; layerIndex++) {

      if (DEBUG) println("layer "+layerIndex+" -------------------");
      int neuronCount = neuronsPerLayer[layerIndex];
      neurons[layerIndex] = new Neuron[neuronCount];
      
      String[] namesPerLayer = names[layerIndex];

      // layer 0 : just value neurons
      if (layerIndex==0) {
        for (int j=0; j<neuronCount; j++) {
          String name = "n"+layerIndex+""+j;
          if (namesPerLayer!=null) name = namesPerLayer[j];
          neurons[layerIndex][j] = new ValueNeuron(name, 0);//inputCount);
        }
      } else {
        int prevLayerIndex = layerIndex-1;
        int prevLayerNeuronCount = neuronsPerLayer[prevLayerIndex];
        for (int j=0; j<neuronCount; j++) {
          String name = "n"+layerIndex+""+j;
          if (namesPerLayer!=null) name = namesPerLayer[j];
          ActiveNeuron neuron = new ActiveNeuron(name, BIAS);//inputCount);
          neurons[layerIndex][j] = neuron; // assign to array
          for (int prevN=0; prevN<prevLayerNeuronCount; prevN++) {
            neuron.addConnection( neurons[prevLayerIndex][prevN] );
          }
          if (DEBUG) println(neuron+" has "+neuron.inputs.length+" inputs");
        }
      }
    }
  }






  void feedForward(float[] inputValues) {


    for (int layer=0; layer<layerCount; layer++) {

      Neuron[] layerNeurons = neurons[layer];

      if (layer==0) {
        for (int i=0; i<layerNeurons.length; i++) {
          ValueNeuron vn = (ValueNeuron)layerNeurons[i];
          vn.value = inputValues[i];
        }
      } else {
        for (int i=0; i<layerNeurons.length; i++) {
          ActiveNeuron an = (ActiveNeuron)layerNeurons[i];
          an.feedforward();
        }
      }
    }
  }





  float[] getLastLayerOutputs() {
    Neuron[] layerNeurons = neurons[layerCount-1];
    float[] output = new float[layerNeurons.length];
    for (int i=0; i<layerNeurons.length; i++) {
      ActiveNeuron an = (ActiveNeuron)layerNeurons[i];
      output[i] = an.getNextOutput();
    }
    return output;
  }






  void draw(float diameter, float spaceX, float spaceY) {

    ellipseMode(DIAMETER);
    stroke(0);
    noFill();


    for (int renderPass=0; renderPass<2; renderPass++) {
      float posX = 0;

      for (int layer=0; layer<layerCount; layer++) {

        Neuron[] layerNeurons = neurons[layer];
        int neuronAmount = layerNeurons.length;
        for (int n=0; n<neuronAmount; n++) {

          float totalY = (neuronAmount-1)*spaceY;
          float posY=0;
          if (neuronAmount>1)
            posY = map(n, 0, neuronAmount-1, -totalY/2, totalY/2);

          if (renderPass==0) 
            layerNeurons[n].drawInputs(posX, posY, diameter);
          else
            layerNeurons[n].drawBody(posX, posY, diameter, layer==0?LEFT:(layer==layerCount-1?RIGHT:Integer.MIN_VALUE));
        }

        posX += spaceX;
      }
    }
  }
}


/** Stores neuron names with their weights */
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
class NetworkWeights {
  
  HashMap<String,float[]> weightsOnNeurons;
  
  NetworkWeights(HashMap<String,float[]> weightsOnNeurons) {
    this.weightsOnNeurons = weightsOnNeurons;
  }
  
  NetworkWeights(Network brain) {
    weightsOnNeurons = new HashMap<String, float[]>();
    for (int i=0;i<brain.neurons.length;i++) {
      for (int j=0;j<brain.neurons[i].length;j++) {
        Neuron n = brain.neurons[i][j];
        if (n instanceof ActiveNeuron) {
          ActiveNeuron an = (ActiveNeuron)n;
          String name = an.getName();
          float[] weights = an.weights;
          weightsOnNeurons.put(name,weights);
        }
      }
    }
  }
  
  NetworkWeights(String[] settings) {
    weightsOnNeurons = new HashMap<String, float[]>();
    for (String line:settings) {
      //println(line);
      String[] parts = line.split(" "); // i.e. n12 0.831041932106 0.161653637886 0.025521039963 0.527899026871
      String name = parts[0];
      //println(name);
      float[] weights = new float[parts.length-1];
      for (int i=0;i<weights.length;i++) {
        weights[i] = Float.parseFloat(parts[i+1]);
      }
      weightsOnNeurons.put(name, weights);
      //println(weights);
    }
  }
  
  
  
  void applyTo(Network brain) {
    for (int i=0;i<brain.neurons.length;i++) {
      for (int j=0;j<brain.neurons[i].length;j++) {
        Neuron n = brain.neurons[i][j];
        if (n instanceof ActiveNeuron) {
          ActiveNeuron an = (ActiveNeuron)n;
          String name = an.getName();
          float[] weights = weightsOnNeurons.get(name);
          an.weights = weights;
        }
      }
    }
  }
  
  ArrayList<String> getSortedNeuronNames() {
    ArrayList<String> sortedKeys = new ArrayList(weightsOnNeurons.keySet());
    sortedKeys.sort( new Comparator<String>() {
      public int compare(String s0, String s1) {
        return s0.compareTo(s1);
      }
    }
    );
    return sortedKeys;
  }
  
  
  
  
  float[] sortedWeightsToFloatArray() {

    // sort keys:
    ArrayList<String> sortedKeys = getSortedNeuronNames();

    // Add all weights:
    ArrayList<Float> weightList = new ArrayList<Float>();
    for (String theKey : sortedKeys) {
      float[] weights = weightsOnNeurons.get(theKey);
      for (float weight : weights) {
        weightList.add(weight);
      }
    }

    float[] out = new float[weightList.size()];
    for (int i=0; i<out.length; i++) {
      out[i] = weightList.get(i);
    }

    return out;
  }
  
  
  NetworkWeights createNewFromSortedWeights(float[] sortedWeights) {
    
    // get sorted keys
    ArrayList<String> sortedKeys = getSortedNeuronNames();
    
    // create structure & add weight data
    HashMap<String, float[]> newWeightsOnNeurons = new HashMap<String, float[]>();
    int flatIndex = 0;
    for (String theKey : sortedKeys) {
      // copy weights
      float[] newWeights = new float[ weightsOnNeurons.get(theKey).length ];
      for (int i=0;i<newWeights.length;i++) {
        newWeights[i] = sortedWeights[flatIndex];
        flatIndex++;
      }
      // and put into new map
      newWeightsOnNeurons.put( theKey, newWeights );
    }
    
    return new NetworkWeights( newWeightsOnNeurons );
    
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    Iterator<Entry<String, float[]>> it = weightsOnNeurons.entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry<String, float[]> pair = (Map.Entry<String, float[]>) it.next();
      
      sb.append(pair.getKey()+": ");
      float[] weights = pair.getValue();
      for (float w:weights) sb.append(w+", ");
      sb.append("\r\n");
    }
    return sb.toString();
  }
  
  
  public String toSavableString() {
    String NL = System.getProperty("line.separator");
    StringBuilder sb = new StringBuilder();
    Iterator<Entry<String, float[]>> it = weightsOnNeurons.entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry<String, float[]> pair = (Map.Entry<String, float[]>) it.next(); 
      sb.append(pair.getKey());
      sb.append(" ");
      float[] weights = pair.getValue();
      for (int i=0;i<weights.length;i++) {
        sb.append(nf(weights[i],1,12));
        if (i<weights.length-1)
          sb.append(" ");
      }
      sb.append(NL);
    }
    return sb.toString();
  }
  
}
