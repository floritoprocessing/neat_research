
class RaceGame {

  PlayField field;
  Vehicle vehicle;


  RaceGame() {
    field = new PlayField();
    vehicle = new Vehicle(120, 200, 800, 6);
    restartSingleVehicle();
  }

  void restartSingleVehicle() {
    vehicle.dead = false;
    vehicle.position = field.getStartPosition();
    vehicle.initDirectionAndSpeed( field.getStartDirection(), 0, 0);
    vehicle.stats.reset();
  }

  void integrateSingleVehicle(float frameTime) {
    if (!vehicle.dead) {

      //vehicle.checkTotalDistance(field);

      vehicle.integrate(frameTime);
      vehicle.checkSensors(field);
      vehicle.checkIfIsInsidePlayfield(field);
      if (!vehicle.dead) {

        float lastTotalCourseDistance = vehicle.stats.singleRoundDistance;
        float currentCourseDistance = field.getCourseDistance( vehicle );

        // check cross startLine  
        Line startLine = field.getStartLine();
        boolean crossStartLine = lineSegmentsIntersect( vehicle.lastPosition, vehicle.position, startLine.getP0(), startLine.getP1() );
        if (crossStartLine) {

          if (lastTotalCourseDistance>currentCourseDistance) {
            println("CROSSSS!!!!!!!!!!! in correct direction");
            vehicle.stats.courseRounds++;
          } else {
            println("CROSSSS!!!!!!!!!!! in REVERSE direction");
            vehicle.stats.courseRounds--;
          }
        }

        //println(currentCourseDistance);
        vehicle.stats.singleRoundDistance = currentCourseDistance;
        vehicle.stats.updateTotalCourseDistance( field );
        //println(vehicle.stats.getTotalCourseDistance());
      }
    }
  }




  void draw() {
    field.draw();
    vehicle.draw();
  }
}




import java.awt.geom.Line2D;

boolean lineSegmentsIntersect(Line l0, Line l1) {
  return lineSegmentsIntersect(l0.getP0(), l0.getP1(), l1.getP0(), l1.getP1());
}

boolean lineSegmentsIntersect(PVector p0, PVector p1, PVector p2, PVector p3) {
  return Line2D.Float.linesIntersect(p0.x, p0.y, p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
}


PVector lineLineIntersection(Line a, Line b) {
  return lineLineIntersection(a.getP0(), a.getP1(), b.getP0(), b.getP1());
}

PVector lineLineIntersection(PVector A, PVector B, PVector C, PVector D) 
{ 
  // Line AB represented as a1x + b1y = c1 
  float a1 = B.y - A.y; 
  float b1 = A.x - B.x; 
  float c1 = a1*(A.x) + b1*(A.y); 

  // Line CD represented as a2x + b2y = c2 
  float a2 = D.y - C.y; 
  float b2 = C.x - D.x; 
  float c2 = a2*(C.x)+ b2*(C.y); 

  float determinant = a1*b2 - a2*b1; 

  if (determinant == 0) 
  { 
    // The lines are parallel. This is simplified 
    // by returning a pair of FLT_MAX 
    return null;//new PVector(Double.MAX_VALUE, Double.MAX_VALUE);
  } else
  { 
    float x = (b2*c1 - b1*c2)/determinant; 
    float y = (a1*c2 - a2*c1)/determinant; 
    return new PVector(x, y);
  }
} 
