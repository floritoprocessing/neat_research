class NetworkToVehicle {
 
  boolean left, right, thrust, brek;
  
  void reset() {
    left = right = thrust = brek = false;
  }
  
  void influenceVehicle() {
    float[] outputs = network.getLastLayerOutputs();
    left = outputs[0]>0;
    right = outputs[1]>0;
    thrust = outputs[2]>0;
    brek = outputs[3]>0;
    
    update();
  }
  
  void update() {
    raceGame.vehicle.turnAndThrust(left,right,thrust,brek);
  }
}
