// ================================================ BRAIN

float INPUT_NEURON_AMOUNT_TO_HIDDEN_NEURON_AMOUNT_FACTOR = 2;
boolean ADD_BIAS_CONNECTION = false;
float BIAS = 1;

// ================================================ VEHICLE

int VEHICLE_SENSORS = 9;
float VEHICLE_SENSOR_DEGREES = 90;
float VEHICLE_SIZE_TO_SENSOR_FAC = 4;
float VEHICLE_SIZE_TO_CENTRAL_SENSOR_FAC = 5;

// ================================================ DRAWING

int FPS = 60;
float DRAW_NEURON_DIAMETER = 10;
float DRAW_NEURON_SPACE_X = 200;
float DRAW_NEURON_SPACE_Y = 30;

float DRAW_NEURON_WEIGHT_TO_STROKE_WEIGHT = 1;
float DRAW_NEURON_STROKE_WEIGHT_MAX = 8;
