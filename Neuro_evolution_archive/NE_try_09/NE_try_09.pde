float MINIMUM_AVERAGE_SPEED = 5;

int MODE_CREATE_GAME = 0;
int MODE_PLAY_GAME = 1;
int MODE_PLAY_BRAIN = 2;
int MODE_TRAINING_ROUNDS = 3;
int MODE_BREEDING = 4;
int mode = MODE_BREEDING;

boolean FAST_BREEDING = true;

boolean BREEDING_LOAD = true;
String BREEDING_GENERATION_FOLDER_TO_LOAD = "Breeding 2018_11_18_13_07_11/005";
int BREEDING_LOAD_GEN = 5;
String FILE_TO_LOAD = BREEDING_GENERATION_FOLDER_TO_LOAD+"/000 Candidate g015 006 totalDistance 6124.txt";//"156.txt";//"000 Candidate 2 totalDistance 319.txt"; //"001 Candidate 14 totalDistance 310.txt"


PImage img;

String breedingFolder;

int lastTime;
RaceGame raceGame;
Network network;
NetworkToVehicle networkToVehicle = new NetworkToVehicle();
Trainer trainer;

int generationIndex = 0;
Generation generation = null;


void setup() {
  size(1200, 600, P3D);
  //fullScreen(P3D);

  
  //if (true) return;
  
  frameRate(FPS);

  img = loadImage("pig2.png");
  raceGame = new RaceGame();
  resetNetwork();
  trainer = new Trainer(1, 0, MINIMUM_AVERAGE_SPEED);
  trainer.maxRounds = 30;
  if (mode==MODE_BREEDING) {
    
    if (!BREEDING_LOAD) {
      trainer.maxRounds = INITIAL_GENERATION_SIZE;
    } else {
      println("Loading a generation from folder: "+BREEDING_GENERATION_FOLDER_TO_LOAD);
      
      String[] files = getFileList(BREEDING_GENERATION_FOLDER_TO_LOAD);
      trainer.maxRounds = files.length;
      TrainerResults[] results = new TrainerResults[files.length];
      for (int i=0;i<files.length;i++) {
        String fileName = BREEDING_GENERATION_FOLDER_TO_LOAD+"/"+files[i];
        println("Loading "+fileName);
        String[] lines = loadStrings(fileName);
        results[i] = new TrainerResults(lines);
      }
      generation = new Generation(results, true);
      generationIndex = BREEDING_LOAD_GEN;
      //generation.newGeneration = new ArrayList<NetworkWeights>();
      
      //exit();
    }
  }

  if (mode==MODE_CREATE_GAME) raceGame.field.initMakePlayfield();
  if (mode==MODE_PLAY_BRAIN || mode==MODE_TRAINING_ROUNDS || mode==MODE_BREEDING) {
    restartBrainGame();
  }
  if (mode==MODE_BREEDING) {
    breedingFolder="Breeding "+nf(year(), 4)+"_"+nf(month()+1, 2)+"_"+nf(day(), 2)+"_"+nf(hour(), 2)+"_"+nf(minute(), 2)+"_"+nf(second(), 2);
  }
  lastTime = millis();
}

void resetNetwork() {
  
  String[] inputNames = new String[VEHICLE_SENSORS+1];
  int half = (VEHICLE_SENSORS-1)/2;
  for (int i=0;i<inputNames.length-1;i++) {
    if (i<half) inputNames[i]="left"+(i);
    else if (i==half) inputNames[i]="center";
    else if (i>half) inputNames[i]="right"+(half-i+1);
  }
  inputNames[inputNames.length-1] = "speed";
  
  String[][] names = new String[][] {
    inputNames,
    null,
    new String[] { "left", "right", "thrust", "break" }
  };
  
  int hiddenNeuronAmount = (int)(inputNames.length * INPUT_NEURON_AMOUNT_TO_HIDDEN_NEURON_AMOUNT_FACTOR);
  
  network = new Network(new int[] {VEHICLE_SENSORS+1, hiddenNeuronAmount, 4}, names);
}

void mousePressed() {
  if (mode==MODE_CREATE_GAME) raceGame.field.addPoint(mouseX, mouseY);
}

void keyPressed() {
  if (key=='=') frameRate(FPS+=25);
  if (key=='-' && FPS>50) frameRate(FPS-=25);
  
  if (mode==MODE_CREATE_GAME) {
    if (key=='f') {
      raceGame.field.finalize();
      mode = MODE_PLAY_GAME;
    }
  } else if (mode==MODE_PLAY_GAME) {
    if (key=='r') {
      raceGame.restartSingleVehicle();
    }
    if (key==CODED) {
      if (keyCode==LEFT) raceGame.vehicle.turnLeft(true);
      else if (keyCode==RIGHT) raceGame.vehicle.turnRight(true);
      else if (keyCode==UP) raceGame.vehicle.increaseSpeed(true);
      else if (keyCode==DOWN) raceGame.vehicle.decreaseSpeed(true);
    }
  } else if (mode==MODE_PLAY_BRAIN) {
    if (key=='r') {
      restartBrainGame();
    } else if (key=='l') {
      restartBrainGame();
      
      String[] lines = loadStrings(FILE_TO_LOAD);
      TrainerResults results = new TrainerResults(lines);
      network.applyWeights(results.networkWeights);
      //network.loadBrain(FILE_TO_LOAD);
      
    }
  }
}



void restartBrainGame() {
  resetNetwork();
  raceGame.restartSingleVehicle();
  networkToVehicle.reset();

  if (mode==MODE_BREEDING & generation!=null) {  
    int round = trainer.currentRound;
    NetworkWeights weights = generation.newGeneration.get(round);
    weights.applyTo(network);
  }

  trainer.nextRound(raceGame.vehicle, network);
}



void keyReleased() {
  if (mode==MODE_PLAY_GAME) {
    if (keyCode==LEFT) raceGame.vehicle.turnLeft(false);
    else if (keyCode==RIGHT) raceGame.vehicle.turnRight(false);
    else if (keyCode==UP) raceGame.vehicle.increaseSpeed(false);
    else if (keyCode==DOWN) raceGame.vehicle.decreaseSpeed(false);
  }
}





void draw() {
  background(255);

  int ti = millis();
  float frameTime = 0.016;//(ti-lastTime)/1000.0;

  boolean iterateWithinFrames = false;
  do {
    
    if (mode==MODE_BREEDING && FAST_BREEDING) {
      //if (trainer.currentRound==1 && !trainer.currentVehicle.dead) iterateWithinFrames = true;
    }

    // integrate racegame
    if (mode!=MODE_CREATE_GAME)
      raceGame.integrateSingleVehicle(frameTime);

    // integrate trainer
    if (mode==MODE_PLAY_BRAIN || mode==MODE_TRAINING_ROUNDS || mode==MODE_BREEDING) {
      trainer.integrate(raceGame.field, generationIndex, frameTime);
    }

    // feedForward brain
    //boolean[] sensorActives = raceGame.vehicle.inSensorVecRange;
    
    float[] inputValues = new float[raceGame.vehicle.inSensorVecDistance.length+1];
    
    for (int i=0; i<inputValues.length-1; i++)
      inputValues[i] = raceGame.vehicle.inSensorVecDistance[i];
      //inputValues[i] = sensorActives[i] ? 1 : -1;
      
    inputValues[inputValues.length-1] = raceGame.vehicle.speed / 200.0;
      
    network.feedForward(inputValues);

    // brain game: BRAIN -> VEHICLE
    // brain game: END CONDITIONS
    if (mode==MODE_PLAY_BRAIN || mode==MODE_TRAINING_ROUNDS || mode==MODE_BREEDING) {
      networkToVehicle.influenceVehicle();
      
      // end of round
      if (trainer.finalEnd) {
        
        
        // rounds
        if (mode==MODE_PLAY_BRAIN || trainer.results.size()<trainer.maxRounds) {
          
          trainer.sortResults();
          restartBrainGame();
          if (FAST_BREEDING && trainer.results.size()<trainer.maxRounds-1) {
            iterateWithinFrames = true;
          } else {
            iterateWithinFrames = false;
          }
          
        } else {

          trainer.sortResults(); 

          if (mode==MODE_TRAINING_ROUNDS) {
            trainer.saveAllResults();
            exit();
          } else if (mode==MODE_BREEDING) {

            String folderName = breedingFolder +"/" + nf(generationIndex, 3) + "/";
            trainer.saveAllResults(folderName);

            // current generation
            generation = new Generation( trainer.results.toArray(new TrainerResults[0]), false );
            generation.breedNew();

            trainer.reset();
            trainer.maxRounds = generation.newGeneration.size();
            generationIndex++;

            restartBrainGame();

            //networkWeights.applyTo(network);
            //noLoop();
          }
        }
      }
    }
    
    
    
    
  } while (iterateWithinFrames==true);


  //
  //
  // DRAWING
  //
  //


  fill(0);


  // DRAW brain game
  if (mode==MODE_PLAY_BRAIN || mode==MODE_TRAINING_ROUNDS || mode==MODE_BREEDING) {
    text("Round "+(trainer.currentRound)+"/"+trainer.maxRounds+": Time="+nf(trainer.totalTime, 1, 2), 10, 20);
    if (!trainer.endTraining) {
      text("Training running. Distance travelled="+nf(raceGame.vehicle.stats.getTotalCourseDistance(), 1, 1), 10, 35);
    }
    if (trainer.endTraining && !trainer.finalEnd) {
      text("Ending game...", 10, 35);
    }
    if (trainer.results.size()>1) {
      text("Best so far is "+trainer.results.get(0).identifier+" with "+nf(trainer.results.get(0).totalDistance, 1, 1), 10, 50);
    }
  }

  // DRAW raceGame
  raceGame.draw();

  // DRAW network
  pushMatrix();
  translate(width/2+100, height/2);
  
  network.draw(DRAW_NEURON_DIAMETER, DRAW_NEURON_SPACE_X, DRAW_NEURON_SPACE_Y);
  popMatrix();


  fill(0);
  if (mode==MODE_CREATE_GAME) {
    text("Press [f] to finalize playing field. Copy from console.", 10, height-20);
  }
  if (mode==MODE_PLAY_GAME) {
    text("Press [r] to restart, use arrow keys to steer.", 10, height-20);
  }
  if (mode==MODE_PLAY_BRAIN) {
    text("[r] to restart, [l] to load network ("+FILE_TO_LOAD+")", 10, height-20);
  }
  if (mode==MODE_BREEDING) {
    text("Breeding generation "+(generationIndex+1), 10, height-20);
  }


  lastTime = ti;
  //noLoop();
}
