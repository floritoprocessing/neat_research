


/*

 
 FIELD
 
 
 */

import java.util.Arrays;


class PlayField {

  boolean makePlayfield = false;
  //Line[] centralLines;
  //Line[] leftLines, rightLines;
  final float roadWidth = 20;

  TrackSegment[] trackSegments;

  float totalDistance = 0;

  float[] FIELD1 = new float[] {
    88, 96, 122, 76, 206, 69, 429, 111, 478, 253, 467, 416, 413, 469, 381, 452, 378, 394, 395, 337, 
    392, 278, 381, 220, 362, 185, 328, 179, 293, 200, 302, 321, 304, 421, 289, 449, 254, 459, 152, 461, 
    92, 449, 81, 361, 100, 331, 165, 301, 182, 271, 146, 214, 125, 201, 77, 178, 72, 128};

  float[] FIELD2_ZIG_ZAG = new float[] {
    106, 117, 141, 119, 187, 135, 245, 128, 288, 88, 336, 93, 357, 134, 391, 158, 430, 138, 471, 167, 
    485, 220, 466, 276, 422, 297, 381, 288, 379, 237, 323, 223, 292, 288, 308, 340, 329, 352, 339, 402, 
    315, 451, 219, 467, 145, 466, 132, 438, 163, 393, 226, 376, 233, 325, 195, 293, 85, 303, 67, 282, 
    53, 195, 86, 138};
    
 float[] FIELD3_SMALL_ZIG_ZAG = new float[] {
   100, 127,   145, 115,   172, 88,   211, 71,   247, 80,   274, 107,   289, 125,   315, 126,   346, 115,   365, 105,
   411, 100,   434, 110,   450, 134,   455, 185,   449, 211,   433, 246,   403, 253,   370, 249,   350, 230,   325, 211,
   295, 216,   275, 239,   251, 277,   226, 299,   187, 310,   160, 306,   148, 290,   130, 274,   102, 270,   75, 263,
   58, 243,   50, 213,   53, 182,   64, 154,   74, 146};

  PlayField() {
    createLines(true, FIELD1);
  }

  void initMakePlayfield() {
    makePlayfield = true;
    trackSegments = new TrackSegment[0];
  }



  void addPoint(float x, float y) {
    //println("addPoint()");
    //println("  ",x,y);
    if (trackSegments.length==0) {
      createLines(false, x, y, x, y);
    } else {
      ArrayList<PVector> points = new ArrayList<PVector>();
      for (int i=0; i<trackSegments.length; i++) {
        points.add(trackSegments[i].centralLine.getP0());
      }
      points.add(trackSegments[trackSegments.length-1].centralLine.getP1());
      points.add( new PVector(x,y) );
      //println("  current points: ");
      //println("    ",points);
      makeLines(points, false);
    }
  }



  void createLines(boolean closeLoop, float... xyPairs) {
    //println("createLines()");
    //println(xyPairs);
    ArrayList<PVector> points = new ArrayList<PVector>();
    for (int i=0; i<xyPairs.length; i+=2) {
      points.add( new PVector(xyPairs[i], xyPairs[i+1]) );
    }
    makeLines(points, closeLoop);
  }



  void finalize() {
    ArrayList<PVector> points = new ArrayList<PVector>();
    for (int i=0; i<trackSegments.length; i++) {
      points.add(trackSegments[i].centralLine.getP0());
    }
    print("float[] xyPoints = new float[] {");
    for (int i=0; i<points.size(); i++) {
      if (i%10==0) {
        println();
      }
      print("   "+int(points.get(i).x)+", "+int(points.get(i).y));
      if (i<points.size()-1) print(",");
    }
    print("};");
    makeLines(points, true);
    makePlayfield = false;
  }



  void makeLines(ArrayList<PVector> points, boolean closeLoop) {
    //println("makeLines()");
    //println("  ",points);
    trackSegments = new TrackSegment[closeLoop ? points.size() : points.size()-1];
    totalDistance = 0;
    
    int amount = closeLoop ? points.size() : points.size()-1;
    for (int i=0; i<amount; i++) {
      trackSegments[i] = new TrackSegment();
      PVector p0 = points.get(i);
      int i1 = (i+1)%(points.size());
      PVector p1 = points.get( i1 );
      trackSegments[i].centralLine = new Line(p0, p1);
      totalDistance += trackSegments[i].centralLine.getMagnitude();
    }
    //println(trackSegments);
    makeSideLines(trackSegments, roadWidth);
  }



  void makeSideLines(TrackSegment[] trackSegments, float wid) {


    for (int i=0; i<trackSegments.length; i++) {
      Line line = trackSegments[i].centralLine;
      PVector dir = PVector.sub(line.getP1(), line.getP0());
      PVector dirLeft = dir.copy();
      dirLeft.rotate(radians(-90));
      dirLeft.setMag( wid );
      trackSegments[i].leftLine = new Line( PVector.add(line.getP0(), dirLeft), PVector.add(line.getP1(), dirLeft) );
      PVector dirRight = dir.copy();
      dirRight.rotate(radians(90));
      dirRight.setMag( wid );
      trackSegments[i].rightLine = new Line( PVector.add(line.getP0(), dirRight), PVector.add(line.getP1(), dirRight) );
    }

    // now adjust all left and right lines for intersections
    //Line[][] leftAndRight = new Line[][] { leftLines, rightLines };
    //for (Line[] sideLines : leftAndRight) {
    //for (TrackSegment segment:trackSegments) {
    for (int left=0; left<2; left++) {

      for (int i=0; i<trackSegments.length; i++) {

        int index0 = i;
        int index1 = i<trackSegments.length-1 ? i+1 : 0;

        Line l0 = left==0 ? trackSegments[index0].leftLine : trackSegments[index0].rightLine;
        Line l1 = left==0 ? trackSegments[index1].leftLine : trackSegments[index1].rightLine;

        PVector intersectionPoint = lineLineIntersection(l0.getP0(), l0.getP1(), l1.getP0(), l1.getP1());
        if (intersectionPoint!=null) {
          l0.setP1( intersectionPoint );
          l1.setP0( intersectionPoint );
        }
      }
    }
  }



  float getCourseDistance(Vehicle vehicle) {

    PVector P = vehicle.position;
    float totalDistance = 0;

    for (int i=0; i<trackSegments.length; i++) {
      TrackSegment segment = trackSegments[i];

      // in Segment AB CD, find line GH that is parallel to AB and CD that goes through P
      // then find percentage of P on GH, meaning ratio GP:GH
      PVector A = segment.rightLine.getP0();
      PVector B = segment.rightLine.getP1();
      PVector C = segment.leftLine.getP0();
      PVector D = segment.leftLine.getP1();


      if (B.x-A.x==0) {
        System.err.println("TODO: infinite inclination, use reverse?");
      } else {
        float inclinationAB = (B.y-A.y)/(B.x-A.x);
        // GH: line with same inclination as AB that goes through the point P
        // GH: y = (x-P.x) * inclinationAB + P.y
        PVector GH0 = new PVector(0, (0-P.x)*inclinationAB + P.y);
        PVector GH1 = new PVector(width, (width-P.x)*inclinationAB + P.y);

        // find G: intersection AC with GH
        // find H: intersection BD with GH
        boolean hasG = lineSegmentsIntersect(A, C, GH0, GH1);
        boolean hasH = lineSegmentsIntersect(B, D, GH0, GH1);
        if (hasG && hasH) {
          PVector G = lineLineIntersection(A, C, GH0, GH1);
          PVector H = lineLineIntersection(B, D, GH0, GH1);
          // now find percentage of P on line GH
          Line GH = new Line(G, H);
          float segmentPercentage = GH.calcPercentageFromX(P.x);
          if (segmentPercentage>=0 && segmentPercentage<1) {
            totalDistance += segment.centralLine.getMagnitude() * segmentPercentage;
            return totalDistance;
          }
        }
      }
      totalDistance += segment.centralLine.getMagnitude();
    } // end for track segment loop


    return Float.NaN;
  }










  PVector getStartPosition() {
    PVector p0 = PVector.mult( PVector.add( trackSegments[0].leftLine.getP0(), trackSegments[0].rightLine.getP0() ), 0.5 );
    PVector p1 = PVector.mult( PVector.add( trackSegments[0].leftLine.getP1(), trackSegments[0].rightLine.getP1() ), 0.5 );
    return PVector.add( PVector.mult(p0, 0.8), PVector.mult(p1, 0.2) );
  }


  float getStartDirection() {
    return PVector.angleBetween(
      PVector.sub( trackSegments[0].centralLine.getP1(), trackSegments[0].centralLine.getP0() ), 
      new PVector(0, -1));
  }

  Line getStartLine() {
    if (trackSegments.length>0)
      return new Line( trackSegments[0].leftLine.getP0(), trackSegments[0].rightLine.getP0() );
    else
      return null;
  }







  boolean checkBoundaryCrossing(PVector p0, PVector p1) {
    Line movement = new Line(p0, p1);
    for (TrackSegment segment : trackSegments)
      if (lineSegmentsIntersect(movement, segment.leftLine) || lineSegmentsIntersect(movement, segment.rightLine))
        return true;
    return false;
  }







  void draw() {
    strokeWeight(1);
    if (makePlayfield) {

      // draw cursor
      stroke(0);
      line(mouseX, mouseY-20, mouseX, mouseY-5);
      line(mouseX, mouseY+20, mouseX, mouseY+5);
      line(mouseX-20, mouseY, mouseX-5, mouseY);
      line(mouseX+20, mouseY, mouseX+5, mouseY);

      //move last point
      if (trackSegments.length>0) {
        trackSegments[trackSegments.length-1].centralLine.setP1( new PVector(mouseX, mouseY) );
      }
      //if (centralLines.length
    }

    for (int i=0; i<trackSegments.length; i++) {
      trackSegments[i].draw();
    }

    stroke(64);
    if (trackSegments.length>0) {
      line(trackSegments[0].leftLine.getP0().x, trackSegments[0].leftLine.getP0().y, 
        trackSegments[0].rightLine.getP0().x, trackSegments[0].rightLine.getP0().y);
    }
  }
}





class Line {

  private final PVector _p0, _p1;
  private PVector _normal;
  private float _heading;
  private float _magnitude;

  Line(PVector p0, PVector p1) {
    this._p0=p0;
    this._p1=p1;
    calcHeadingAndMagnitude();
  }

  private void calcHeadingAndMagnitude() {
    PVector dir = PVector.sub(_p1, _p0);
    _heading = dir.heading();
    _magnitude = dir.mag();
    _normal = dir.copy();
    _normal.rotate(HALF_PI);
    _normal.normalize();
  }

  void setP0(PVector p) {
    _p0.set(p);
    calcHeadingAndMagnitude();
  }

  void setP1(PVector p) {
    _p1.set(p);
    calcHeadingAndMagnitude();
  }

  PVector getP0() {
    return _p0;
  }
  PVector getP1() {
    return _p1;
  }
  PVector getNormal() {
    return _normal;
  }

  float getHeading() {
    return _heading;
  }
  float getMagnitude() {
    return _magnitude;
  }

  float calcPercentageFromX(float x) {
    return (x-_p0.x)/(_p1.x-_p0.x);
  }


  void draw() {
    line(_p0.x, _p0.y, _p1.x, _p1.y);
  }


  String toString() {
    return "Line ("+_p0.toString()+", "+_p1.toString()+")";
  }
}






class TrackSegment {

  boolean debugDrawFlag = false;
  Line leftLine, centralLine, rightLine;

  TrackSegment() {
  }

  float getLength() {
    return centralLine.getMagnitude();
  }

  void draw() {

    if (debugDrawFlag) {
      noStroke();
      fill(255, 128, 128);
      beginShape(QUADS);
      PVector p0 = leftLine.getP0();
      PVector p1 = leftLine.getP1();
      PVector p2 = rightLine.getP1();
      PVector p3 = rightLine.getP0();
      vertex(p0.x, p0.y);
      vertex(p1.x, p1.y);
      vertex(p2.x, p2.y);
      vertex(p3.x, p3.y);
      endShape();
      debugDrawFlag = false;
    }

    //println("Drawing "+centralLine);
    stroke(0);
    leftLine.draw();
    rightLine.draw();
    stroke(128);
    centralLine.draw();
  }
}
