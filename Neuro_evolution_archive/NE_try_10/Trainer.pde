import java.util.Comparator;  

class Trainer {

  final float MINIMUM_DISTANCE_TO_TRAVEL = 5;
  final float idleTimeMaximum;
  final float idleTimeEnd;
  final float minimumAverageSpeed;

  float totalTime;
  float endTime;
  boolean endTraining = false;
  boolean finalEnd = false;
  int currentRound = 0;
  int maxRounds = 0;

  Network currentNetwork;
  Vehicle currentVehicle;

  ArrayList<TrainerResults> results = new ArrayList<TrainerResults>();
  //TrainerResults currentResults;

  Trainer(float idleTimeMaximum, float idleTimeEnd, float minimumAverageSpeed) {
    this.idleTimeMaximum = idleTimeMaximum;
    this.idleTimeEnd = idleTimeEnd;
    this.minimumAverageSpeed = minimumAverageSpeed;
  }

  void reset() {
    totalTime = 0;
    endTraining = false;
    endTime = 0;
    finalEnd = false;
    currentRound = 0;
    results.clear();
  }


  void nextRound(Vehicle vehicle, Network network) {
    currentVehicle = vehicle;
    currentNetwork = network;
    totalTime = 0;
    endTraining = false;
    endTime = 0;
    finalEnd = false;
    currentRound++;
  }



  void integrate(PlayField field, int genIndex, float frameTime) {
    totalTime += frameTime;

    if (!endTraining) {
      //println(vehicle.stats.totalDistanceSinceStart);
      //currentResults.totalDistance = vehicle.stats.totalDistanceSinceStart;
    }

    // check average speed
    float av = currentVehicle.stats.averageSpeed;

    if (!endTraining && !Float.isNaN(av) && av<minimumAverageSpeed) {
      doEndTraining(genIndex, "Slower than average speed of "+minimumAverageSpeed);
    }

    // check idle
    else if (!endTraining && currentVehicle.stats.getTotalCourseDistance()<MINIMUM_DISTANCE_TO_TRAVEL && totalTime>idleTimeMaximum) {
      doEndTraining(genIndex, "Did not travel enougth ("+MINIMUM_DISTANCE_TO_TRAVEL+") within ("+idleTimeMaximum+") seconds.");
    } 

    // check full laps
    else if (!endTraining && currentVehicle.stats.getTotalCourseDistance() > LAPS_FOR_SUCCESS * field.totalDistance) {
      doEndTraining(genIndex, "WINNER! Travelled for more than"+LAPS_FOR_SUCCESS+" laps!!");
    }

    // raceGame.vehicle.dead
    else if (!endTraining && raceGame.vehicle.dead) {
      doEndTraining(genIndex, "Vehicle is dead");
    }

    // check end
    if (endTraining && totalTime > endTime+idleTimeEnd) {
      //println("Results: "+results.size()+" (TODO sort by totalDistance)");
      finalEnd = true;
    }
  }


  void doEndTraining(int genIndex, String reason) {
    if (!endTraining) {


      String identifier = "Candidate g"+nf(genIndex, 3)+" "+nf(results.size()+1, 3);

      NetworkWeights networkWeights = new NetworkWeights(currentNetwork);
      float totalDistance = currentVehicle.stats.getTotalCourseDistance();
      TrainerResults result = new TrainerResults(identifier, reason, networkWeights, totalDistance, totalTime);
      //println(result.toSavableString());   

      results.add(result);
      endTraining = true;
      endTime = totalTime;
    }
  }


  void sortResults() {
    results.sort( new Comparator<TrainerResults>() {
      public int compare(TrainerResults r0, TrainerResults r1) {
        if (r0.totalDistance>r1.totalDistance) return -1;
        else if (r0.totalDistance<r1.totalDistance) return 1;
        else return 0;
      }
    } 
    );
  }

  void saveAllResults() {
    saveAllResults(null);
  }

  void saveAllResults(String folderName) {
    String NL = System.getProperty("line.separator");
    for (int i=0; i<results.size(); i++) {
      TrainerResults result = results.get(i); 
      String ranking = nf(i, 3);
      if (folderName==null) {
        String dateTimeString = nf(year(), 4)+"_"+nf(month()+1, 2)+"_"+nf(day(), 2)+"_"+nf(hour(), 2)+"_"+nf(minute(), 2)+"_"+nf(second(), 2);
        folderName = "Trainer results "+dateTimeString+"/";
      }
      String fileName = ranking+" "+result.identifier+" totalDistance "+(int)result.totalDistance+".txt";
      String fullPath = folderName+fileName;
      //println("Saving "+fullPath);
      saveStrings(fullPath, result.toSavableString().split(NL));
    }
  }
}







class TrainerResults {

  final String NL = System.getProperty("line.separator");
  final String PROPERTY_SEPARATOR = NL+"#"+NL;

  final String identifier;
  final String endReason;
  final NetworkWeights networkWeights;
  final float totalDistance;
  final float totalTime;
  int age = 0;

  TrainerResults(String identifier, String endReason, NetworkWeights networkWeights, float totalDistance, float totalTime) {
    this.identifier = identifier;
    this.endReason = endReason;
    this.networkWeights = networkWeights;
    this.totalDistance = totalDistance;
    this.totalTime = totalTime;
  }

  TrainerResults(String[] fileLines) {

    String identifier="";
    String endReason="";
    float totalDistance=0;
    float totalTime=0;
    NetworkWeights networkWeights = null;

    int valuesSet = 0;
    // make one string
    StringBuilder sb = new StringBuilder();
    for (String line : fileLines) {
      sb.append(line);
      sb.append(NL);
    }
    String allLines = sb.toString();

    String[] propertyChunks = allLines.split(PROPERTY_SEPARATOR);
    for (String propertyChunk : propertyChunks) {
      String[] keyVals = propertyChunk.split(NL);
      if (keyVals.length<2) {
        throw new RuntimeException("Error loading file: "+NL+allLines);
      } else {
        String theKey = keyVals[0];
        String[] theVals = getVals(keyVals);
        switch (theKey) {
          case "name" : identifier = theVals[0]; valuesSet++; break;
          case "endReason" : endReason = theVals[0]; valuesSet++; break;
          case "totalDistance" : totalDistance = Float.parseFloat(theVals[0]); valuesSet++; break;
          case "totalTime" : totalTime = Float.parseFloat(theVals[0]); valuesSet++; break;
          case "networkWeights" : networkWeights = new NetworkWeights(theVals); valuesSet++; break;
          default:
            //System.err.println("Unimplemented key "+theKey);
            break;
        }
      }
    }

    //
    this.identifier = identifier;
    this.endReason = endReason;
    this.networkWeights = networkWeights;
    this.totalDistance = totalDistance;
    this.totalTime = totalTime;
    
    if (valuesSet<5) {
      throw new RuntimeException("BAIL, only "+valuesSet+" value(s) set");
    }
  }

  String[] getVals(String[] keyVals) {
    String[] theVals = new String[keyVals.length-1];
    System.arraycopy(keyVals, 1, theVals, 0, theVals.length);
    return theVals;
  }

  String toSavableString() {

    StringBuilder sb = new StringBuilder();

    sb.append("name");
    sb.append(NL);
    sb.append(identifier);
    sb.append(PROPERTY_SEPARATOR);

    sb.append("endReason");
    sb.append(NL);
    sb.append(endReason);
    sb.append(PROPERTY_SEPARATOR);

    sb.append("totalDistance");
    sb.append(NL);
    sb.append(totalDistance);
    sb.append(PROPERTY_SEPARATOR);

    sb.append("totalTime");
    sb.append(NL);
    sb.append(totalTime);
    sb.append(PROPERTY_SEPARATOR);

    sb.append("networkWeights");
    sb.append(NL);
    sb.append(networkWeights.toSavableString());
    return sb.toString();
  }

  //public static Comparator<TrainerResults> distanceComparator = new Comparator<TrainerResults>() {
  //  public int compare(TrainerResults r0, TrainerResults r1) {
  //    if (r0.totalDistance<r1.totalDistance) return -1;
  //    else if (r0.totalDistance>r1.totalDistance) return 1;
  //    else return 0;
  //  }
  //}
}
