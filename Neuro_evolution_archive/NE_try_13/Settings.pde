// ================================================ BRAIN

float INPUT_NEURON_AMOUNT_TO_HIDDEN_NEURON_AMOUNT_FACTOR = 2;
//float OUTPUT_NEURON_AMOUNT_TO_HIDDEN_NEURON_AMOUNT_FACTOR = 1.5;
boolean ADD_BIAS_CONNECTION = false;
float BIAS = 1;

// ================================================ GENETICS

float RANDOM_WEIGHT_VALUE = 0;

float MUTATION_CHANCE_PER_WEIGHT = 0.015; // 0.05 0.1
float MUTATION_RANGE = 1.0; // 0.05 0.2 1.0

float FOR_FITNESS_MAX_AVERAGE_SPEED = 150;
//float FOR_FITNESS_START_SPEED_INFLUENCE_ON_DISTANCE_PERCENTAGE = 0.05;

// ================================================ VEHICLE

int VEHICLE_SENSORS = 7;
float VEHICLE_SENSOR_DEGREES = 60;
float VEHICLE_SIZE_TO_SENSOR_FAC = 5.0;
float VEHICLE_SIZE_TO_CENTRAL_SENSOR_FAC = 6.0;

// ================================================ DRAWING

int FPS = 300;
float DRAW_NEURON_DIAMETER = 10;
float DRAW_NEURON_SPACE_X = 100;
float DRAW_NEURON_SPACE_Y = 30;

float DRAW_NEURON_WEIGHT_TO_STROKE_WEIGHT = 5;
float DRAW_NEURON_STROKE_WEIGHT_MAX = 8;
