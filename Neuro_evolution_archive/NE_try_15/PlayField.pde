


/*

 
 FIELD
 
 
 */

import java.util.Arrays;


class PlayField {

  boolean makePlayfield = false;
  //Line[] centralLines;
  //Line[] leftLines, rightLines;
  final float roadWidth = 20;

  TrackSegment[] trackSegments;

  float totalDistance = 0;

  float[] FIELD1 = new float[] {
    88, 96, 122, 76, 206, 69, 429, 111, 478, 253, 467, 416, 413, 469, 381, 452, 378, 394, 395, 337, 
    392, 278, 381, 220, 362, 185, 328, 179, 293, 200, 302, 321, 304, 421, 289, 449, 254, 459, 152, 461, 
    92, 449, 81, 361, 100, 331, 165, 301, 182, 271, 146, 214, 125, 201, 77, 178, 72, 128};

  float[] FIELD2_ZIG_ZAG = new float[] {
    106, 117, 141, 119, 187, 135, 245, 128, 288, 88, 336, 93, 357, 134, 391, 158, 430, 138, 471, 167, 
    485, 220, 466, 276, 422, 297, 381, 288, 379, 237, 323, 223, 292, 288, 308, 340, 329, 352, 339, 402, 
    315, 451, 219, 467, 145, 466, 132, 438, 163, 393, 226, 376, 233, 325, 195, 293, 85, 303, 67, 282, 
    53, 195, 86, 138};

  float[] FIELD3_SMALL_ZIG_ZAG = new float[] {
    100, 127, 145, 115, 172, 88, 211, 71, 247, 80, 274, 107, 289, 125, 315, 126, 346, 115, 365, 105, 
    411, 100, 434, 110, 450, 134, 455, 185, 449, 211, 433, 246, 403, 253, 370, 249, 350, 230, 325, 211, 
    295, 216, 275, 239, 251, 277, 226, 299, 187, 310, 160, 306, 148, 290, 130, 274, 102, 270, 75, 263, 
    58, 243, 50, 213, 53, 182, 64, 154, 74, 146};

  float[] FIELD4_RACETRACK1 = new float[] {
    249, 281, 312, 280, 333, 263, 330, 247, 316, 232, 290, 229, 256, 229, 206, 236, 170, 229, 144, 208, 
    145, 184, 161, 168, 185, 160, 197, 160, 208, 156, 219, 145, 223, 134, 226, 120, 238, 88, 306, 42, 
    374, 47, 393, 79, 365, 104, 306, 115, 300, 146, 325, 162, 372, 177, 402, 195, 410, 266, 412, 346, 
    408, 428, 404, 474, 366, 522, 335, 545, 85, 544, 59, 522, 62, 501, 101, 482, 206, 481, 313, 469, 
    345, 415, 332, 357, 284, 365, 235, 395, 157, 427, 68, 403, 42, 342, 64, 293, 126, 284};

  float[] FIELD5_WICKED = new float[] {
    291, 277, 389, 277, 391, 202, 307, 195, 305, 119, 346, 95, 411, 108, 463, 141, 472, 215, 462, 288, 
    454, 335, 409, 414, 332, 495, 200, 537, 108, 503, 30, 389, 19, 261, 25, 188, 46, 114, 106, 63, 
    157, 53, 197, 63, 209, 88, 191, 128, 149, 167, 116, 220, 106, 297, 112, 373, 156, 441, 200, 449, 
    204, 449, 238, 438, 250, 402, 203, 341, 211, 279, 251, 266, 268, 276};

  //  float[] FIELD6_GRAND_SPEEDWAY = new float[] {
  //    627, 391, 606, 383, 578, 381, 562, 394, 535, 412, 510, 406, 481, 385, 467, 358, 432, 351, 402, 367, 
  //    393, 392, 400, 433, 388, 462, 347, 463, 324, 443, 308, 379, 334, 302, 406, 270, 485, 276, 569, 269, 
  //    590, 233, 589, 176, 563, 133, 475, 102, 304, 96, 195, 119, 115, 197, 101, 274, 110, 308, 176, 326, 
  //    233, 378, 239, 447, 202, 508, 133, 583, 152, 662, 245, 711, 326, 731, 448, 742, 476, 728, 488, 701, 
  //    485, 694, 480, 686, 448, 671, 388, 672, 320, 655, 270, 620, 268, 585, 281, 563, 323, 556, 367, 556, 
  //    446, 573, 530, 612, 586, 627, 605, 614, 602, 586, 566, 550, 565, 530, 601, 499, 649, 471, 663, 460, 
  //    664, 425};

  float[] FIELD6_GRAND_SPEEDWAY = new float[] {
    564, 301, 545, 294, 520, 292, 505, 304, 481, 320, 459, 315, 432, 296, 420, 272, 388, 265, 361, 280, 
    353, 302, 360, 339, 349, 365, 312, 366, 291, 348, 277, 291, 300, 221, 365, 193, 436, 198, 512, 192, 
    531, 159, 530, 108, 506, 69, 427, 41, 273, 36, 175, 57, 103, 127, 90, 196, 99, 227, 158, 243, 
    209, 290, 215, 352, 181, 407, 119, 474, 136, 545, 220, 589, 293, 607, 403, 617, 428, 605, 439, 580, 
    436, 574, 432, 567, 403, 553, 349, 554, 288, 539, 243, 508, 241, 476, 252, 456, 290, 450, 330, 450, 
    401, 465, 477, 500, 527, 514, 544, 502, 541, 477, 509, 445, 508, 427, 540, 399, 584, 373, 596, 364, 
    597, 332};

  float[] FIELD7_BIGGY = new float[] {
    456, 314, 492, 314, 497, 274, 442, 263, 444, 210, 553, 202, 549, 329, 608, 331, 618, 138, 392, 149, 
    298, 353, 231, 383, 192, 357, 179, 310, 275, 149, 372, 96, 521, 61, 673, 72, 702, 100, 701, 134, 
    683, 392, 668, 413, 669, 450, 718, 482, 745, 515, 733, 561, 686, 586, 627, 586, 93, 564, 58, 542, 
    56, 506, 91, 492, 152, 493, 572, 524, 607, 516, 596, 470, 567, 433, 449, 459, 337, 440, 356, 397, 
    409, 396, 415, 362, 390, 321, 407, 304, 428, 305};

    PlayField() {
      float[] points = FIELD2_ZIG_ZAG;

  //float scale = 0.9;
  //for (int i=0; i<points.length; i++) {
  //  points[i] *= scale;
  //  if (i%2==1) points[i]-=50;
  //  points[i] = (int)points[i];
  //}
  //printPointsArray(points);

  createLines(true, points);
}

void initMakePlayfield() {
  makePlayfield = true;
  trackSegments = new TrackSegment[0];
}



void addPoint(float x, float y) {
  //println("addPoint()");
  //println("  ",x,y);
  if (trackSegments.length==0) {
    createLines(false, x, y, x, y);
  } else {
    ArrayList<PVector> points = new ArrayList<PVector>();
    for (int i=0; i<trackSegments.length; i++) {
      points.add(trackSegments[i].centralLine.getP0());
    }
    points.add(trackSegments[trackSegments.length-1].centralLine.getP1());
    points.add( new PVector(x, y) );
    //println("  current points: ");
    //println("    ",points);
    makeLines(points, false);
  }
}



void createLines(boolean closeLoop, float... xyPairs) {
  //println("createLines()");
  //println(xyPairs);
  ArrayList<PVector> points = new ArrayList<PVector>();
  for (int i=0; i<xyPairs.length; i+=2) {
    points.add( new PVector(xyPairs[i], xyPairs[i+1]) );
  }
  makeLines(points, closeLoop);
}



void finalize() {
  ArrayList<PVector> points = new ArrayList<PVector>();
  for (int i=0; i<trackSegments.length; i++) {
    points.add(trackSegments[i].centralLine.getP0());
  }
  printPointsArray(points);
  makeLines(points, true);
  makePlayfield = false;
}

void printPointsArray(float... xyPairs) {
  ArrayList<PVector> points = new ArrayList<PVector>();
  for (int i=0; i<xyPairs.length; i+=2) {
    points.add( new PVector(xyPairs[i], xyPairs[i+1]) );
  }
  printPointsArray(points);
}

void printPointsArray(ArrayList<PVector> points) {
  print("float[] xyPoints = new float[] {");
  for (int i=0; i<points.size(); i++) {
    if (i%10==0) {
      println();
    }
    print("   "+int(points.get(i).x)+", "+int(points.get(i).y));
    if (i<points.size()-1) print(",");
  }
  print("};");
}



void makeLines(ArrayList<PVector> points, boolean closeLoop) {
  //println("makeLines()");
  //println("  ",points);
  trackSegments = new TrackSegment[closeLoop ? points.size() : points.size()-1];
  totalDistance = 0;

  int amount = closeLoop ? points.size() : points.size()-1;
  for (int i=0; i<amount; i++) {
    trackSegments[i] = new TrackSegment();
    PVector p0 = points.get(i);
    int i1 = (i+1)%(points.size());
    PVector p1 = points.get( i1 );
    trackSegments[i].centralLine = new Line(p0, p1);
    totalDistance += trackSegments[i].centralLine.getMagnitude();
  }
  //println(trackSegments);
  makeSideLines(trackSegments, roadWidth);
}



void makeSideLines(TrackSegment[] trackSegments, float wid) {


  for (int i=0; i<trackSegments.length; i++) {
    Line line = trackSegments[i].centralLine;
    PVector dir = PVector.sub(line.getP1(), line.getP0());
    PVector dirLeft = dir.copy();
    dirLeft.rotate(radians(-90));
    dirLeft.setMag( wid );
    trackSegments[i].leftLine = new Line( PVector.add(line.getP0(), dirLeft), PVector.add(line.getP1(), dirLeft) );
    PVector dirRight = dir.copy();
    dirRight.rotate(radians(90));
    dirRight.setMag( wid );
    trackSegments[i].rightLine = new Line( PVector.add(line.getP0(), dirRight), PVector.add(line.getP1(), dirRight) );
  }

  // now adjust all left and right lines for intersections
  //Line[][] leftAndRight = new Line[][] { leftLines, rightLines };
  //for (Line[] sideLines : leftAndRight) {
  //for (TrackSegment segment:trackSegments) {
  for (int left=0; left<2; left++) {

    for (int i=0; i<trackSegments.length; i++) {

      int index0 = i;
      int index1 = i<trackSegments.length-1 ? i+1 : 0;

      Line l0 = left==0 ? trackSegments[index0].leftLine : trackSegments[index0].rightLine;
      Line l1 = left==0 ? trackSegments[index1].leftLine : trackSegments[index1].rightLine;

      PVector intersectionPoint = lineLineIntersection(l0.getP0(), l0.getP1(), l1.getP0(), l1.getP1());
      if (intersectionPoint!=null) {
        l0.setP1( intersectionPoint );
        l1.setP0( intersectionPoint );
      }
    }
  }
}



float getCourseDistance(Vehicle vehicle) {

  PVector P = vehicle.position;
  float totalDistance = 0;

  for (int i=0; i<trackSegments.length; i++) {
    TrackSegment segment = trackSegments[i];

    // in Segment AB CD, find line GH that is parallel to AB and CD that goes through P
    // then find percentage of P on GH, meaning ratio GP:GH
    PVector A = segment.rightLine.getP0();
    PVector B = segment.rightLine.getP1();
    PVector C = segment.leftLine.getP0();
    PVector D = segment.leftLine.getP1();


    if (B.x-A.x==0) {
      System.err.println("TODO: infinite inclination on segment "+i+"("+segment.centralLine.getP1()+","+segment.centralLine.getP0()+"), use reverse?");
    } else {
      float inclinationAB = (B.y-A.y)/(B.x-A.x);
      // GH: line with same inclination as AB that goes through the point P
      // GH: y = (x-P.x) * inclinationAB + P.y
      PVector GH0 = new PVector(0, (0-P.x)*inclinationAB + P.y);
      PVector GH1 = new PVector(width, (width-P.x)*inclinationAB + P.y);

      // find G: intersection AC with GH
      // find H: intersection BD with GH
      boolean hasG = lineSegmentsIntersect(A, C, GH0, GH1);
      boolean hasH = lineSegmentsIntersect(B, D, GH0, GH1);
      if (hasG && hasH) {
        PVector G = lineLineIntersection(A, C, GH0, GH1);
        PVector H = lineLineIntersection(B, D, GH0, GH1);
        // now find percentage of P on line GH
        Line GH = new Line(G, H);
        float segmentPercentage = GH.calcPercentageFromX(P.x);
        if (segmentPercentage>=0 && segmentPercentage<1) {
          totalDistance += segment.centralLine.getMagnitude() * segmentPercentage;
          return totalDistance;
        }
      }
    }
    totalDistance += segment.centralLine.getMagnitude();
  } // end for track segment loop


  return Float.NaN;
}










PVector getStartPosition() {
  PVector p0 = PVector.mult( PVector.add( trackSegments[0].leftLine.getP0(), trackSegments[0].rightLine.getP0() ), 0.5 );
  PVector p1 = PVector.mult( PVector.add( trackSegments[0].leftLine.getP1(), trackSegments[0].rightLine.getP1() ), 0.5 );
  return PVector.add( PVector.mult(p0, 0.8), PVector.mult(p1, 0.2) );
}


float getStartDirection() {
  return trackSegments[0].centralLine.getHeading()+HALF_PI;
}

Line getStartLine() {
  if (trackSegments.length>0)
    return new Line( trackSegments[0].leftLine.getP0(), trackSegments[0].rightLine.getP0() );
  else
    return null;
}







boolean checkBoundaryCrossing(PVector p0, PVector p1) {
  Line movement = new Line(p0, p1);
  for (TrackSegment segment : trackSegments)
    if (lineSegmentsIntersect(movement, segment.leftLine) || lineSegmentsIntersect(movement, segment.rightLine))
      return true;
  return false;
}







void draw() {
  strokeWeight(1);
  if (makePlayfield) {

    // draw cursor
    stroke(0);
    line(mouseX, mouseY-20, mouseX, mouseY-5);
    line(mouseX, mouseY+20, mouseX, mouseY+5);
    line(mouseX-20, mouseY, mouseX-5, mouseY);
    line(mouseX+20, mouseY, mouseX+5, mouseY);

    //move last point
    if (trackSegments.length>0) {
      trackSegments[trackSegments.length-1].centralLine.setP1( new PVector(mouseX, mouseY) );
    }
    //if (centralLines.length
  }

  for (int i=0; i<trackSegments.length; i++) {
    trackSegments[i].draw();
  }

  stroke(64);
  if (trackSegments.length>0) {
    line(trackSegments[0].leftLine.getP0().x, trackSegments[0].leftLine.getP0().y, 
      trackSegments[0].rightLine.getP0().x, trackSegments[0].rightLine.getP0().y);
  }
}
}





class Line {

  private final PVector _p0, _p1;
  private PVector _normal;
  private float _heading;
  private float _magnitude;

  Line(PVector p0, PVector p1) {
    this._p0=p0;
    this._p1=p1;
    calcHeadingAndMagnitude();
  }

  private void calcHeadingAndMagnitude() {
    PVector dir = PVector.sub(_p1, _p0);
    _heading = dir.heading();
    _magnitude = dir.mag();
    _normal = dir.copy();
    _normal.rotate(HALF_PI);
    _normal.normalize();
  }

  void setP0(PVector p) {
    _p0.set(p);
    calcHeadingAndMagnitude();
  }

  void setP1(PVector p) {
    _p1.set(p);
    calcHeadingAndMagnitude();
  }

  PVector getP0() {
    return _p0;
  }
  PVector getP1() {
    return _p1;
  }
  PVector getNormal() {
    return _normal;
  }

  float getHeading() {
    return _heading;
  }

  float getMagnitude() {
    return _magnitude;
  }

  float calcPercentageFromX(float x) {
    return (x-_p0.x)/(_p1.x-_p0.x);
  }


  void draw() {
    line(_p0.x, _p0.y, _p1.x, _p1.y);
  }


  String toString() {
    return "Line ("+_p0.toString()+", "+_p1.toString()+")";
  }
}






class TrackSegment {

  boolean debugDrawFlag = false;
  Line leftLine, centralLine, rightLine;

  TrackSegment() {
  }

  float getLength() {
    return centralLine.getMagnitude();
  }

  void draw() {

    if (debugDrawFlag) {
      noStroke();
      fill(255, 128, 128);
      beginShape(QUADS);
      PVector p0 = leftLine.getP0();
      PVector p1 = leftLine.getP1();
      PVector p2 = rightLine.getP1();
      PVector p3 = rightLine.getP0();
      vertex(p0.x, p0.y);
      vertex(p1.x, p1.y);
      vertex(p2.x, p2.y);
      vertex(p3.x, p3.y);
      endShape();
      debugDrawFlag = false;
    }

    //println("Drawing "+centralLine);
    stroke(0);
    leftLine.draw();
    rightLine.draw();
    stroke(128);
    centralLine.draw();
  }
}
