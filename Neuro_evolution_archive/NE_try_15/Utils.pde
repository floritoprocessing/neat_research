import java.util.Collections;

String[] getFileList(String processingFilePath) {
  
  String absoluteFilePath = sketchPath(processingFilePath);
  //println(filePath);
  
  File folder = new File(absoluteFilePath);
  if (!folder.isDirectory()) {
    throw new RuntimeException("Sorry, folder "+absoluteFilePath+" does not exist");
  } else {
    
    // get all filenames
    ArrayList<String> fileNames = new ArrayList<String>();
    File[] files = folder.listFiles();
    for (File file:files) {
      if (file.isFile()) fileNames.add(file.getName());
    }
    
    Collections.sort(fileNames);
    return fileNames.toArray(new String[0]);
  }
  
  //return null;
}
