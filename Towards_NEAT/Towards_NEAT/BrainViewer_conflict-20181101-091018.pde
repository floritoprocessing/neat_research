class BrainViewer {

  float nodeSize = 20;
  float totalWidth = 300;
  float totalHeight = 170;
  float CONNECTION_TEXT_RIGHT = 4;

  int CONNECTION_STROKE = 0xff000000;
  int CONNECTION_TEXT = 0xff000000;
  int CONNECTION_TEXT_BG = 0x44ffffff;

  int NODE_STROKE = 0xff000000;
  int NODE_FILL = 0xffcccccc;
  int NODE_TEXT = 0xff000000;

  long seed;
  
  BrainViewer() {
    seed = System.currentTimeMillis();
  }

  void draw(Brain brain, boolean nodeText, boolean weightText) {

    randomSeed(seed); 
    strokeWeight(1);



    int layerCount = brain.feedForwardOrder.size();
    HashMap<Node, PVector> nodePositions = new HashMap<Node, PVector>();
    for (int l=0; l<layerCount; l++) {
      float x = map(l, 0, layerCount-1, 0, totalWidth);

      ArrayList<Node> layer = brain.feedForwardOrder.get(l);

      int nodeCount = layer.size();
      float y0 = 0;
      float y1 = totalHeight;
      if (l>0 && l<layerCount-1) {
        float off = (random(1.0)<0.5?-1:1)*random(0.05, 0.3)*totalHeight;
        y0=off;
        y1=totalHeight-off;
      }
      for (int i=0; i<nodeCount; i++) {
        Node n = layer.get(i);      
        float y = nodeCount==1 ? totalHeight/2 : map(i, 0, nodeCount-1, y0, y1);
        nodePositions.put( n, new PVector(x, y) );
      }
    }

    // draw connections

    textAlign(CENTER, CENTER);
    for (Connection connection : brain.connections) {
      PVector p0 = nodePositions.get(connection.input);
      PVector p1 = nodePositions.get(connection.output);
      stroke(CONNECTION_STROKE);
      line(p0.x, p0.y, -1, p1.x, p1.y, -1);
      if (weightText) {
        String wt = nf(connection.info.weight, 1, 4);
        pushMatrix();
        translate( (p0.x*1+p1.x*CONNECTION_TEXT_RIGHT)/(CONNECTION_TEXT_RIGHT+1), (p0.y*1+p1.y*CONNECTION_TEXT_RIGHT)/(CONNECTION_TEXT_RIGHT+1) );
        float rotZ = atan2(p1.y-p0.y, p1.x-p0.x);
        rotate(rotZ);
        float rw = textWidth(wt);
        float rh = textAscent()+textDescent();
        noStroke();
        fill(CONNECTION_TEXT_BG&0xffffff, CONNECTION_TEXT_BG>>24);

        rect(-rw/2, -rh/2, rw, rh);
        fill(CONNECTION_TEXT);
        text(wt, 0, 0);
        popMatrix();
      }
    }

    // draw nodes
    textAlign(CENTER, CENTER);
    ellipseMode(CENTER);
    stroke(NODE_STROKE);
    for (Node node : brain.nodes) {
      PVector p = nodePositions.get(node);
      fill(NODE_FILL);
      ellipse(p.x, p.y, nodeSize, nodeSize);
      if (nodeText) {
        fill(NODE_TEXT);
        text(node.info.id, p.x, p.y);
      }
    }

    randomSeed(System.currentTimeMillis());
  }
}
