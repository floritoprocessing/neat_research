class Connection {
  
  ConnectionModel info;
  
  Node input = null;
  Node output = null;
  
  Connection(ConnectionModel info) {
    this.info = info;
  }
  
  String toString() {
    return info.toString();
  }
  
}
