class ConnectionModel {

  final int innovation;
  final int inputNodeId;
  final int outputNodeId;
  final float weight;
  final boolean enabled;


  ConnectionModel(int innovation, int inputNodeId, int outputNodeId, float weight, boolean enabled) {
    this.innovation = innovation;
    this.inputNodeId = inputNodeId;
    this.outputNodeId = outputNodeId;
    this.weight = weight;
    this.enabled = enabled;
  }
  
  ConnectionModel cloneWithNewWeight(float newWeight) {
    return new ConnectionModel(innovation, inputNodeId, outputNodeId, newWeight, enabled);
  }
  
  ConnectionModel cloneDisabled() {
    return new ConnectionModel(innovation, inputNodeId, outputNodeId, weight, false);
  }
  
  String toString() {
    return inputNodeId+"->"+outputNodeId;
  }

  /*
  *    SERIALISATION
   */

  static final String KEY_INNOVATION = "innovation";
  static final String KEY_INPUT_NODE_ID = "inputNodeId";
  static final String KEY_OUTPUT_NODE_ID = "outputNodeId";
  static final String KEY_WEIGHT = "weight";
  static final String KEY_ENABLED = "enabled";

  ConnectionModel(JSONObject json) {
    this.innovation = json.getInt(KEY_INNOVATION);
    this.inputNodeId = json.getInt(KEY_INPUT_NODE_ID);
    this.outputNodeId = json.getInt(KEY_OUTPUT_NODE_ID);
    this.weight = json.getFloat(KEY_WEIGHT);
    this.enabled = json.getBoolean(KEY_ENABLED);
  }

  JSONObject toJSON() {
    JSONObject out = new JSONObject();
    out.setInt(KEY_INNOVATION, innovation);
    out.setInt(KEY_INPUT_NODE_ID, inputNodeId);
    out.setInt(KEY_OUTPUT_NODE_ID, outputNodeId);
    out.setFloat(KEY_WEIGHT, weight);
    out.setBoolean(KEY_ENABLED, enabled); 
    return out;
  }
}
