class NodeModel {
  
  static final int TYPE_SENSOR = 0;
  static final int TYPE_HIDDEN = 1;
  static final int TYPE_OUTPUT = 2;
  
  final int id; // also same as innovation
  final int type;
  
  NodeModel(int id, int type) {
    this.id = id;
    this.type = type;
  }
  
  String typeAsString(int t) {
    switch (t) {
      case TYPE_SENSOR: return "sensor";
      case TYPE_HIDDEN: return "hidden";
      case TYPE_OUTPUT: return "output";
      default: throw new RuntimeException("Unknown node type "+t);
    }
  }
  public String toString() {
    return "Node (id="+id+", type="+typeAsString(type)+"("+type+"))";
  }
  
  /*
  *    SERIALISATION
  */
  
  static final String KEY_ID = "id";
  static final String KEY_TYPE = "type";
  
  NodeModel(JSONObject json) {
    this.id = json.getInt(KEY_ID);
    this.type = json.getInt(KEY_TYPE);
  }
  
  JSONObject toJSON() {
    JSONObject out = new JSONObject();
    out.setInt(KEY_ID, id);
    out.setInt(KEY_TYPE, type);
    return out;
  }
  
}
