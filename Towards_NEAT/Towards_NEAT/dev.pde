/*

NEAT.stanley.ec02.pdf
3.1 Genetic Encoding (107)

MUTATION

types:

Weight mutation
- weight

Structural mutation
- Add connection
In the add connection
mutation, a single new connection gene with a random weight is added connecting
two previously unconnected nodes.

- Add node
In the add node mutation, an existing connection is split and the new node placed where the old connection used to be. 
The old connection is disabled and two new connections are added to the genome. 
The new connection leading into the new node receives a weight of 1, 
and the new connection leading out receives the same weight as the old connection.


Whenever a new gene appears (through structural mutation), 
a global innovation number is incremented and assigned to that gene. (108)

A possible problem is that the same structural innovation will receive different innovation
numbers in the same generation if it occurs by chance more than once. However,
by keeping a list of the innovations that occurred in the current generation, it
is possible to ensure that when the same structure arises more than once through independent
mutations in the same generation, each identical mutation is assigned the
same innovation number.


CROSSING OVER
(108)

When crossing over, the genes in both genomes with the same innovation numbers are lined up. (matching genes)
Genes that do not match are either disjoint (in the middle) or excess (at the end)
In composing the offspring, genes are randomly chosen from either parent at matching genes,
whereas all excess or disjoint genes are always included from the more fit parent.

*/
