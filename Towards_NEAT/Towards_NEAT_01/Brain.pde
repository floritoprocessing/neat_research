import java.util.*;

class Brain {

  float RANDOM_WEIGHT_RANGE = 0.2;
  float RANDOM_WEIGHT_MUTATION_RANGE = 0.2;

  ArrayList<Connection> connections;
  ArrayList<Node> nodes;
  ArrayList<ArrayList<Node>> feedForwardOrder;

  Brain() {
    clear();
  }

  Brain(NodeModel[] nodeInfos, ConnectionModel[] connectionInfos) {
    build(nodeInfos, connectionInfos);
  }

  void clear() {
    connections = new ArrayList<Connection>();
    nodes = new ArrayList<Node>();
    feedForwardOrder = new ArrayList<ArrayList<Node>>();
  }

  /*
  *
   *  BUILD BRAIN
   *
   */

  void build(NodeModel[] nodeInfos, ConnectionModel[] connectionInfos) {
    clear();

    for (NodeModel nodeInfo : nodeInfos) {
      Node node = new Node(nodeInfo);
      nodes.add(node);
    }

    for (ConnectionModel connectionInfo : connectionInfos) {
      Connection connection = createConnectionAndSetNodeIO(connectionInfo);
      connections.add(connection);
    }

    createFeedForwardOrder();
  }

  /*
  *  Creates the connection and sets the input, ouputs of the Connection and adds the connection to the Node inputs and outputs
   */
  Connection createConnectionAndSetNodeIO(ConnectionModel connectionInfo) {
    Node inputNode = getNodeById(connectionInfo.inputNodeId);
    Node outputNode = getNodeById(connectionInfo.outputNodeId);
    Connection connection = new Connection(connectionInfo, inputNode, outputNode);

    inputNode.outputConnections.add(connection);
    outputNode.inputConnections.add(connection);
    return connection;
  }


  Node getNodeById(int id) {
    for (Node node : nodes) {
      if (node.info.id==id) return node;
    }
    throw new RuntimeException("Node id="+id+" not found");
  }

  ArrayList<Node> getNodesByType(int type) {
    ArrayList<Node> out = new ArrayList<Node>();
    for (Node node : nodes) {
      if (node.info.type==type) out.add(node);
    }
    return out;
  }







  void createFeedForwardOrder() {
    boolean DEBUG = true;
    if (DEBUG) println("BEGIN createFeedForwardOrder()");

    NodeDependancyTree tree = buildDependancyTree();
    if (DEBUG) println("  dependancy tree: "+tree);

    if (DEBUG) println("END createFeedForwardOrder");
  }


  NodeDependancyTree buildDependancyTree() {
    NodeDependancyTree tree = new NodeDependancyTree();

    // add sensor nodes
    tree.addSensors( getNodesByType(NodeModel.TYPE_SENSOR) );

    // add nodes that only contain the sensors in the tree
    tree.findAndAddDependantNodes( );


    return tree;
  }


  class NodeDependancyTree extends ArrayList<NodeDependancy> {
    ArrayList<Integer> nodeIds = new ArrayList<Integer>();
    void addSensors( ArrayList<Node> nodes ) {
      for (Node node : nodes) {
        addNode(node);
      }
    }
    void addNodeDependancy( NodeDependancy nd) {
      add(nd);
      nodeIds.add( nd.node.info.id );
    }
    void addNode( Node node ) {
      add( new NodeDependancy( this, node ) );
      nodeIds.add( node.info.id );
    }
    NodeDependancy getDependancyWithNodeId(int id) {
      for (NodeDependancy nd : this) {
        if (nd.node.info.id==id) return nd;
      }
      throw new RuntimeException("not found!");
    }
    // finds a node that has as an input node only the existing nodes in the tree
    void findAndAddDependantNodes() {
      for (Node node : nodes) {
        //println("findAndAddDependantNodes for "+node.info.id);
        boolean skip = nodeIds.contains( node.info.id );
        if (!skip) {

          NodeDependancy nodeDependancy = new NodeDependancy(this, node);
          boolean onlyRegisteredNodes = true;
          for (Connection connection : node.inputConnections) {
            Node inputNode = connection.input;
            boolean isRegistered = nodeIds.contains( inputNode.info.id );
            if (isRegistered) {
              nodeDependancy.addDependantNodeOnlyOnce( getDependancyWithNodeId( inputNode.info.id ) );
            }
            onlyRegisteredNodes = onlyRegisteredNodes && isRegistered;
          }

          if (onlyRegisteredNodes) {
            addNodeDependancy(nodeDependancy);
          }
        }
        //println("result: "+this);
      } // end iterate nodes
    }
  }

  class NodeDependancy {
    NodeDependancyTree tree;
    Node node;
    ArrayList<Node> dependantNodes = new ArrayList<Node>(); // nodes that depend on this one
    NodeDependancy(NodeDependancyTree tree, Node node) {
      this.tree = tree;
      this.node = node;
    }

    void addDependantNodeOnlyOnce(NodeDependancy nodeDependancy) {
      if (!dependantNodes.contains(nodeDependancy.node)) {
        //println("  adding "+nodeDependancy.node.info.id);
        dependantNodes.add(nodeDependancy.node);
        // also add not only node, but also all previous dependant nodes
        ArrayList<Node> depNodes = new ArrayList<Node>();
        nodeDependancy.getAllDependantNodesRecursivelyInTree(depNodes, "  ");
        for (Node n : depNodes) 
          if (!dependantNodes.contains(n)) 
            dependantNodes.add( n );
        //dependantNodes.addAll( depNodes );
        //println("  adding "+depNodes);
        //dependantNodes.addAll( nodeDependancy.getAllDependantNodesRecursivelyInTree() );
      }
    }

    void getAllDependantNodesRecursivelyInTree(ArrayList<Node> out, String indent) {
      // with this dep (i.e. 5: [3]) in tree, find a node dependency 3 and add more dependancies
      //println(indent+"(getDeps for "+node.info.id+"...)");
      for (NodeDependancy ndsInTree : tree) {

        Node nodeInDepTree = ndsInTree.node;
        if (node == nodeInDepTree) {
          // add unique
          
          for (Node n : ndsInTree.dependantNodes) {
            if (!out.contains(n)) out.add(n);
            NodeDependancy nd = tree.getDependancyWithNodeId( n.info.id );
            nd.getAllDependantNodesRecursivelyInTree( out, indent+"  " );
          }
        }
      }
    }

    String toString() {
      String arr="";
      for (int i=0; i<dependantNodes.size(); i++) {
        arr += dependantNodes.get(i).info.id;
        if (i<dependantNodes.size()-1) arr+=", ";
      }
      return node.info.id+": ["+arr+"]";
    }
  }








  void createFeedForwardOrder2() {
    boolean DEBUG = true;
    if (DEBUG) println("BEGIN createFeedForwardOrder()");
    feedForwardOrder.clear();


    // start at the end:
    ArrayList<Node> outputLayer = getNodesByType(NodeModel.TYPE_OUTPUT);
    feedForwardOrder.add( outputLayer );
    if (DEBUG) println("  end: "+outputLayer);
    if (DEBUG) println();

    // create a hashtable that contains nodes and their distances to the end
    ArrayList<ArrayList<NodeDistance>> allNodePaths = new ArrayList<ArrayList<NodeDistance>>();

    //recurse through connections to find paths
    if (DEBUG) println("  CREATE reverse paths per output node:");
    for (Node node : outputLayer) {

      ArrayList<NodeDistance> reverseNodeDistances = new ArrayList<NodeDistance>();
      createReversePath(node, 0, reverseNodeDistances);
      allNodePaths.add( reverseNodeDistances );
      if (DEBUG) println("  from "+node+": "+reverseNodeDistances);
    }


    // find nodes that appear with different distances:
    if (DEBUG) println();
    if (DEBUG) println("  GET NodeDistances with different distances:");

    for (Node node : nodes) {
      ArrayList<NodeDistance> distancesPerNode = new ArrayList<NodeDistance>();
      for (ArrayList<NodeDistance> paths : allNodePaths) {
        for (NodeDistance nd : paths) {
          if (nd.node==node) {

            // check if it' not the same distance (same level)
            boolean distanceExists = false;
            for (NodeDistance foundDs : distancesPerNode) {
              if (foundDs.distance==nd.distance) 
                distanceExists = true;
            }
            if (!distanceExists)
              distancesPerNode.add(nd);
          }
        }
      }

      if (distancesPerNode.size()>1) {
        if (DEBUG) println("  "+node+" appears "+distancesPerNode.size()+" times: "+distancesPerNode);
      }
    }








    //if (DEBUG) println("  all: "+allNodePaths);
    if (DEBUG) println();




    if (DEBUG) println("END createFeedForwardOrder()");
  }


  void createReversePath(Node sourceNode, int distance, ArrayList<NodeDistance> nodeOut) {
    distance++;
    ArrayList<Connection> inputConnections = sourceNode.inputConnections;
    for (Connection inputConnection : inputConnections) {

      Node inputNode = inputConnection.input;
      nodeOut.add( 0, new NodeDistance(inputNode, inputConnection, distance) );
      createReversePath(inputNode, distance, nodeOut);
    }
  }



  class NodeDistance {
    Node node;
    Connection connection;
    int distance;
    NodeDistance(Node node, Connection connection, int distance) {
      this.node=node;
      this.connection = connection;
      this.distance=distance;
    }
    NodeDistance clone() { 
      return new NodeDistance(node, connection, distance);
    }
    String toString() {
      return "("+node.info.id+")"+(connection==null?"":" "+connection.toString())+" d="+distance;
    }
  }



  //void removeFeedbackNodes(ArrayList<Node> nodes, ArrayList<ArrayList<Node>> alreadyRegisteredNodes) {
  //  for (int i=0; i<nodes.size(); i++) {    
  //    Node node = nodes.get(i);
  //    if (existsIn(alreadyRegisteredNodes, node)) {
  //      nodes.remove(i);
  //      i--;
  //    }
  //  }
  //}

  boolean existsIn(ArrayList<ArrayList<Node>> alreadyRegisteredNodes, Node testNode) {
    for (ArrayList<Node> nodes : alreadyRegisteredNodes) {
      for (Node node : nodes) {
        if (node==testNode) return true;
      }
    }
    return false;
  }

  boolean existsConnectionBetween(Node input, Node output) {
    for (Connection connection : connections) {
      if (connection.input==input && connection.output==output) return true;
    }
    return false;
  }

  ArrayList<int[]> getAllPossibleFutureConnections() {
    ArrayList<int[]> possibleConnections = new ArrayList<int[]>();

    int layers = feedForwardOrder.size();
    for (int l0=0; l0<layers-1; l0++) {
      ArrayList<Node> layer0 = feedForwardOrder.get(l0);
      for (int n0=0; n0<layer0.size(); n0++) {
        Node node0 = layer0.get(n0);

        for (int l1=l0+1; l1<layers; l1++) {
          ArrayList<Node> layer1 = feedForwardOrder.get(l1);
          for (int n1=0; n1<layer1.size(); n1++) {
            Node node1 = layer1.get(n1);

            if (!existsConnectionBetween(node0, node1)) {
              int id0 = node0.info.id;
              int id1 = node1.info.id;
              boolean willMakeFeedback = willConnectionMakeFeedBack(id0, id1);
              if (!willMakeFeedback) {
                int[] connection = new int[] { id0, id1 };
                possibleConnections.add(connection);
              }
            }
            //
          }
        }
      }
    } // end for l0
    return possibleConnections;
  }



  boolean willConnectionMakeFeedBack(int id0, int id1) {
    boolean DEBUG = true;
    if (DEBUG) print("checking feedback between "+id0+" and "+id1+": ");

    ArrayList<Integer> visitedNodes = new ArrayList<Integer>();
    visitedNodes.add(id0);
    Node node = getNodeById(id1);

    boolean feedback = followConnectionsAndCheckFeedback(node, visitedNodes);
    if (DEBUG) {
      print(visitedNodes);
      print(feedback?" FEEDBACK DETECTED!!":"");
    }
    if (DEBUG) println();
    return feedback;
  }

  boolean followConnectionsAndCheckFeedback(Node n, ArrayList<Integer> visitedNodes) {

    if (visitedNodes.contains(n.info.id)) {
      return true;
    }

    visitedNodes.add(n.info.id);

    ArrayList<Connection> nodeConns = n.outputConnections;

    ArrayList<Node> nextNodes = new ArrayList<Node>();
    for (Connection conn : nodeConns) {
      if (conn.info.enabled) {
        Node nextNode = conn.output;
        if (!nextNodes.contains(nextNode)) {
          nextNodes.add(nextNode);
        }
      }
    }

    for (Node nextNode : nextNodes) {

      boolean feedback = followConnectionsAndCheckFeedback(nextNode, visitedNodes);
      if (feedback) return true;
    }


    return false;
  }


  float getRandomWeight() {
    return randomGaussian()*RANDOM_WEIGHT_RANGE;
  }

  float getRandomWeightMutation() {
    return randomGaussian()*RANDOM_WEIGHT_MUTATION_RANGE;
  }















  void createFeedForwardOrder1() {
    feedForwardOrder.clear();

    boolean DEBUG = true;
    // find nodes without input

    if (DEBUG) println("createFeedForwardOrder()");

    // get sensor layer
    ArrayList<Node> firstLayer = getNodesByType(NodeModel.TYPE_SENSOR);
    feedForwardOrder.add(firstLayer);

    if (DEBUG) print("Layer 0("+firstLayer.size()+"): ");
    if (DEBUG) println(firstLayer);


    boolean secondLayerHasNodes; 
    int layerIndex = 1;

    do {

      // get enabled connections that are coming from the first layer nodes
      ArrayList<Connection> conns = new ArrayList<Connection>();
      for (Node node : firstLayer) {
        for (Connection c : node.outputConnections) {
          if (c.info.enabled) {
            conns.add(c);
          }
        }
      }
      if (DEBUG) print("  connections: ");
      if (DEBUG) println(conns);

      // get nodes that are connected to these connections
      ArrayList<Node> secondLayer = new ArrayList<Node>();
      for (Connection conn : conns) {
        Node receiver = conn.output;
        if (!secondLayer.contains(receiver))
          secondLayer.add(receiver);
      }
      secondLayerHasNodes = secondLayer.size()>0;
      if (DEBUG) print("  next layer nodes: "+secondLayer.size());

      if (secondLayerHasNodes) {
        //check for nodes that feed back to previous nodes to avoid recursion
        //removeFeedbackNodes(secondLayer, feedForwardOrder);

        // sort for viewing

        if (secondLayer.size()!=0)
          feedForwardOrder.add(secondLayer);
      }

      if (secondLayer.size()!=0) {
        //if (DEBUG) println();
        if (DEBUG) println(layerIndex+"nd layer ("+(secondLayerHasNodes?secondLayer.size():"-empty-")+")");
        if (DEBUG) println(secondLayer);
      }
      layerIndex++;

      firstLayer = secondLayer;
    } while (secondLayerHasNodes);

    // sort last layer by index
    feedForwardOrder.get(feedForwardOrder.size()-1).sort( new Comparator<Node>() {
      public int compare(Node n0, Node n1) {
        int id0 = n0.info.id;
        int id1 = n1.info.id;
        if (id0<id1) return -1;
        else if (id0>id1) return 1;
        else return 0;
      }
    }
    );

    if (DEBUG) println("end createFeedForwardOrder()");

    //_feedForwardOrderCreated = true;
  }
}
