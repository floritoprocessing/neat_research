class Node {
  
  final NodeModel info;
  
  ArrayList<Connection> inputConnections = new ArrayList<Connection>();
  ArrayList<Connection> outputConnections = new ArrayList<Connection>();
  
  Node(NodeModel info) {
    this.info = info;
  }
  
  public String toString() {
    return info.toString();
  }
  
}
