import java.util.*;

class Brain {

  float RANDOM_WEIGHT_RANGE = 0.2;
  float RANDOM_WEIGHT_MUTATION_RANGE = 0.2;

  ArrayList<Connection> connections;
  ArrayList<Node> nodes;
  ArrayList<ArrayList<Node>> feedForwardOrder;
    
  
  NodeDependancyTree tree;

  Brain() {
    clear();
  }

  Brain(NodeModel[] nodeInfos, ConnectionModel[] connectionInfos) {
    build(nodeInfos, connectionInfos);
  }

  void clear() {
    connections = new ArrayList<Connection>();
    nodes = new ArrayList<Node>();
  }

  /*
  *
   *  BUILD BRAIN
   *
   */

  void build(NodeModel[] nodeInfos, ConnectionModel[] connectionInfos) {
    clear();

    for (NodeModel nodeInfo : nodeInfos) {
      Node node = new Node(nodeInfo);
      nodes.add(node);
    }

    for (ConnectionModel connectionInfo : connectionInfos) {
      Connection connection = createConnectionAndSetNodeIO(connectionInfo);
      connections.add(connection);
    }

    createFeedForwardOrder();
  }

  /*
  *  Creates the connection and sets the input, ouputs of the Connection and adds the connection to the Node inputs and outputs
   */
  Connection createConnectionAndSetNodeIO(ConnectionModel connectionInfo) {
    Node inputNode = getNodeById(connectionInfo.inputNodeId);
    Node outputNode = getNodeById(connectionInfo.outputNodeId);
    Connection connection = new Connection(connectionInfo, inputNode, outputNode);

    inputNode.outputConnections.add(connection);
    outputNode.inputConnections.add(connection);
    return connection;
  }


  Node getNodeById(int id) {
    for (Node node : nodes) {
      if (node.info.id==id) return node;
    }
    throw new RuntimeException("Node id="+id+" not found");
  }



  ArrayList<Node> getNodesByType(int type) {
    ArrayList<Node> out = new ArrayList<Node>();
    for (Node node : nodes) {
      if (node.info.type==type) out.add(node);
    }
    return out;
  }
  
  ArrayList<Connection> getConnectionsLeadingIntoNode(Node node) {
    ArrayList<Connection> out = new ArrayList<Connection>();
    for (Connection connection:connections) {
      if (connection.output == node) out.add(connection);
    }
    return out;
  }







  void createFeedForwardOrder() {
    boolean DEBUG = true;
    if (DEBUG) println("BEGIN createFeedForwardOrder()");

    NodeDependancyTree tree = buildDependancyTree(this);
    if (DEBUG) println("  dependancy tree: "+tree);

    if (DEBUG) println("END createFeedForwardOrder");
  }








  boolean existsConnectionBetween(Node input, Node output) {
    for (Connection connection : connections) {
      if (connection.input==input && connection.output==output) return true;
    }
    return false;
  }
  
  ArrayList<int[]> getAllPossibleFutureConnections() {
    throw new RuntimeException("Unimplemented!!");
  }

  //ArrayList<int[]> getAllPossibleFutureConnections() {
  //  ArrayList<int[]> possibleConnections = new ArrayList<int[]>();

  //  int layers = feedForwardOrder.size();
  //  for (int l0=0; l0<layers-1; l0++) {
  //    ArrayList<Node> layer0 = feedForwardOrder.get(l0);
  //    for (int n0=0; n0<layer0.size(); n0++) {
  //      Node node0 = layer0.get(n0);

  //      for (int l1=l0+1; l1<layers; l1++) {
  //        ArrayList<Node> layer1 = feedForwardOrder.get(l1);
  //        for (int n1=0; n1<layer1.size(); n1++) {
  //          Node node1 = layer1.get(n1);

  //          if (!existsConnectionBetween(node0, node1)) {
  //            int id0 = node0.info.id;
  //            int id1 = node1.info.id;
  //            boolean willMakeFeedback = willConnectionMakeFeedBack(id0, id1);
  //            if (!willMakeFeedback) {
  //              int[] connection = new int[] { id0, id1 };
  //              possibleConnections.add(connection);
  //            }
  //          }
  //          //
  //        }
  //      }
  //    }
  //  } // end for l0
  //  return possibleConnections;
  //}



  boolean willConnectionMakeFeedBack(int id0, int id1) {
    boolean DEBUG = true;
    if (DEBUG) print("checking feedback between "+id0+" and "+id1+": ");

    ArrayList<Integer> visitedNodes = new ArrayList<Integer>();
    visitedNodes.add(id0);
    Node node = getNodeById(id1);

    boolean feedback = followConnectionsAndCheckFeedback(node, visitedNodes);
    if (DEBUG) {
      print(visitedNodes);
      print(feedback?" FEEDBACK DETECTED!!":"");
    }
    if (DEBUG) println();
    return feedback;
  }

  boolean followConnectionsAndCheckFeedback(Node n, ArrayList<Integer> visitedNodes) {

    if (visitedNodes.contains(n.info.id)) {
      return true;
    }

    visitedNodes.add(n.info.id);

    ArrayList<Connection> nodeConns = n.outputConnections;

    ArrayList<Node> nextNodes = new ArrayList<Node>();
    for (Connection conn : nodeConns) {
      if (conn.info.enabled) {
        Node nextNode = conn.output;
        if (!nextNodes.contains(nextNode)) {
          nextNodes.add(nextNode);
        }
      }
    }

    for (Node nextNode : nextNodes) {

      boolean feedback = followConnectionsAndCheckFeedback(nextNode, visitedNodes);
      if (feedback) return true;
    }


    return false;
  }


  float getRandomWeight() {
    return randomGaussian()*RANDOM_WEIGHT_RANGE;
  }

  float getRandomWeightMutation() {
    return randomGaussian()*RANDOM_WEIGHT_MUTATION_RANGE;
  }















}
