NodeDependancyTree buildDependancyTree(Brain brain) {

  NodeDependancyTree tree = new NodeDependancyTree(brain);

  // add sensor nodes
  tree.addSensors( brain.getNodesByType(NodeModel.TYPE_SENSOR) );

  // add nodes that only contain the sensors in the tree
  tree.findAndAddDependantNodes();


  return tree;
}


class NodeDependancyTree extends ArrayList<NodeDependancy> {

  Brain brain;
  ArrayList<Node> registeredNodes = new ArrayList<Node>();

  NodeDependancyTree(Brain brain) {
    super();
    this.brain = brain;
  }

  void addSensors( ArrayList<Node> nodes ) {
    for (Node node : nodes) {
      addNode(node);
    }
  }

  void addNodeDependancy( NodeDependancy nd) {
    add(nd);
    registeredNodes.add( nd.node );
  }

  void addNode( Node node ) {
    add( new NodeDependancy( this, node ) );
    registeredNodes.add( node );
  }

  NodeDependancy getDependancyWithNodeId(int id) {
    for (NodeDependancy nd : this) {
      if (nd.node.info.id==id) return nd;
    }
    throw new RuntimeException("not found!");
  }



  // finds a node that has as an input node only the existing nodes in the tree
  void findAndAddDependantNodes() {

    //for (int i=0;i<tree

    ArrayList<NodeDependancy> unsortedDependenciesWithoutSensors = new ArrayList<NodeDependancy>();
    for (int i=brain.nodes.size()-1; i>=0; i--) {
      Node node = brain.nodes.get(i);

      ArrayList<Node> allInputNodes = new ArrayList<Node>();
      getNodeInputNodes(brain, node, true, allInputNodes);

      NodeDependancy dep = new NodeDependancy(this, node);
      dep.dependantNodes.addAll( allInputNodes );
      if (allInputNodes.size()>0) {
        unsortedDependenciesWithoutSensors.add( dep );
        //println("  "+node+" -> "+allInputNodes);
      }
    }
    //println("  unsorted: "+unsortedDependenciesWithoutSensors);


    // remove from unsorted dependcies thos that have all dependant nodes present
    // i.e. 3: [1,2]
    int pass=0;
    do {

      // find dependency with only
      //println("pass "+pass++);
      
      for (int i=0; i<unsortedDependenciesWithoutSensors.size(); i++) {
        NodeDependancy unsortedNodeDependancy = unsortedDependenciesWithoutSensors.get(i);
       // println("  check "+unsortedNodeDependancy+" for connections with only "+registeredNodes);

        //ArrayList<NodeDependancy> allowedDependencies = getDependanciesWithOnlyTheseAndNotEmpty(registeredNodes);
        boolean legalNodes = true;
        for (Node n : unsortedNodeDependancy.dependantNodes) {
          boolean legalNode = false;
          if (registeredNodes.contains(n)) {
            legalNode = true;
          }
          legalNodes = legalNodes && legalNode;
        }
        if (legalNodes) {
          //println("  ->YES");
          unsortedDependenciesWithoutSensors.remove( unsortedNodeDependancy );
          addNodeDependancy( unsortedNodeDependancy );
          break; // break for i loop
        } else {
        }
      } // for i

      //unsortedDependenciesWithoutSensors.remove(0);
      //println(unsortedDependenciesWithoutSensors.size());
    } while (unsortedDependenciesWithoutSensors.size()>0);//);



    if (true) return;

    for (int i=brain.nodes.size()-1; i>=0; i--) {
      Node node = brain.nodes.get(i);
      //for (Node node : brain.nodes) {
      println("findAndAddDependantNodes for "+node.info.id);
      boolean skip = registeredNodes.contains( node );
      if (!skip) {

        NodeDependancy nodeDependancy = new NodeDependancy(this, node);
        boolean onlyRegisteredNodes = true;
        for (Connection connection : node.inputConnections) {
          Node inputNode = connection.input;
          boolean isRegistered = registeredNodes.contains( inputNode );
          if (isRegistered) {
            nodeDependancy.addDependantNodeOnlyOnce( getDependancyWithNodeId( inputNode.info.id ) );
          }
          onlyRegisteredNodes = onlyRegisteredNodes && isRegistered;
        }

        if (onlyRegisteredNodes) {
          addNodeDependancy(nodeDependancy);
        }
      }
      println("result: "+this);
    } // end iterate nodes
  }


  void getNodeInputNodes(Brain brain, Node node, boolean recurse, ArrayList<Node> output) {

    ArrayList<Connection> inputConnections = brain.getConnectionsLeadingIntoNode(node);
    for (Connection inputConnection : inputConnections) {
      Node previousNode = inputConnection.input;
      if (!output.contains(previousNode))
        output.add( previousNode );
      if (recurse) {
        getNodeInputNodes(brain, previousNode, recurse, output);
      }
    }
  }
}


class NodeDependancy {
  NodeDependancyTree tree;
  Node node;
  ArrayList<Node> dependantNodes = new ArrayList<Node>(); // nodes that depend on this one
  NodeDependancy(NodeDependancyTree tree, Node node) {
    this.tree = tree;
    this.node = node;
  }


  void addDependantNodeOnlyOnce(NodeDependancy nodeDependancy) {
    if (!dependantNodes.contains(nodeDependancy.node)) {
      println("  adding "+nodeDependancy.node.info.id);
      dependantNodes.add(nodeDependancy.node);
      // also add not only node, but also all previous dependant nodes
      ArrayList<Node> depNodes = new ArrayList<Node>();
      nodeDependancy.getAllDependantNodesRecursivelyInTree(depNodes, "  ");
      for (Node n : depNodes) 
        if (!dependantNodes.contains(n)) 
          dependantNodes.add( n );
      //dependantNodes.addAll( depNodes );
      //println("  adding "+depNodes);
      //dependantNodes.addAll( nodeDependancy.getAllDependantNodesRecursivelyInTree() );
    }
  }

  void getAllDependantNodesRecursivelyInTree(ArrayList<Node> out, String indent) {
    // with this dep (i.e. 5: [3]) in tree, find a node dependency 3 and add more dependancies
    //println(indent+"(getDeps for "+node.info.id+"...)");
    for (NodeDependancy ndsInTree : tree) {

      Node nodeInDepTree = ndsInTree.node;
      if (node == nodeInDepTree) {
        // add unique

        for (Node n : ndsInTree.dependantNodes) {
          if (!out.contains(n)) out.add(n);
          NodeDependancy nd = tree.getDependancyWithNodeId( n.info.id );
          nd.getAllDependantNodesRecursivelyInTree( out, indent+"  " );
        }
      }
    }
  }

  String toString() {
    String arr="";
    for (int i=0; i<dependantNodes.size(); i++) {
      arr += dependantNodes.get(i).info.id;
      if (i<dependantNodes.size()-1) arr+=", ";
    }
    return node.info.id+": ["+arr+"]";
  }
}
