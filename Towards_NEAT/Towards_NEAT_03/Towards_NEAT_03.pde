Brain brain;
BrainViewer brainViewer;

int nodeInnovation=0;
int connectionInnovation = 0;

void setup() {
  size(1000,600,P3D);
  
  //frameRate(2);
  /*
         _____
        /     \
  S1---H3--H5--O6
     X
  S2---H4------O7
  
  dependancy tree: [1: [], 2: [], 4: [1, 2], 7: [4, 1, 2], 3: [1, 2], 5: [3, 1, 2, 4], 6: [3, 1, 2, 5, 4]]
  dependancy tree: [1: [], 2: [], 3: [1, 2], 4: [1, 2], 5: [3, 1, 2, 4], 6: [3, 1, 2, 5, 4], 7: [4, 1, 2]]
  */
  
   NodeModel[] nodeInfos = new NodeModel[] {
    new NodeModel(1, NodeModel.TYPE_SENSOR),
    new NodeModel(2, NodeModel.TYPE_SENSOR),
    new NodeModel(3, NodeModel.TYPE_HIDDEN),
    new NodeModel(4, NodeModel.TYPE_HIDDEN),
    new NodeModel(5, NodeModel.TYPE_HIDDEN),
    new NodeModel(6, NodeModel.TYPE_OUTPUT),
    new NodeModel(7, NodeModel.TYPE_OUTPUT),
   };
   ConnectionModel[] connectionInfos = new ConnectionModel[] {
    new ConnectionModel(connectionInnovation++, 1, 3, 1.0, true),
    new ConnectionModel(connectionInnovation++, 1, 4, 1.0, true),   
    new ConnectionModel(connectionInnovation++, 2, 3, 1.0, true),
    new ConnectionModel(connectionInnovation++, 2, 4, 1.0, true),  
    new ConnectionModel(connectionInnovation++, 3, 5, 1.0, true), 
    new ConnectionModel(connectionInnovation++, 3, 6, 1.0, true),   
    new ConnectionModel(connectionInnovation++, 4, 5, 1.0, true),
    new ConnectionModel(connectionInnovation++, 4, 7, 1.0, true),
    new ConnectionModel(connectionInnovation++, 5, 6, 1.0, true)
   };
   
   
  
  /*
    _____
   /     \
  S0--H2--O3
     /
  S1------O4
  
  dependancy tree: [0: [], 1: [], 4: [1], 2: [0, 1], 3: [0, 2, 1]]
  */
  
   //NodeModel[] nodeInfos = new NodeModel[] {
   // new NodeModel(0, NodeModel.TYPE_SENSOR),
   // new NodeModel(1, NodeModel.TYPE_SENSOR),
   // new NodeModel(2, NodeModel.TYPE_HIDDEN),
   // new NodeModel(3, NodeModel.TYPE_OUTPUT),
   // new NodeModel(4, NodeModel.TYPE_OUTPUT),
   //};
   //ConnectionModel[] connectionInfos = new ConnectionModel[] {
   // new ConnectionModel(connectionInnovation++, 0, 2, 1.0, true),
   // new ConnectionModel(connectionInnovation++, 0, 3, 1.0, true),
   // new ConnectionModel(connectionInnovation++, 2, 3, 1.0, true), 
   // new ConnectionModel(connectionInnovation++, 1, 2, 1.0, true),
   // new ConnectionModel(connectionInnovation++, 1, 4, 1.0, true)    
   //};
   
  /*
  
      ____
    /     \
  S0--H2--H3--H5--O7
         /        /
        /        /
       /        /
      /        /
  S1--H4------H6--O8
  dependancy tree: [
    0: [], 
    1: [], 
    4: [1], 
    6: [4, 1], 
    8: [6, 4, 1], 
    2: [0], 
    3: [2, 0, 4, 1], 
    5: [3, 2, 0, 4, 1], 
    7: [5, 3, 2, 0, 4, 1, 6]]
    or
    dependancy tree: [
    0: [], 
    1: [], 
    2: [0], 
    4: [1], 
    3: [2, 0, 4, 1], 
    5: [3, 2, 0, 4, 1], 
    6: [4, 1], 
    7: [5, 3, 2, 0, 4, 1, 6], 
    8: [6, 4, 1]]
  
  */
  
  //NodeModel[] nodeInfos = new NodeModel[] {
  //  new NodeModel(0, NodeModel.TYPE_SENSOR),
  //  new NodeModel(1, NodeModel.TYPE_SENSOR),
    
  //  new NodeModel(2, NodeModel.TYPE_HIDDEN),
  //  new NodeModel(3, NodeModel.TYPE_HIDDEN),
  //  new NodeModel(4, NodeModel.TYPE_HIDDEN),
  //  new NodeModel(5, NodeModel.TYPE_HIDDEN),
  //  new NodeModel(6, NodeModel.TYPE_HIDDEN),
    
  //  new NodeModel(7, NodeModel.TYPE_OUTPUT),
  //  new NodeModel(8, NodeModel.TYPE_OUTPUT)
  //};
  
  //ConnectionModel[] connectionInfos = new ConnectionModel[] {
  //  new ConnectionModel(connectionInnovation++, 0, 2, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 2, 3, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 3, 5, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 5, 7, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 1, 4, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 4, 3, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 4, 6, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 6, 8, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 6, 7, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 0, 3, 1.0, true)
  //};
  
  // TODO: create feedback and test if it is detected
  
  brain = new Brain(nodeInfos, connectionInfos);
  brainViewer = new BrainViewer();
  println();
  //draw();
  //noLoop();
  //exit();
}

void keyPressed() {
  
  // add weight mutation
  if (key=='w') {
    Connection connection = brain.connections.get( (int)random(brain.connections.size()) );
    connection.info = connection.info.cloneWithNewWeight(connection.info.weight + brain.getRandomWeightMutation());
  }
  
  // add connection
  else if (key=='c') {   
    ArrayList<int[]> idPairs = brain.getAllPossibleFutureConnections();
    if (idPairs.size()==0) {
      println("Add node: no more connections available");
    } else {
      int[] idPair = idPairs.get( (int)random(idPairs.size()) );
      float w = brain.getRandomWeight();
      ConnectionModel cm = new ConnectionModel(connectionInnovation++, idPair[0], idPair[1], w, true);
      Connection connection = brain.createConnectionAndSetNodeIO(cm);
      brain.connections.add(connection);
      println("Add node: connection created: "+cm);
      brain.createDependancyTree();
    }
  }
  
  // add node (split connection)
  else if (key=='n') {
    
    print("Add node: split connection ");
    Connection connectionToSplit = brain.connections.get( (int)random(brain.connections.size()) );
    print(connectionToSplit.info+", ");
    // disable connection
    connectionToSplit.info = connectionToSplit.info.cloneDisabled();
    // add node
    NodeModel nodeInfo = new NodeModel(nodeInnovation++, NodeModel.TYPE_HIDDEN);
    Node node = new Node(nodeInfo);
    brain.nodes.add(node);
    print("add node "+node);
    // add connections
    // The new connection leading into the new node receives a weight of 1, 
    ConnectionModel cm0 = new ConnectionModel(
      connectionInnovation++, 
      connectionToSplit.input.info.id,
      node.info.id,
      1, true);
    //the new connection leading out receives the same weight as the old connection.
    ConnectionModel cm1 = new ConnectionModel(
      connectionInnovation++, 
      node.info.id,
      connectionToSplit.output.info.id,
      connectionToSplit.info.weight, true);
    
    Connection c0 = brain.createConnectionAndSetNodeIO(cm0);
    brain.connections.add(c0);
    Connection c1 = brain.createConnectionAndSetNodeIO(cm1);
    brain.connections.add(c1);
    
    print(", created "+cm0+" and "+cm1);
    println();
    
    brain.createDependancyTree();
    
  }
  
}


void draw() {
  background(255);
  fill(0);
  textAlign(LEFT,BASELINE);
  text("[w]eight mutation",10,20);
  text("add [c]onnection",10,35);
  text("add [n]ode",10,50);
  translate(50,100);
  brainViewer.draw(brain, true, true);
}
