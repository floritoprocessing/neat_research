import java.util.*;

class Brain {

  float RANDOM_WEIGHT_RANGE = 0.2;
  float RANDOM_WEIGHT_MUTATION_RANGE = 0.2;

  ArrayList<Connection> connections;
  ArrayList<Node> nodes;

  NodeDependancyTree tree;

  boolean rebuildFlagForDraw = false;

  Brain() {
    clear();
  }

  Brain(NodeModel[] nodeInfos, ConnectionModel[] connectionInfos) {
    build(nodeInfos, connectionInfos);
  }

  void clear() {
    connections = new ArrayList<Connection>();
    nodes = new ArrayList<Node>();
  }

  /*
  *
   *  BUILD BRAIN
   *
   */

  void build(NodeModel[] nodeInfos, ConnectionModel[] connectionInfos) {
    clear();

    for (NodeModel nodeInfo : nodeInfos) {
      Node node = new Node(nodeInfo);
      nodes.add(node);
    }

    for (ConnectionModel connectionInfo : connectionInfos) {
      Connection connection = createConnectionAndSetNodeIO(connectionInfo);
      connections.add(connection);
    }

    createDependancyTree();
  }


  void createDependancyTree() {
    boolean DEBUG = true;
    rebuildFlagForDraw = true;
    
    if (DEBUG) println("createDependancyTree()");
    tree = buildDependancyTree(this);
    //if (DEBUG) println("  dependancy tree: "+tree);
    if (DEBUG) {
      for (NodeDependancy nd:tree) {
        println(nd);
      }
    }
    //if (DEBUG) println("END build");
  }

  /*
  *  Creates the connection and sets the input, ouputs of the Connection and adds the connection to the Node inputs and outputs
   */
  Connection createConnectionAndSetNodeIO(ConnectionModel connectionInfo) {
    Node inputNode = getNodeById(connectionInfo.inputNodeId);
    Node outputNode = getNodeById(connectionInfo.outputNodeId);
    Connection connection = new Connection(connectionInfo, inputNode, outputNode);

    inputNode.outputConnections.add(connection);
    outputNode.inputConnections.add(connection);
    return connection;
  }


  Node getNodeById(int id) {
    for (Node node : nodes) {
      if (node.info.id==id) return node;
    }
    throw new RuntimeException("Node id="+id+" not found");
  }



  ArrayList<Node> getNodesByType(int type) {
    ArrayList<Node> out = new ArrayList<Node>();
    for (Node node : nodes) {
      if (node.info.type==type) out.add(node);
    }
    return out;
  }

  ArrayList<Connection> getEnabledConnectionsLeadingIntoNode(Node node) {
    ArrayList<Connection> out = new ArrayList<Connection>();
    for (Connection connection : connections) {
      if (connection.info.enabled && connection.output == node) out.add(connection);
    }
    return out;
  }












  ArrayList<int[]> getAllPossibleFutureConnections() {

    ArrayList<int[]> out = new ArrayList<int[]>();
    for (int i=0; i<nodes.size(); i++) {

      Node node0 = nodes.get(i);
      int id0 = node0.info.id;

      for (int j=0; j<nodes.size(); j++) {

        Node node1 = nodes.get(j);
        int id1 = node1.info.id;

        // not connection node to self
        // and not existing connection
        // and not will make feedback
        if (  id0!=id1 
          && !existsConnectionBetween(node0, node1) 
          && !willEnabledConnectionMakeFeedBack(id0, id1)
          && node0.info.type!=NodeModel.TYPE_OUTPUT
          && node1.info.type!=NodeModel.TYPE_SENSOR
          ) {

          int[] connection = new int[] { id0, id1 };
          out.add(connection);
        }
      }
    }

    return out;
  }




  boolean existsConnectionBetween(Node input, Node output) {
    for (Connection connection : connections) {
      if (connection.input==input && connection.output==output) return true;
    }
    return false;
  }


  boolean willEnabledConnectionMakeFeedBack(int id0, int id1) {
    boolean DEBUG = false;
    if (DEBUG) print("checking feedback between "+id0+" and "+id1+": ");

    ArrayList<Integer> visitedNodes = new ArrayList<Integer>();
    visitedNodes.add(id0);
    Node node = getNodeById(id1);

    boolean feedback = followEnabledConnectionsAndCheckFeedback(node, visitedNodes);
    if (DEBUG) {
      print(visitedNodes);
      print(feedback?" FEEDBACK DETECTED!!":"");
    }
    if (DEBUG) println();
    return feedback;
  }

  boolean followEnabledConnectionsAndCheckFeedback(Node n, ArrayList<Integer> visitedNodes) {

    if (visitedNodes.contains(n.info.id)) {
      return true;
    }

    visitedNodes.add(n.info.id);

    ArrayList<Connection> nodeConns = n.outputConnections;

    ArrayList<Node> nextNodes = new ArrayList<Node>();
    for (Connection conn : nodeConns) {
      if (conn.info.enabled) {
        Node nextNode = conn.output;
        if (!nextNodes.contains(nextNode)) {
          nextNodes.add(nextNode);
        }
      }
    }

    for (Node nextNode : nextNodes) {

      boolean feedback = followEnabledConnectionsAndCheckFeedback(nextNode, visitedNodes);
      if (feedback) return true;
    }


    return false;
  }





  float getRandomWeight() {
    return randomGaussian()*RANDOM_WEIGHT_RANGE;
  }

  float getRandomWeightMutation() {
    return randomGaussian()*RANDOM_WEIGHT_MUTATION_RANGE;
  }
}
