NodeDependancyTree buildDependancyTree(Brain brain) {

  NodeDependancyTree tree = new NodeDependancyTree(brain);

  // add sensor nodes
  tree.addSensors( brain.getNodesByType(NodeModel.TYPE_SENSOR) );

  // add nodes that only contain the sensors in the tree
  tree.findAndAddDependantNodes();


  return tree;
}

/*
*
 *    class NodeDependancyTree
 *
 */


class NodeDependancyTree extends ArrayList<NodeDependancy> {

  Brain brain;
  ArrayList<Node> registeredNodes = new ArrayList<Node>();

  NodeDependancyTree(Brain brain) {
    super();
    this.brain = brain;
  }

  void addSensors( ArrayList<Node> nodes ) {
    for (Node node : nodes) {
      addNode(node);
    }
  }

  void addNodeDependancy( NodeDependancy nd) {
    add(nd);
    registeredNodes.add( nd.node );
  }

  void addNode( Node node ) {
    add( new NodeDependancy( this, node ) );
    registeredNodes.add( node );
  }

  NodeDependancy getDependancyWithNodeId(int id) {
    for (NodeDependancy nd : this) {
      if (nd.node.info.id==id) return nd;
    }
    throw new RuntimeException("not found!");
  }



  // finds a node that has as an input node only the existing nodes in the tree
  void findAndAddDependantNodes() {

    //for (int i=0;i<tree

    ArrayList<NodeDependancy> unsortedDependenciesWithoutSensors = new ArrayList<NodeDependancy>();
    //for (int i=brain.nodes.size()-1; i>=0; i--) {
    for (int i=0; i<brain.nodes.size(); i++) {
      Node node = brain.nodes.get(i);

      ArrayList<Node> allInputNodes = new ArrayList<Node>();
      getNodeInputNodesThroughEnabledConnections(brain, node, true, allInputNodes);

      NodeDependancy dep = new NodeDependancy(this, node);
      dep.dependantNodes.addAll( allInputNodes );
      if (allInputNodes.size()>0) {
        unsortedDependenciesWithoutSensors.add( dep );
        //println("  "+node+" -> "+allInputNodes);
      }
    }
    //println("  unsorted: "+unsortedDependenciesWithoutSensors);


    // remove from unsorted dependcies thos that have all dependant nodes present
    // i.e. 3: [1,2]
    //int pass=0;
    do {

      // find dependency with only
      //println("pass "+pass++);

      for (int i=0; i<unsortedDependenciesWithoutSensors.size(); i++) {
        NodeDependancy unsortedNodeDependancy = unsortedDependenciesWithoutSensors.get(i);
        // println("  check "+unsortedNodeDependancy+" for connections with only "+registeredNodes);

        //ArrayList<NodeDependancy> allowedDependencies = getDependanciesWithOnlyTheseAndNotEmpty(registeredNodes);
        boolean legalNodes = true;
        for (Node n : unsortedNodeDependancy.dependantNodes) {
          boolean legalNode = false;
          if (registeredNodes.contains(n)) {
            legalNode = true;
          }
          legalNodes = legalNodes && legalNode;
        }
        if (legalNodes) {
          //println("  ->YES");
          unsortedDependenciesWithoutSensors.remove( unsortedNodeDependancy );
          addNodeDependancy( unsortedNodeDependancy );
          break; // break for i loop
        } else {
        }
      } // for i

      //unsortedDependenciesWithoutSensors.remove(0);
      //println(unsortedDependenciesWithoutSensors.size());
    } while (unsortedDependenciesWithoutSensors.size()>0);//);
  }


  void getNodeInputNodesThroughEnabledConnections(Brain brain, Node node, boolean recurse, ArrayList<Node> output) {

    ArrayList<Connection> inputConnections = brain.getEnabledConnectionsLeadingIntoNode(node);
    for (Connection inputConnection : inputConnections) {
      if (inputConnection.info.enabled) {
        Node previousNode = inputConnection.input;
        if (!output.contains(previousNode))
          output.add( previousNode );
        if (recurse) {
          getNodeInputNodesThroughEnabledConnections(brain, previousNode, recurse, output);
        }
      }
    }
  }
}

/*
*
 *    class NodeDependancy
 *
 */

class NodeDependancy {

  NodeDependancyTree tree;
  Node node;
  ArrayList<Node> dependantNodes = new ArrayList<Node>(); // nodes that depend on this one

  NodeDependancy(NodeDependancyTree tree, Node node) {
    this.tree = tree;
    this.node = node;
  }

  boolean hasSameDependantNodes(NodeDependancy other) {
    boolean oneDifferent = false;
    for (Node node : dependantNodes) {
      boolean same;
      if (other.dependantNodes.contains(node)) {
        // same = true;
      } else {
        oneDifferent = true;
      }
      //same = same && allSame;
    }
    return !oneDifferent;
  }


  void getAllDependantNodesRecursivelyInTree(ArrayList<Node> out, String indent) {
    // with this dep (i.e. 5: [3]) in tree, find a node dependency 3 and add more dependancies
    //println(indent+"(getDeps for "+node.info.id+"...)");
    for (NodeDependancy ndsInTree : tree) {

      Node nodeInDepTree = ndsInTree.node;
      if (node == nodeInDepTree) {
        // add unique

        for (Node n : ndsInTree.dependantNodes) {
          if (!out.contains(n)) out.add(n);
          NodeDependancy nd = tree.getDependancyWithNodeId( n.info.id );
          nd.getAllDependantNodesRecursivelyInTree( out, indent+"  " );
        }
      }
    }
  }

  String toString() {
    String arr="";
    for (int i=0; i<dependantNodes.size(); i++) {
      arr += dependantNodes.get(i).info.id;
      if (i<dependantNodes.size()-1) arr+=", ";
    }
    return node.info.id+": ["+arr+"]";
  }
}
