class BrainViewer {

  float nodeSize = 20;
  float outerRectWidth = 600;
  float outerRectHeight = 300;
  float CONNECTION_TEXT_RIGHT = 2.5;

  int CONNECTION_STROKE = 0xff000000;
  int CONNECTION_TEXT = 0xff000000;
  int CONNECTION_TEXT_BG = 0x44ffffff;

  int NODE_STROKE = 0xff000000;
  int NODE_FILL = 0xffcccccc;
  int NODE_TEXT = 0xff000000;

  float VERT_OFF_PERC = 0.1;
  float VERT_OFF_MIN_DIST = 0.09; // has to be less than half VERT_OFF_PERC

  float totalWidth, totalHeight;

  int lastSeedTime;
  ArrayList<Float> randomOffsetsAtSeed;
  float NEW_RANDOM_SEED_TIME = 0.05;

  long seed;

  HashMap<Node, Float> nodeYPositions;

  BrainViewer() {
    seed = System.currentTimeMillis();
    nodeYPositions = new HashMap<Node, Float>();
    totalWidth = outerRectWidth-2*nodeSize;
    totalHeight = outerRectHeight-2*nodeSize;
    lastSeedTime = millis();
  }

  void draw(Brain brain, boolean nodeText, boolean weightText) {

    int ti = millis();
    float dt = (ti-lastSeedTime)/1000.0;

    boolean newSeed = false;
    if (dt>NEW_RANDOM_SEED_TIME || randomOffsetsAtSeed==null || brain.rebuildFlagForDraw) {
      brain.rebuildFlagForDraw = false;
      randomOffsetsAtSeed = new ArrayList<Float>();
      lastSeedTime = ti;
      seed = System.currentTimeMillis();
      newSeed = true;
    }


    //println("new seed? "+newSeed);
    randomSeed(seed); 



    strokeWeight(1);

    stroke(200);
    noFill();
    rect(0, 0, outerRectWidth, outerRectHeight);

    // create node positions:


    NodeDependancyTree treeNodes = brain.tree;   
    HashMap<Node, PVector> nodePositions = new HashMap<Node, PVector>();


    float step = totalHeight;
    float x=0;
    float y=0;

    int randomIndex=0;

    float randomOffset = totalHeight*(random(1.0)<0.5?-1:1)*random(VERT_OFF_PERC);
    if (newSeed) {
      randomOffsetsAtSeed.clear();
      randomOffsetsAtSeed.add(randomOffset);
    } else {
      randomOffset = randomOffsetsAtSeed.get(randomIndex);
      randomIndex++;
    }

    int maxHorizontalLayers = 0;
    int maxVerticalLayers = 0;
    int vLayers = 0;

    ArrayList<ArrayList<Node>> layers = new ArrayList<ArrayList<Node>>();
    ArrayList<Node> currentLayer = new ArrayList<Node>();
    layers.add(currentLayer);

    NodeDependancy lastNd = null;
    for (NodeDependancy nd : treeNodes) {
      //println(randomOffset);
      // compare with previous nd to estimate vertical / horizontal distance
      if (lastNd==null) { // first one
      } else {
        if (nd.hasSameDependantNodes(lastNd)) {
          y += step;
          vLayers++;
        } else {
          currentLayer = new ArrayList<Node>();
          layers.add(currentLayer);
          maxVerticalLayers = max(maxVerticalLayers, vLayers);
          y = 0;
          x += step;
          maxHorizontalLayers++;
        }
      }

      currentLayer.add(nd.node);
      nodePositions.put(nd.node, new PVector(x, y, randomOffset));

      float oldRandomOffset = randomOffset;
      while (abs(oldRandomOffset-randomOffset)<VERT_OFF_MIN_DIST) {
        randomOffset = totalHeight*(random(1.0)<0.5?-1:1)*random(VERT_OFF_PERC);
      }

      if (newSeed) {
        randomOffsetsAtSeed.add(randomOffset);
      } else {
        randomOffset = randomOffsetsAtSeed.get(randomIndex);
        randomIndex++;
      }

      lastNd = nd;
    }





    // SCALE UP & KEEP WITHIN RANGE

    float currentWidth = maxHorizontalLayers*step;
    float currentHeight = (maxVerticalLayers-1)*step;
    float scaleX = totalWidth/currentWidth;
    float scaleY = totalHeight/currentHeight;
    if (currentHeight==0) {
      scaleY=1;
    }

    Iterator keyIterator = nodePositions.keySet().iterator();
    while (keyIterator.hasNext()) {
      Node node = (Node)keyIterator.next();
      PVector pos = nodePositions.get(node);
      pos.x *= scaleX;
      pos.y *= scaleY;
      pos.z *= scaleY;
      if (node.info.type == NodeModel.TYPE_SENSOR) {
        pos.x = 0;
      } else if (node.info.type == NodeModel.TYPE_OUTPUT) {
        pos.x = totalWidth;
      } else {
        y = pos.y+pos.z;
      if (y<-nodeSize/2||y>totalHeight+nodeSize/2) pos.z*=-1;
      pos.y += pos.z;
      }
      
    }



    // CHECK SOLO LAYERS: ADAPT

    // check solo node on layer: get y previous node position
    // unless 
    for (ArrayList<Node> layer : layers) {
      //println(layer);
      if (layer.size()==1) {
        Node node = layer.get(0);
        NodeDependancy nd = treeNodes.getDependancyWithNodeId(node.info.id);
        if (nd.dependantNodes.size()==1) {
          y = nodePositions.get( nd.dependantNodes.get(0) ).y;
          float z = nodePositions.get(node).z;
          if (y+z<0||y+z>totalHeight) z*=-1;
          nodePositions.get(node).y = y + z;
        }
        //nodePositions.get(node).y = totalHeight/2 + nodePositions.get(node).z;
      }
    }
    
    ArrayList<Node> inputNodes =brain.getNodesByType(NodeModel.TYPE_SENSOR);
    for (int i=0;i<inputNodes.size();i++) {
      y = map(i,0,inputNodes.size()-1,0,totalHeight);
      nodePositions.get(inputNodes.get(i)).y = y;
    }
    
    ArrayList<Node> outputNodes =brain.getNodesByType(NodeModel.TYPE_OUTPUT);
    for (int i=0;i<outputNodes.size();i++) {
      y = map(i,0,outputNodes.size()-1,0,totalHeight);
      nodePositions.get(outputNodes.get(i)).y = y;
    }



    // MOVE
    keyIterator = nodePositions.keySet().iterator();
    while (keyIterator.hasNext()) {
      Node node = (Node)keyIterator.next();
      PVector pos = nodePositions.get(node);
      pos.add(nodeSize, nodeSize);
      //println(node, pos);
    }



    // draw connections

    textAlign(CENTER, CENTER);
    for (Connection connection : brain.connections) {
      if (connection.info.enabled) {
        PVector p0 = nodePositions.get(connection.input);
        PVector p1 = nodePositions.get(connection.output);
        stroke(CONNECTION_STROKE);
        line(p0.x, p0.y, -1, p1.x, p1.y, -1);

        // arrow:
        pushMatrix();
        float l = 5;
        translate((p0.x+p1.x)/2, (p0.y+p1.y)/2, -1);
        float rotZ = atan2(p1.y-p0.y, p1.x-p0.x);
        rotate(rotZ);

        line(0, 0, -l, -l);
        line(0, 0, -l, l);
        popMatrix();

        if (weightText) {
          String connectionText = connection.info.innovation+": "+nf(connection.info.weight, 1, 4);
          pushMatrix();

          translate( (p0.x*1+p1.x*CONNECTION_TEXT_RIGHT)/(CONNECTION_TEXT_RIGHT+1), (p0.y*1+p1.y*CONNECTION_TEXT_RIGHT)/(CONNECTION_TEXT_RIGHT+1) );
          rotate(rotZ);
          float rw = textWidth(connectionText);
          float rh = textAscent()+textDescent();
          noStroke();
          fill(CONNECTION_TEXT_BG&0xffffff, CONNECTION_TEXT_BG>>24);

          rect(-rw/2, -rh/2, rw, rh);
          fill(CONNECTION_TEXT);
          text(connectionText, 0, 0);
          popMatrix();
        }
      } // end if enabled
    }

    // draw nodes
    textAlign(CENTER, CENTER);
    ellipseMode(CENTER);
    stroke(NODE_STROKE);
    for (Node node : brain.nodes) {
      PVector p = nodePositions.get(node);
      if (p!=null) {
        fill(NODE_FILL);
        ellipse(p.x, p.y, nodeSize, nodeSize);
        if (nodeText) {
          fill(NODE_TEXT);
          text(node.info.id, p.x, p.y);
        }
      }
    }

    randomSeed(System.currentTimeMillis());
  }
}
