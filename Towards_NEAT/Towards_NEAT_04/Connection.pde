class Connection {
  
  ConnectionModel info;
  
  final Node input;
  final Node output;
  
  Connection(ConnectionModel info, Node input, Node output) {
    this.info = info;
    this.input = input;
    this.output = output;
  }
  
  String toString() {
    return info.toString();
  }
  
}
