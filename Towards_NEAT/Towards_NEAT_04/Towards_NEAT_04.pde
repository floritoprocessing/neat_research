Brain brain;
BrainViewer brainViewer;

int nodeInnovation;
int connectionInnovation;


void setup() {
  size(1000, 600, P3D);

  setupBrain();
  brainViewer = new BrainViewer();
}

void setupBrain() {
  printClear();
  nodeInnovation=0;
  connectionInnovation = 0;

  BrainSetup brainSetup = new BrainSetup();
  nodeInnovation = brainSetup.nodeInfos.length;
  connectionInnovation = brainSetup.connectionInfos.length;

  brain = new Brain(brainSetup.nodeInfos, brainSetup.connectionInfos);
}

void keyPressed() {

  if (key=='r') {
    setupBrain();
  }
  // add weight mutation
  if (key=='w') {
    Connection connection = brain.connections.get( (int)random(brain.connections.size()) );
    connection.info = connection.info.cloneWithNewWeight(connection.info.weight + brain.getRandomWeightMutation());
  }

  // add connection
  else if (key=='c') {   
    ArrayList<int[]> idPairs = brain.getAllPossibleFutureConnections();
    //idPairs.clear();
    //idPairs.add(new int[] {3,4});
    if (idPairs.size()==0) {
      println("Add connection: no more connections available");
    } else {
      //println("Add node: possible connections:");
      //for (int[] pair:idPairs) print(pair+", ");
      //println();
      int[] idPair = idPairs.get( (int)random(idPairs.size()) );
      float w = brain.getRandomWeight();
      ConnectionModel cm = new ConnectionModel(connectionInnovation++, idPair[0], idPair[1], w, true);
      Connection connection = brain.createConnectionAndSetNodeIO(cm);
      brain.connections.add(connection);
      println("Add connection: connection created: "+cm);
      brain.createDependancyTree();
    }
  }

  // add node (split connection)
  else if (key=='n') {

    print("Add node: split connection ");
    System.err.println("Watch out, are we updating node input/output connections?");
    Connection connectionToSplit = brain.connections.get( (int)random(brain.connections.size()) );
    print(connectionToSplit.info+", ");
    // disable connection
    connectionToSplit.info = connectionToSplit.info.cloneDisabled();
    // add node
    NodeModel nodeInfo = new NodeModel(nodeInnovation++, NodeModel.TYPE_HIDDEN);
    Node node = new Node(nodeInfo);
    brain.nodes.add(node);
    print("add node "+node);
    // add connections
    // The new connection leading into the new node receives a weight of 1, 
    ConnectionModel cm0 = new ConnectionModel(
      connectionInnovation++, 
      connectionToSplit.input.info.id, 
      node.info.id, 
      1, true);
    //the new connection leading out receives the same weight as the old connection.
    ConnectionModel cm1 = new ConnectionModel(
      connectionInnovation++, 
      node.info.id, 
      connectionToSplit.output.info.id, 
      connectionToSplit.info.weight, true);

    Connection c0 = brain.createConnectionAndSetNodeIO(cm0);
    brain.connections.add(c0);
    Connection c1 = brain.createConnectionAndSetNodeIO(cm1);
    brain.connections.add(c1);

    print(", created "+cm0+" and "+cm1);
    println();

    brain.createDependancyTree();
  }
}


void draw() {
  background(255);
  fill(0);
  textAlign(LEFT, BASELINE);
  text("[r]eset brain", 10, 20);
  text("[w]eight mutation", 10, 35);
  text("add [c]onnection", 10, 50);
  text("add [n]ode", 10, 65);
  
  printDebugText(this,700,50);
  
  translate(50, 100);
  brainViewer.draw(brain, true, true);
}
