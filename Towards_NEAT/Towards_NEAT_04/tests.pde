class BrainSetup {
  //frameRate(2);
  /*
         _____
        /     \
  S1---H3--H5--O6
     X
  S2---H4------O7
  
  dependancy tree: [1: [], 2: [], 4: [1, 2], 7: [4, 1, 2], 3: [1, 2], 5: [3, 1, 2, 4], 6: [3, 1, 2, 5, 4]]
  dependancy tree: [1: [], 2: [], 3: [1, 2], 4: [1, 2], 5: [3, 1, 2, 4], 6: [3, 1, 2, 5, 4], 7: [4, 1, 2]]
  */
  
   //NodeModel[] nodeInfos = new NodeModel[] {
   // new NodeModel(0, NodeModel.TYPE_SENSOR),
   // new NodeModel(1, NodeModel.TYPE_SENSOR),
   // new NodeModel(2, NodeModel.TYPE_HIDDEN),
   // new NodeModel(3, NodeModel.TYPE_HIDDEN),
   // new NodeModel(4, NodeModel.TYPE_HIDDEN),
   // new NodeModel(5, NodeModel.TYPE_OUTPUT),
   // new NodeModel(6, NodeModel.TYPE_OUTPUT),
   //};
   //ConnectionModel[] connectionInfos = new ConnectionModel[] {
   // new ConnectionModel(connectionInnovation++, 0, 2, 1.0, true),
   // new ConnectionModel(connectionInnovation++, 0, 3, 1.0, true),   
   // new ConnectionModel(connectionInnovation++, 1, 2, 1.0, true),
   // new ConnectionModel(connectionInnovation++, 1, 3, 1.0, true),  
   // new ConnectionModel(connectionInnovation++, 2, 4, 1.0, true), 
   // new ConnectionModel(connectionInnovation++, 2, 5, 1.0, true),   
   // new ConnectionModel(connectionInnovation++, 3, 4, 1.0, true),
   // new ConnectionModel(connectionInnovation++, 3, 6, 1.0, true),
   // new ConnectionModel(connectionInnovation++, 4, 5, 1.0, true)
   //};
   
   
  
  /*
    _____
   /     \
  S0--H2--O3
     /
  S1------O4
  
  dependancy tree: [0: [], 1: [], 4: [1], 2: [0, 1], 3: [0, 2, 1]]
  */
  
   //NodeModel[] nodeInfos = new NodeModel[] {
   // new NodeModel(0, NodeModel.TYPE_SENSOR),
   // new NodeModel(1, NodeModel.TYPE_SENSOR),
   // new NodeModel(2, NodeModel.TYPE_HIDDEN),
   // new NodeModel(3, NodeModel.TYPE_OUTPUT),
   // new NodeModel(4, NodeModel.TYPE_OUTPUT),
   //};
   //ConnectionModel[] connectionInfos = new ConnectionModel[] {
   // new ConnectionModel(connectionInnovation++, 0, 2, 1.0, true),
   // new ConnectionModel(connectionInnovation++, 0, 3, 1.0, true),
   // new ConnectionModel(connectionInnovation++, 2, 3, 1.0, true), 
   // new ConnectionModel(connectionInnovation++, 1, 2, 1.0, true),
   // new ConnectionModel(connectionInnovation++, 1, 4, 1.0, true)    
   //};
   
  /*
  
      ____
    /     \
  S0--H2--H3--H5--O7
         /        /
        /        /
       /        /
      /        /
  S1--H4------H6--O8
  dependancy tree: [
    0: [], 
    1: [], 
    4: [1], 
    6: [4, 1], 
    8: [6, 4, 1], 
    2: [0], 
    3: [2, 0, 4, 1], 
    5: [3, 2, 0, 4, 1], 
    7: [5, 3, 2, 0, 4, 1, 6]]
    or
    dependancy tree: [
    0: [], 
    1: [], 
    2: [0], 
    4: [1], 
    3: [2, 0, 4, 1], 
    5: [3, 2, 0, 4, 1], 
    6: [4, 1], 
    7: [5, 3, 2, 0, 4, 1, 6], 
    8: [6, 4, 1]]
  
  */
  
  //NodeModel[] nodeInfos = new NodeModel[] {
  //  new NodeModel(0, NodeModel.TYPE_SENSOR),
  //  new NodeModel(1, NodeModel.TYPE_SENSOR),
    
  //  new NodeModel(2, NodeModel.TYPE_HIDDEN),
  //  new NodeModel(3, NodeModel.TYPE_HIDDEN),
  //  new NodeModel(4, NodeModel.TYPE_HIDDEN),
  //  new NodeModel(5, NodeModel.TYPE_HIDDEN),
  //  new NodeModel(6, NodeModel.TYPE_HIDDEN),
    
  //  new NodeModel(7, NodeModel.TYPE_OUTPUT),
  //  new NodeModel(8, NodeModel.TYPE_OUTPUT)
  //};
  
  //ConnectionModel[] connectionInfos = new ConnectionModel[] {
  //  new ConnectionModel(connectionInnovation++, 0, 2, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 2, 3, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 3, 5, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 5, 7, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 1, 4, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 4, 3, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 4, 6, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 6, 8, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 6, 7, 1.0, true),
  //  new ConnectionModel(connectionInnovation++, 0, 3, 1.0, true)
  //};
  
  NodeModel[] nodeInfos = new NodeModel[] {
    new NodeModel(0, NodeModel.TYPE_SENSOR),
    new NodeModel(1, NodeModel.TYPE_SENSOR),   
    new NodeModel(2, NodeModel.TYPE_HIDDEN),
    new NodeModel(3, NodeModel.TYPE_HIDDEN),
    new NodeModel(4, NodeModel.TYPE_OUTPUT),
    new NodeModel(5, NodeModel.TYPE_OUTPUT)
  };
  ConnectionModel[] connectionInfos = new ConnectionModel[] {
    new ConnectionModel(connectionInnovation++, 0, 2, 1.0, true),
    new ConnectionModel(connectionInnovation++, 2, 4, 1.0, true),
    new ConnectionModel(connectionInnovation++, 1, 3, 1.0, true),
    new ConnectionModel(connectionInnovation++, 3, 5, 1.0, true),
  };
}
