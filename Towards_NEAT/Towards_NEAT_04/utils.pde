static String debugText="";
static int maxLengthDebugText = 35;
static float maxDebugTextWidth = Float.MAX_VALUE;

static void printDebugText(PApplet pa, float x, float y) {
  if (maxDebugTextWidth==Float.MAX_VALUE) {
    maxDebugTextWidth = 100;//pa.width-x-10;
  }
  wrapDebugText(pa);
  pa.text("debug text:", x, y);
  pa.text(debugText, x,y+30);
}

static void wrapDebugText(PApplet pa) {
  //PApplet.println("wrapDebugText "+maxDebugTextWidth);
  String[] lines = debugText.split("\r\n");
  ArrayList<String> wrappedLines = new ArrayList<String>();
  for (String line:lines) {
     wrappedLines.addAll( wrapLine(pa, line) );
  }
  debugText = "";
  for (int i=0;i<wrappedLines.size();i++) {
    debugText += wrappedLines.get(i);
    if (i<wrappedLines.size()-1)
      debugText += "\r\n";
  }
}

static ArrayList<String> wrapLine(PApplet pa, String line) {
  ArrayList<String> out = new ArrayList<String>();
  if (pa.textWidth(line)<maxDebugTextWidth) {
    out.add(line);
  } else {
    out.add("TODO wrap: ");//+line);//+line.substring(0,20)+"...");
  }
  return out;
}

static void println(Object o) {
  PApplet.println(o);
  debugText+=o.toString()+"\r\n";
  checkDebugTextLength();
}
static void print(Object o) {
  PApplet.print(o);
  debugText+=o.toString();
  checkDebugTextLength();
}
static void print(String s) {
  PApplet.print(s);
  debugText+=s;
  checkDebugTextLength();
}
static void println(String s) {
  PApplet.print(s);
  debugText+=s+"\r\n";
  checkDebugTextLength();
}

static void printClear() {
  debugText = "";
  checkDebugTextLength();
}

static void checkDebugTextLength() {
  String[] lines = debugText.split("\r\n");

  int linesToRemove = lines.length-maxLengthDebugText;

  if (linesToRemove>0) {
    debugText = "";

    for (int i=linesToRemove; i<lines.length; i++) {
      debugText += lines[i];
      if (i<lines.length-1)
        debugText += "\r\n";
    }
  }
}
